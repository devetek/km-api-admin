<?php
/**
 * @SWG\Swagger(
 *     schemes={"http"},
 *     host="apidev.hompes.id",
 *     basePath="/v1",
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="Home Pesantren Documentation",
 *         description="Dokumentasi API Home Pesantren",
 *         termsOfService="http://devetek.com/terms/",
 *         @SWG\Contact(
 *             email="support@devetek.com"
 *         ),
 *         @SWG\License(
 *             name="Apache 2.0",
 *             url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *         )
 *     ),
 *     @SWG\ExternalDocumentation(
 *         description="Cari tahu lebih banyak tentang Devetek",
 *         url="http://devetek.com"
 *     )
 * )
 */
