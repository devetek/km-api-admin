<?php
/**
 * @SWG\Get(
 *     path="/user",
 *     summary="Get all user",
 *     tags={"user"},
 *     description="Dapatkan semua data user",
 *     operationId="userAll",
 *     produces={"application/json"},
 *     @SWG\Parameter(
 *         name="page",
 *         in="query",
 *         description="Posisi page",
 *         required=false,
 *         type="integer",
 *     ),
 *     @SWG\Parameter(
 *         name="limit",
 *         in="query",
 *         description="Maksimal data ditampilkan",
 *         required=false,
 *         type="integer",
 *     ),
 *     @SWG\Response(
 *          response=200,
 *          description="successful operation",
 *          @SWG\schema(
 *            type="array",
 *            @SWG\items(
 *              type="object",
 *              @SWG\Property(
 *                property="data",
 *                type="object",
 *                @SWG\items(
 *                  type="object",
 *                  @SWG\Property(property="name", type="string"),
 *                  @SWG\Property(property="address", type="string"),
 *                  @SWG\Property(property="genre", type="string"),
 *                ),
 *              ),
 *              @SWG\Property(property="hasNext", type="string"),
 *              @SWG\Property(property="status", type="boolean"),
 *            ),
 *          ),
 *     ),
 *     @SWG\Response(
 *          response="400",
 *          description="Invalid tag value",
 *          @SWG\schema(
 *            type="string",
 *            @SWG\Property(property="name", type="string"),
 *          ),
 *     ),
 *     security={
 *         {
 *             "petstore_auth": {"write:pets", "read:pets"}
 *         }
 *     },
 *     deprecated=true
 * )
 */
