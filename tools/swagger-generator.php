<?php

$FILE_NAME = './public/doc/swagger.json';

require __DIR__ . '/../vendor/autoload.php';

$swagger = \Swagger\scan(__DIR__ . '/../tools/docs');

file_put_contents($FILE_NAME, $swagger);
