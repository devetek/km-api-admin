# Home Pesantren API

## About

Home Pesantren API v0.0.1-beta is first phase API for Home Pesantren. Providing data using REST API.

Available modules on v0.0.1-beta:

-   User (Dazzle)
-   Book (Rubbick)
-   Package (Keeper of the Light)
-   Chat (Bloodseeker)
-   Purchase (Bounty Hunter)

This repository related with [Omni](https://github.com/devetek/Omni).

Author: @arivin29

## Run Development

Requirements:

-   UNIX
-   Docker Engine 17.06.0+

Running on your local environment using docker, with exec command below

```sh
make run-dev
```

After execute command above, you will have single proxy port activated for all services.

-   [http://localhost:9200/mysql](http://localhost:9200/mysql) is databse management.
-   [http://localhost:9200](http://localhost:9200) is Home Pesantren API.
-   [http://localhost:9200/doc](http://localhost:9200/doc) is swagger UI.

Databse Environment

```sh
MYSQL_USER: devhompes
MYSQL_PASSWORD: development
```

Running this project manually, with prepare all environment requirements by your self.

## Auto Deploy on Push

-   Push to `master` branch and it will deployed automatically. Open [apidev.hompes.id](http://apidev.hompes.id) to see the updates.

## Generate API Documentation

-   execute command below:

```sh
make generate-docs
```

-   push to repository to master branch

```sh
git push origin master
```

-   open [home pesantren documentation](http://apidev.hompes.id/doc)

## Swagger Documentation References

-   Swagger Home: [swagger 2](https://github.com/zircote/swagger-php/tree/2.0.13)
-   Swagger Docs: [Docs](https://github.com/zircote/swagger-php/tree/2.x/docs)
-   Swagger Example: [Example](https://github.com/zircote/swagger-php/tree/2.0.13/Examples)
