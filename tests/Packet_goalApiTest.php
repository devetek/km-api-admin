<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Packet_goalApiTest extends TestCase
{
    use MakePacket_goalTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePacket_goal()
    {
        $packetGoal = $this->fakePacket_goalData();
        $this->json('POST', '/api/v1/packetGoals', $packetGoal);

        $this->assertApiResponse($packetGoal);
    }

    /**
     * @test
     */
    public function testReadPacket_goal()
    {
        $packetGoal = $this->makePacket_goal();
        $this->json('GET', '/api/v1/packetGoals/'.$packetGoal->id_packet_goal);

        $this->assertApiResponse($packetGoal->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePacket_goal()
    {
        $packetGoal = $this->makePacket_goal();
        $editedPacket_goal = $this->fakePacket_goalData();

        $this->json('PUT', '/api/v1/packetGoals/'.$packetGoal->id_packet_goal, $editedPacket_goal);

        $this->assertApiResponse($editedPacket_goal);
    }

    /**
     * @test
     */
    public function testDeletePacket_goal()
    {
        $packetGoal = $this->makePacket_goal();
        $this->json('DELETE', '/api/v1/packetGoals/'.$packetGoal->id_packet_goal);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/packetGoals/'.$packetGoal->id_packet_goal);

        $this->assertResponseStatus(404);
    }
}
