<?php

use App\Models\Packet_cover;
use App\Repositories\Packet\admin\Packet_coverRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Packet_coverRepositoryTest extends TestCase
{
    use MakePacket_coverTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Packet_coverRepository
     */
    protected $packetCoverRepo;

    public function setUp()
    {
        parent::setUp();
        $this->packetCoverRepo = App::make(Packet_coverRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePacket_cover()
    {
        $packetCover = $this->fakePacket_coverData();
        $createdPacket_cover = $this->packetCoverRepo->create($packetCover);
        $createdPacket_cover = $createdPacket_cover->toArray();
        $this->assertArrayHasKey('id', $createdPacket_cover);
        $this->assertNotNull($createdPacket_cover['id'], 'Created Packet_cover must have id specified');
        $this->assertNotNull(Packet_cover::find($createdPacket_cover['id']), 'Packet_cover with given id must be in DB');
        $this->assertModelData($packetCover, $createdPacket_cover);
    }

    /**
     * @test read
     */
    public function testReadPacket_cover()
    {
        $packetCover = $this->makePacket_cover();
        $dbPacket_cover = $this->packetCoverRepo->find($packetCover->id_packet);
        $dbPacket_cover = $dbPacket_cover->toArray();
        $this->assertModelData($packetCover->toArray(), $dbPacket_cover);
    }

    /**
     * @test update
     */
    public function testUpdatePacket_cover()
    {
        $packetCover = $this->makePacket_cover();
        $fakePacket_cover = $this->fakePacket_coverData();
        $updatedPacket_cover = $this->packetCoverRepo->update($fakePacket_cover, $packetCover->id_packet);
        $this->assertModelData($fakePacket_cover, $updatedPacket_cover->toArray());
        $dbPacket_cover = $this->packetCoverRepo->find($packetCover->id_packet);
        $this->assertModelData($fakePacket_cover, $dbPacket_cover->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePacket_cover()
    {
        $packetCover = $this->makePacket_cover();
        $resp = $this->packetCoverRepo->delete($packetCover->id_packet);
        $this->assertTrue($resp);
        $this->assertNull(Packet_cover::find($packetCover->id_packet), 'Packet_cover should not exist in DB');
    }
}
