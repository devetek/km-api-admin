<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Packet_detailApiTest extends TestCase
{
    use MakePacket_detailTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePacket_detail()
    {
        $packetDetail = $this->fakePacket_detailData();
        $this->json('POST', '/api/v1/packetDetails', $packetDetail);

        $this->assertApiResponse($packetDetail);
    }

    /**
     * @test
     */
    public function testReadPacket_detail()
    {
        $packetDetail = $this->makePacket_detail();
        $this->json('GET', '/api/v1/packetDetails/'.$packetDetail->id_packet_detail);

        $this->assertApiResponse($packetDetail->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePacket_detail()
    {
        $packetDetail = $this->makePacket_detail();
        $editedPacket_detail = $this->fakePacket_detailData();

        $this->json('PUT', '/api/v1/packetDetails/'.$packetDetail->id_packet_detail, $editedPacket_detail);

        $this->assertApiResponse($editedPacket_detail);
    }

    /**
     * @test
     */
    public function testDeletePacket_detail()
    {
        $packetDetail = $this->makePacket_detail();
        $this->json('DELETE', '/api/v1/packetDetails/'.$packetDetail->id_packet_detail);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/packetDetails/'.$packetDetail->id_packet_detail);

        $this->assertResponseStatus(404);
    }
}
