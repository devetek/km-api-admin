<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Publisher_teamApiTest extends TestCase
{
    use MakePublisher_teamTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePublisher_team()
    {
        $publisherTeam = $this->fakePublisher_teamData();
        $this->json('POST', '/api/v1/publisherTeams', $publisherTeam);

        $this->assertApiResponse($publisherTeam);
    }

    /**
     * @test
     */
    public function testReadPublisher_team()
    {
        $publisherTeam = $this->makePublisher_team();
        $this->json('GET', '/api/v1/publisherTeams/'.$publisherTeam->id_publisher_team);

        $this->assertApiResponse($publisherTeam->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePublisher_team()
    {
        $publisherTeam = $this->makePublisher_team();
        $editedPublisher_team = $this->fakePublisher_teamData();

        $this->json('PUT', '/api/v1/publisherTeams/'.$publisherTeam->id_publisher_team, $editedPublisher_team);

        $this->assertApiResponse($editedPublisher_team);
    }

    /**
     * @test
     */
    public function testDeletePublisher_team()
    {
        $publisherTeam = $this->makePublisher_team();
        $this->json('DELETE', '/api/v1/publisherTeams/'.$publisherTeam->id_publisher_team);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/publisherTeams/'.$publisherTeam->id_publisher_team);

        $this->assertResponseStatus(404);
    }
}
