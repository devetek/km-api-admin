<?php

use App\Models\Member_report;
use App\Repositories\Member\admin\Member_reportRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Member_reportRepositoryTest extends TestCase
{
    use MakeMember_reportTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Member_reportRepository
     */
    protected $memberReportRepo;

    public function setUp()
    {
        parent::setUp();
        $this->memberReportRepo = App::make(Member_reportRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateMember_report()
    {
        $memberReport = $this->fakeMember_reportData();
        $createdMember_report = $this->memberReportRepo->create($memberReport);
        $createdMember_report = $createdMember_report->toArray();
        $this->assertArrayHasKey('id', $createdMember_report);
        $this->assertNotNull($createdMember_report['id'], 'Created Member_report must have id specified');
        $this->assertNotNull(Member_report::find($createdMember_report['id']), 'Member_report with given id must be in DB');
        $this->assertModelData($memberReport, $createdMember_report);
    }

    /**
     * @test read
     */
    public function testReadMember_report()
    {
        $memberReport = $this->makeMember_report();
        $dbMember_report = $this->memberReportRepo->find($memberReport->id_company);
        $dbMember_report = $dbMember_report->toArray();
        $this->assertModelData($memberReport->toArray(), $dbMember_report);
    }

    /**
     * @test update
     */
    public function testUpdateMember_report()
    {
        $memberReport = $this->makeMember_report();
        $fakeMember_report = $this->fakeMember_reportData();
        $updatedMember_report = $this->memberReportRepo->update($fakeMember_report, $memberReport->id_company);
        $this->assertModelData($fakeMember_report, $updatedMember_report->toArray());
        $dbMember_report = $this->memberReportRepo->find($memberReport->id_company);
        $this->assertModelData($fakeMember_report, $dbMember_report->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteMember_report()
    {
        $memberReport = $this->makeMember_report();
        $resp = $this->memberReportRepo->delete($memberReport->id_company);
        $this->assertTrue($resp);
        $this->assertNull(Member_report::find($memberReport->id_company), 'Member_report should not exist in DB');
    }
}
