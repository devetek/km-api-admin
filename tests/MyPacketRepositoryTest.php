<?php

use App\Models\MyPacket;
use App\Repositories\Packet\client\MyPacketRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MyPacketRepositoryTest extends TestCase
{
    use MakeMyPacketTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var MyPacketRepository
     */
    protected $myPacketRepo;

    public function setUp()
    {
        parent::setUp();
        $this->myPacketRepo = App::make(MyPacketRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateMyPacket()
    {
        $myPacket = $this->fakeMyPacketData();
        $createdMyPacket = $this->myPacketRepo->create($myPacket);
        $createdMyPacket = $createdMyPacket->toArray();
        $this->assertArrayHasKey('id', $createdMyPacket);
        $this->assertNotNull($createdMyPacket['id'], 'Created MyPacket must have id specified');
        $this->assertNotNull(MyPacket::find($createdMyPacket['id']), 'MyPacket with given id must be in DB');
        $this->assertModelData($myPacket, $createdMyPacket);
    }

    /**
     * @test read
     */
    public function testReadMyPacket()
    {
        $myPacket = $this->makeMyPacket();
        $dbMyPacket = $this->myPacketRepo->find($myPacket->id_packet);
        $dbMyPacket = $dbMyPacket->toArray();
        $this->assertModelData($myPacket->toArray(), $dbMyPacket);
    }

    /**
     * @test update
     */
    public function testUpdateMyPacket()
    {
        $myPacket = $this->makeMyPacket();
        $fakeMyPacket = $this->fakeMyPacketData();
        $updatedMyPacket = $this->myPacketRepo->update($fakeMyPacket, $myPacket->id_packet);
        $this->assertModelData($fakeMyPacket, $updatedMyPacket->toArray());
        $dbMyPacket = $this->myPacketRepo->find($myPacket->id_packet);
        $this->assertModelData($fakeMyPacket, $dbMyPacket->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteMyPacket()
    {
        $myPacket = $this->makeMyPacket();
        $resp = $this->myPacketRepo->delete($myPacket->id_packet);
        $this->assertTrue($resp);
        $this->assertNull(MyPacket::find($myPacket->id_packet), 'MyPacket should not exist in DB');
    }
}
