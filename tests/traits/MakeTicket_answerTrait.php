<?php

use Faker\Factory as Faker;
use App\Models\Ticket_answer;
use App\Repositories\Ticket\admin\Ticket_answerRepository;

trait MakeTicket_answerTrait
{
    /**
     * Create fake instance of Ticket_answer and save it in database
     *
     * @param array $ticketAnswerFields
     * @return Ticket_answer
     */
    public function makeTicket_answer($ticketAnswerFields = [])
    {
        /** @var Ticket_answerRepository $ticketAnswerRepo */
        $ticketAnswerRepo = App::make(Ticket_answerRepository::class);
        $theme = $this->fakeTicket_answerData($ticketAnswerFields);
        return $ticketAnswerRepo->create($theme);
    }

    /**
     * Get fake instance of Ticket_answer
     *
     * @param array $ticketAnswerFields
     * @return Ticket_answer
     */
    public function fakeTicket_answer($ticketAnswerFields = [])
    {
        return new Ticket_answer($this->fakeTicket_answerData($ticketAnswerFields));
    }

    /**
     * Get fake data of Ticket_answer
     *
     * @param array $postFields
     * @return array
     */
    public function fakeTicket_answerData($ticketAnswerFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'id_ticket' => $fake->randomDigitNotNull,
            'date_answer' => $fake->date('Y-m-d H:i:s'),
            'id_user' => $fake->randomDigitNotNull,
            'answer' => $fake->text,
            'has_approved' => $fake->word,
            'validasi' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $ticketAnswerFields);
    }
}
