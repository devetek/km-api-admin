<?php

use Faker\Factory as Faker;
use App\Models\ChangeCover;
use App\Repositories\Book\admin\ChangeCoverRepository;

trait MakeChangeCoverTrait
{
    /**
     * Create fake instance of ChangeCover and save it in database
     *
     * @param array $changeCoverFields
     * @return ChangeCover
     */
    public function makeChangeCover($changeCoverFields = [])
    {
        /** @var ChangeCoverRepository $changeCoverRepo */
        $changeCoverRepo = App::make(ChangeCoverRepository::class);
        $theme = $this->fakeChangeCoverData($changeCoverFields);
        return $changeCoverRepo->create($theme);
    }

    /**
     * Get fake instance of ChangeCover
     *
     * @param array $changeCoverFields
     * @return ChangeCover
     */
    public function fakeChangeCover($changeCoverFields = [])
    {
        return new ChangeCover($this->fakeChangeCoverData($changeCoverFields));
    }

    /**
     * Get fake data of ChangeCover
     *
     * @param array $postFields
     * @return array
     */
    public function fakeChangeCoverData($changeCoverFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $changeCoverFields);
    }
}
