<?php

use Faker\Factory as Faker;
use App\Models\Ticket;
use App\Repositories\Ticket\admin\TicketRepository;

trait MakeTicketTrait
{
    /**
     * Create fake instance of Ticket and save it in database
     *
     * @param array $ticketFields
     * @return Ticket
     */
    public function makeTicket($ticketFields = [])
    {
        /** @var TicketRepository $ticketRepo */
        $ticketRepo = App::make(TicketRepository::class);
        $theme = $this->fakeTicketData($ticketFields);
        return $ticketRepo->create($theme);
    }

    /**
     * Get fake instance of Ticket
     *
     * @param array $ticketFields
     * @return Ticket
     */
    public function fakeTicket($ticketFields = [])
    {
        return new Ticket($this->fakeTicketData($ticketFields));
    }

    /**
     * Get fake data of Ticket
     *
     * @param array $postFields
     * @return array
     */
    public function fakeTicketData($ticketFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'id_purchase' => $fake->randomDigitNotNull,
            'for_module' => $fake->word,
            'module_id' => $fake->randomDigitNotNull,
            'sub_module_id' => $fake->randomDigitNotNull,
            'id_user' => $fake->randomDigitNotNull,
            'title_ticket' => $fake->word,
            'ticket_content' => $fake->text,
            'date_publish' => $fake->date('Y-m-d H:i:s'),
            'date_close' => $fake->date('Y-m-d H:i:s'),
            'ticket_status' => $fake->word,
            'id_publisher_close' => $fake->randomDigitNotNull,
            'id_user_close' => $fake->randomDigitNotNull,
            'note_for_close' => $fake->word,
            'validasi' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $ticketFields);
    }
}
