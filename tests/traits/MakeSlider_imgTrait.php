<?php

use Faker\Factory as Faker;
use App\Models\Slider_img;
use App\Repositories\Slider\member\Slider_imgRepository;

trait MakeSlider_imgTrait
{
    /**
     * Create fake instance of Slider_img and save it in database
     *
     * @param array $sliderImgFields
     * @return Slider_img
     */
    public function makeSlider_img($sliderImgFields = [])
    {
        /** @var Slider_imgRepository $sliderImgRepo */
        $sliderImgRepo = App::make(Slider_imgRepository::class);
        $theme = $this->fakeSlider_imgData($sliderImgFields);
        return $sliderImgRepo->create($theme);
    }

    /**
     * Get fake instance of Slider_img
     *
     * @param array $sliderImgFields
     * @return Slider_img
     */
    public function fakeSlider_img($sliderImgFields = [])
    {
        return new Slider_img($this->fakeSlider_imgData($sliderImgFields));
    }

    /**
     * Get fake data of Slider_img
     *
     * @param array $postFields
     * @return array
     */
    public function fakeSlider_imgData($sliderImgFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'id_gallery' => $fake->randomDigitNotNull,
            'img' => $fake->word,
            'thum' => $fake->word,
            'status' => $fake->word
        ], $sliderImgFields);
    }
}
