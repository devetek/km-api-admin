<?php

use Faker\Factory as Faker;
use App\Models\Purchase;
use App\Repositories\Purchase\client\PurchaseRepository;

trait MakePurchaseTrait
{
    /**
     * Create fake instance of Purchase and save it in database
     *
     * @param array $purchaseFields
     * @return Purchase
     */
    public function makePurchase($purchaseFields = [])
    {
        /** @var PurchaseRepository $purchaseRepo */
        $purchaseRepo = App::make(PurchaseRepository::class);
        $theme = $this->fakePurchaseData($purchaseFields);
        return $purchaseRepo->create($theme);
    }

    /**
     * Get fake instance of Purchase
     *
     * @param array $purchaseFields
     * @return Purchase
     */
    public function fakePurchase($purchaseFields = [])
    {
        return new Purchase($this->fakePurchaseData($purchaseFields));
    }

    /**
     * Get fake data of Purchase
     *
     * @param array $postFields
     * @return array
     */
    public function fakePurchaseData($purchaseFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'for_module' => $fake->word,
            'unique_code' => $fake->randomDigitNotNull,
            'module_id' => $fake->randomDigitNotNull,
            'price_purchase' => $fake->randomDigitNotNull,
            'date_purchase' => $fake->date('Y-m-d H:i:s'),
            'id_member' => $fake->randomDigitNotNull,
            'id_user' => $fake->randomDigitNotNull,
            'qty' => $fake->randomDigitNotNull,
            'id_user_accept' => $fake->randomDigitNotNull,
            'date_accept' => $fake->date('Y-m-d H:i:s'),
            'amount_paid' => $fake->randomDigitNotNull,
            'validasi' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $purchaseFields);
    }
}
