<?php

use Faker\Factory as Faker;
use App\Models\BookContent;
use App\Repositories\Book\member\BookContentRepository;

trait MakeBookContentTrait
{
    /**
     * Create fake instance of BookContent and save it in database
     *
     * @param array $bookContentFields
     * @return BookContent
     */
    public function makeBookContent($bookContentFields = [])
    {
        /** @var BookContentRepository $bookContentRepo */
        $bookContentRepo = App::make(BookContentRepository::class);
        $theme = $this->fakeBookContentData($bookContentFields);
        return $bookContentRepo->create($theme);
    }

    /**
     * Get fake instance of BookContent
     *
     * @param array $bookContentFields
     * @return BookContent
     */
    public function fakeBookContent($bookContentFields = [])
    {
        return new BookContent($this->fakeBookContentData($bookContentFields));
    }

    /**
     * Get fake data of BookContent
     *
     * @param array $postFields
     * @return array
     */
    public function fakeBookContentData($bookContentFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'id_book_section' => $fake->randomDigitNotNull,
            'order_book_content' => $fake->randomDigitNotNull,
            'time_book_content' => $fake->randomDigitNotNull,
            'title_book_content' => $fake->word,
            'book_content_type' => $fake->word,
            'book_content' => $fake->text,
            'validasi' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $bookContentFields);
    }
}
