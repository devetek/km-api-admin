<?php

use Faker\Factory as Faker;
use App\Models\Publisher_team_member;
use App\Repositories\Publisher\admin\Publisher_team_memberRepository;

trait MakePublisher_team_memberTrait
{
    /**
     * Create fake instance of Publisher_team_member and save it in database
     *
     * @param array $publisherTeamMemberFields
     * @return Publisher_team_member
     */
    public function makePublisher_team_member($publisherTeamMemberFields = [])
    {
        /** @var Publisher_team_memberRepository $publisherTeamMemberRepo */
        $publisherTeamMemberRepo = App::make(Publisher_team_memberRepository::class);
        $theme = $this->fakePublisher_team_memberData($publisherTeamMemberFields);
        return $publisherTeamMemberRepo->create($theme);
    }

    /**
     * Get fake instance of Publisher_team_member
     *
     * @param array $publisherTeamMemberFields
     * @return Publisher_team_member
     */
    public function fakePublisher_team_member($publisherTeamMemberFields = [])
    {
        return new Publisher_team_member($this->fakePublisher_team_memberData($publisherTeamMemberFields));
    }

    /**
     * Get fake data of Publisher_team_member
     *
     * @param array $postFields
     * @return array
     */
    public function fakePublisher_team_memberData($publisherTeamMemberFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'id_team' => $fake->randomDigitNotNull,
            'id_publisher' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $publisherTeamMemberFields);
    }
}
