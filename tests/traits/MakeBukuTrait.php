<?php

use Faker\Factory as Faker;
use App\Models\Buku;
use App\Repositories\Admin\BukuRepository;

trait MakeBukuTrait
{
    /**
     * Create fake instance of Buku and save it in database
     *
     * @param array $bukuFields
     * @return Buku
     */
    public function makeBuku($bukuFields = [])
    {
        /** @var BukuRepository $bukuRepo */
        $bukuRepo = App::make(BukuRepository::class);
        $theme = $this->fakeBukuData($bukuFields);
        return $bukuRepo->create($theme);
    }

    /**
     * Get fake instance of Buku
     *
     * @param array $bukuFields
     * @return Buku
     */
    public function fakeBuku($bukuFields = [])
    {
        return new Buku($this->fakeBukuData($bukuFields));
    }

    /**
     * Get fake data of Buku
     *
     * @param array $postFields
     * @return array
     */
    public function fakeBukuData($bukuFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'id_pengajar' => $fake->randomDigitNotNull,
            'level' => $fake->word,
            'judul' => $fake->word,
            'deskripsi' => $fake->text,
            'id_user' => $fake->randomDigitNotNull,
            'lama_bejalar' => $fake->word,
            'is_active' => $fake->randomDigitNotNull,
            'validasi' => $fake->randomDigitNotNull,
            'sampul' => $fake->word,
            'background' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $bukuFields);
    }
}
