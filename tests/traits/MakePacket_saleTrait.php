<?php

use Faker\Factory as Faker;
use App\Models\Packet_sale;
use App\Repositories\Packet\admin\Packet_saleRepository;

trait MakePacket_saleTrait
{
    /**
     * Create fake instance of Packet_sale and save it in database
     *
     * @param array $packetSaleFields
     * @return Packet_sale
     */
    public function makePacket_sale($packetSaleFields = [])
    {
        /** @var Packet_saleRepository $packetSaleRepo */
        $packetSaleRepo = App::make(Packet_saleRepository::class);
        $theme = $this->fakePacket_saleData($packetSaleFields);
        return $packetSaleRepo->create($theme);
    }

    /**
     * Get fake instance of Packet_sale
     *
     * @param array $packetSaleFields
     * @return Packet_sale
     */
    public function fakePacket_sale($packetSaleFields = [])
    {
        return new Packet_sale($this->fakePacket_saleData($packetSaleFields));
    }

    /**
     * Get fake data of Packet_sale
     *
     * @param array $postFields
     * @return array
     */
    public function fakePacket_saleData($packetSaleFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'promo_text' => $fake->word,
            'id_packet' => $fake->randomDigitNotNull,
            'efective_date' => $fake->date('Y-m-d H:i:s'),
            'id_user' => $fake->randomDigitNotNull,
            'is_priority' => $fake->randomDigitNotNull,
            'time_support' => $fake->randomDigitNotNull,
            'price_packet_sale' => $fake->randomDigitNotNull,
            'validasi' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $packetSaleFields);
    }
}
