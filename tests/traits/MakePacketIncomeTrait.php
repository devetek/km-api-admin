<?php

use Faker\Factory as Faker;
use App\Models\PacketIncome;
use App\Repositories\Packet\admin\PacketIncomeRepository;

trait MakePacketIncomeTrait
{
    /**
     * Create fake instance of PacketIncome and save it in database
     *
     * @param array $packetIncomeFields
     * @return PacketIncome
     */
    public function makePacketIncome($packetIncomeFields = [])
    {
        /** @var PacketIncomeRepository $packetIncomeRepo */
        $packetIncomeRepo = App::make(PacketIncomeRepository::class);
        $theme = $this->fakePacketIncomeData($packetIncomeFields);
        return $packetIncomeRepo->create($theme);
    }

    /**
     * Get fake instance of PacketIncome
     *
     * @param array $packetIncomeFields
     * @return PacketIncome
     */
    public function fakePacketIncome($packetIncomeFields = [])
    {
        return new PacketIncome($this->fakePacketIncomeData($packetIncomeFields));
    }

    /**
     * Get fake data of PacketIncome
     *
     * @param array $postFields
     * @return array
     */
    public function fakePacketIncomeData($packetIncomeFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'id_packet' => $fake->randomDigitNotNull,
            'name_packet' => $fake->word,
            'excerpt_packet' => $fake->text,
            'description_packet' => $fake->text,
            'id_publisher_team' => $fake->randomDigitNotNull,
            'cover_packet' => $fake->word,
            'thum_packet' => $fake->word,
            'date_publish' => $fake->word,
            'id_company' => $fake->randomDigitNotNull,
            'status_packet' => $fake->word,
            'is_public' => $fake->word,
            'price' => $fake->randomDigitNotNull,
            'validasi' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'id_packet_sale' => $fake->randomDigitNotNull,
            'promo_text' => $fake->word,
            'price_packet_sale' => $fake->randomDigitNotNull,
            'time_support' => $fake->randomDigitNotNull,
            'phone_company' => $fake->word,
            'name_company' => $fake->word,
            'about_company' => $fake->text,
            'id_member_subscribe' => $fake->randomDigitNotNull,
            'amount_paid' => $fake->randomDigitNotNull,
            'date_active' => $fake->date('Y-m-d H:i:s')
        ], $packetIncomeFields);
    }
}
