<?php

use Faker\Factory as Faker;
use App\Models\Member_report;
use App\Repositories\Member\admin\Member_reportRepository;

trait MakeMember_reportTrait
{
    /**
     * Create fake instance of Member_report and save it in database
     *
     * @param array $memberReportFields
     * @return Member_report
     */
    public function makeMember_report($memberReportFields = [])
    {
        /** @var Member_reportRepository $memberReportRepo */
        $memberReportRepo = App::make(Member_reportRepository::class);
        $theme = $this->fakeMember_reportData($memberReportFields);
        return $memberReportRepo->create($theme);
    }

    /**
     * Get fake instance of Member_report
     *
     * @param array $memberReportFields
     * @return Member_report
     */
    public function fakeMember_report($memberReportFields = [])
    {
        return new Member_report($this->fakeMember_reportData($memberReportFields));
    }

    /**
     * Get fake data of Member_report
     *
     * @param array $postFields
     * @return array
     */
    public function fakeMember_reportData($memberReportFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'id_company' => $fake->randomDigitNotNull,
            'name_company' => $fake->word,
            'total' => $fake->word,
            'active' => $fake->word,
            'inactive' => $fake->word
        ], $memberReportFields);
    }
}
