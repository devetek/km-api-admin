<?php

use Faker\Factory as Faker;
use App\Models\PublisherCover;
use App\Repositories\Publisher\admin\PublisherCoverRepository;

trait MakePublisherCoverTrait
{
    /**
     * Create fake instance of PublisherCover and save it in database
     *
     * @param array $publisherCoverFields
     * @return PublisherCover
     */
    public function makePublisherCover($publisherCoverFields = [])
    {
        /** @var PublisherCoverRepository $publisherCoverRepo */
        $publisherCoverRepo = App::make(PublisherCoverRepository::class);
        $theme = $this->fakePublisherCoverData($publisherCoverFields);
        return $publisherCoverRepo->create($theme);
    }

    /**
     * Get fake instance of PublisherCover
     *
     * @param array $publisherCoverFields
     * @return PublisherCover
     */
    public function fakePublisherCover($publisherCoverFields = [])
    {
        return new PublisherCover($this->fakePublisherCoverData($publisherCoverFields));
    }

    /**
     * Get fake data of PublisherCover
     *
     * @param array $postFields
     * @return array
     */
    public function fakePublisherCoverData($publisherCoverFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'first_name' => $fake->word,
            'last_name' => $fake->word,
            'public_name' => $fake->word,
            'title_publisher' => $fake->word,
            'foto_publisher' => $fake->word,
            'thum_publisher' => $fake->word,
            'phone_number' => $fake->word,
            'mail_publisher' => $fake->word,
            'id_company' => $fake->randomDigitNotNull,
            'join_date' => $fake->word,
            'status_publisher' => $fake->word,
            'id_user' => $fake->randomDigitNotNull,
            'validasi' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $publisherCoverFields);
    }
}
