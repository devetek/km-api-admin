<?php

use Faker\Factory as Faker;
use App\Models\Member;
use App\Repositories\Member\client\MemberRepository;

trait MakeMemberTrait
{
    /**
     * Create fake instance of Member and save it in database
     *
     * @param array $memberFields
     * @return Member
     */
    public function makeMember($memberFields = [])
    {
        /** @var MemberRepository $memberRepo */
        $memberRepo = App::make(MemberRepository::class);
        $theme = $this->fakeMemberData($memberFields);
        return $memberRepo->create($theme);
    }

    /**
     * Get fake instance of Member
     *
     * @param array $memberFields
     * @return Member
     */
    public function fakeMember($memberFields = [])
    {
        return new Member($this->fakeMemberData($memberFields));
    }

    /**
     * Get fake data of Member
     *
     * @param array $postFields
     * @return array
     */
    public function fakeMemberData($memberFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'id_company' => $fake->randomDigitNotNull,
            'first_name' => $fake->word,
            'last_name' => $fake->word,
            'tgl_lahir' => $fake->word,
            'last_education' => $fake->word,
            'phone_member' => $fake->word,
            'poscode_member' => $fake->word,
            'email_member' => $fake->word,
            'foto_member' => $fake->word,
            'thum_member' => $fake->word,
            'address_member' => $fake->word,
            'status_member' => $fake->word,
            'register_form' => $fake->word,
            'validasi' => $fake->randomDigitNotNull,
            'id_user' => $fake->randomDigitNotNull,
            'id_user_rgister' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $memberFields);
    }
}
