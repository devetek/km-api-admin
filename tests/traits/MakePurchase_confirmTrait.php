<?php

use Faker\Factory as Faker;
use App\Models\Purchase_confirm;
use App\Repositories\Client_Page\web\Purchase_confirmRepository;

trait MakePurchase_confirmTrait
{
    /**
     * Create fake instance of Purchase_confirm and save it in database
     *
     * @param array $purchaseConfirmFields
     * @return Purchase_confirm
     */
    public function makePurchase_confirm($purchaseConfirmFields = [])
    {
        /** @var Purchase_confirmRepository $purchaseConfirmRepo */
        $purchaseConfirmRepo = App::make(Purchase_confirmRepository::class);
        $theme = $this->fakePurchase_confirmData($purchaseConfirmFields);
        return $purchaseConfirmRepo->create($theme);
    }

    /**
     * Get fake instance of Purchase_confirm
     *
     * @param array $purchaseConfirmFields
     * @return Purchase_confirm
     */
    public function fakePurchase_confirm($purchaseConfirmFields = [])
    {
        return new Purchase_confirm($this->fakePurchase_confirmData($purchaseConfirmFields));
    }

    /**
     * Get fake data of Purchase_confirm
     *
     * @param array $postFields
     * @return array
     */
    public function fakePurchase_confirmData($purchaseConfirmFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'id_purchase' => $fake->randomDigitNotNull,
            'date_confirm' => $fake->date('Y-m-d H:i:s'),
            'transfer_form_chanel' => $fake->word,
            'transfer_form_name' => $fake->word,
            'value_paid' => $fake->randomDigitNotNull,
            'foto_confirm' => $fake->word,
            'validasi' => $fake->randomDigitNotNull,
            'id_user' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $purchaseConfirmFields);
    }
}
