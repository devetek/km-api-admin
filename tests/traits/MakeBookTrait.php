<?php

use Faker\Factory as Faker;
use App\Models\Book;
use App\Repositories\Book\client\BookRepository;

trait MakeBookTrait
{
    /**
     * Create fake instance of Book and save it in database
     *
     * @param array $bookFields
     * @return Book
     */
    public function makeBook($bookFields = [])
    {
        /** @var BookRepository $bookRepo */
        $bookRepo = App::make(BookRepository::class);
        $theme = $this->fakeBookData($bookFields);
        return $bookRepo->create($theme);
    }

    /**
     * Get fake instance of Book
     *
     * @param array $bookFields
     * @return Book
     */
    public function fakeBook($bookFields = [])
    {
        return new Book($this->fakeBookData($bookFields));
    }

    /**
     * Get fake data of Book
     *
     * @param array $postFields
     * @return array
     */
    public function fakeBookData($bookFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'title_book' => $fake->word,
            'org_price' => $fake->randomDigitNotNull,
            'actual_price' => $fake->randomDigitNotNull,
            'description' => $fake->text,
            'promotional_text' => $fake->text,
            'id_publisher' => $fake->randomDigitNotNull,
            'id_team' => $fake->randomDigitNotNull,
            'id_user' => $fake->randomDigitNotNull,
            'cover_url' => $fake->word,
            'thum_cover_url' => $fake->word,
            'date_publish' => $fake->word,
            'status' => $fake->word,
            'validasi' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $bookFields);
    }
}
