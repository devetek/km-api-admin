<?php

use Faker\Factory as Faker;
use App\Models\Packet_requirement;
use App\Repositories\Packet\client\Packet_requirementRepository;

trait MakePacket_requirementTrait
{
    /**
     * Create fake instance of Packet_requirement and save it in database
     *
     * @param array $packetRequirementFields
     * @return Packet_requirement
     */
    public function makePacket_requirement($packetRequirementFields = [])
    {
        /** @var Packet_requirementRepository $packetRequirementRepo */
        $packetRequirementRepo = App::make(Packet_requirementRepository::class);
        $theme = $this->fakePacket_requirementData($packetRequirementFields);
        return $packetRequirementRepo->create($theme);
    }

    /**
     * Get fake instance of Packet_requirement
     *
     * @param array $packetRequirementFields
     * @return Packet_requirement
     */
    public function fakePacket_requirement($packetRequirementFields = [])
    {
        return new Packet_requirement($this->fakePacket_requirementData($packetRequirementFields));
    }

    /**
     * Get fake data of Packet_requirement
     *
     * @param array $postFields
     * @return array
     */
    public function fakePacket_requirementData($packetRequirementFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'id_packet' => $fake->randomDigitNotNull,
            'order_requirement' => $fake->randomDigitNotNull,
            'icon' => $fake->word,
            'validasi' => $fake->randomDigitNotNull,
            'requirement' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $packetRequirementFields);
    }
}
