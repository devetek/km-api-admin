<?php

use Faker\Factory as Faker;
use App\Models\Member_cover;
use App\Repositories\Member\admin\Member_coverRepository;

trait MakeMember_coverTrait
{
    /**
     * Create fake instance of Member_cover and save it in database
     *
     * @param array $memberCoverFields
     * @return Member_cover
     */
    public function makeMember_cover($memberCoverFields = [])
    {
        /** @var Member_coverRepository $memberCoverRepo */
        $memberCoverRepo = App::make(Member_coverRepository::class);
        $theme = $this->fakeMember_coverData($memberCoverFields);
        return $memberCoverRepo->create($theme);
    }

    /**
     * Get fake instance of Member_cover
     *
     * @param array $memberCoverFields
     * @return Member_cover
     */
    public function fakeMember_cover($memberCoverFields = [])
    {
        return new Member_cover($this->fakeMember_coverData($memberCoverFields));
    }

    /**
     * Get fake data of Member_cover
     *
     * @param array $postFields
     * @return array
     */
    public function fakeMember_coverData($memberCoverFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'id_company' => $fake->randomDigitNotNull,
            'first_name' => $fake->word,
            'last_name' => $fake->word,
            'last_education' => $fake->word,
            'phone_member' => $fake->word,
            'poscode_member' => $fake->word,
            'email_member' => $fake->word,
            'foto_member' => $fake->word,
            'thum_member' => $fake->word,
            'address_member' => $fake->word,
            'status_member' => $fake->word,
            'register_form' => $fake->word,
            'validasi' => $fake->randomDigitNotNull,
            'id_user' => $fake->randomDigitNotNull,
            'id_user_rgister' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $memberCoverFields);
    }
}
