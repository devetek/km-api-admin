<?php

use Faker\Factory as Faker;
use App\Models\Packet_requirements;
use App\Repositories\Packet\member_Guest\Packet_requirementsRepository;

trait MakePacket_requirementsTrait
{
    /**
     * Create fake instance of Packet_requirements and save it in database
     *
     * @param array $packetRequirementsFields
     * @return Packet_requirements
     */
    public function makePacket_requirements($packetRequirementsFields = [])
    {
        /** @var Packet_requirementsRepository $packetRequirementsRepo */
        $packetRequirementsRepo = App::make(Packet_requirementsRepository::class);
        $theme = $this->fakePacket_requirementsData($packetRequirementsFields);
        return $packetRequirementsRepo->create($theme);
    }

    /**
     * Get fake instance of Packet_requirements
     *
     * @param array $packetRequirementsFields
     * @return Packet_requirements
     */
    public function fakePacket_requirements($packetRequirementsFields = [])
    {
        return new Packet_requirements($this->fakePacket_requirementsData($packetRequirementsFields));
    }

    /**
     * Get fake data of Packet_requirements
     *
     * @param array $postFields
     * @return array
     */
    public function fakePacket_requirementsData($packetRequirementsFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'id_paket' => $fake->randomDigitNotNull,
            'order_requirement' => $fake->randomDigitNotNull,
            'requirement' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $packetRequirementsFields);
    }
}
