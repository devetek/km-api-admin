<?php

use Faker\Factory as Faker;
use App\Models\Content;
use App\Repositories\Book\client\ContentRepository;

trait MakeContentTrait
{
    /**
     * Create fake instance of Content and save it in database
     *
     * @param array $contentFields
     * @return Content
     */
    public function makeContent($contentFields = [])
    {
        /** @var ContentRepository $contentRepo */
        $contentRepo = App::make(ContentRepository::class);
        $theme = $this->fakeContentData($contentFields);
        return $contentRepo->create($theme);
    }

    /**
     * Get fake instance of Content
     *
     * @param array $contentFields
     * @return Content
     */
    public function fakeContent($contentFields = [])
    {
        return new Content($this->fakeContentData($contentFields));
    }

    /**
     * Get fake data of Content
     *
     * @param array $postFields
     * @return array
     */
    public function fakeContentData($contentFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'id_book_section' => $fake->randomDigitNotNull,
            'order_book_content' => $fake->randomDigitNotNull,
            'time_book_content' => $fake->randomDigitNotNull,
            'title_book_content' => $fake->word,
            'book_content_type' => $fake->word,
            'book_content' => $fake->text,
            'validasi' => $fake->randomDigitNotNull,
            'id_user' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $contentFields);
    }
}
