<?php

use Faker\Factory as Faker;
use App\Models\Slider;
use App\Repositories\Slider\member\SliderRepository;

trait MakeSliderTrait
{
    /**
     * Create fake instance of Slider and save it in database
     *
     * @param array $sliderFields
     * @return Slider
     */
    public function makeSlider($sliderFields = [])
    {
        /** @var SliderRepository $sliderRepo */
        $sliderRepo = App::make(SliderRepository::class);
        $theme = $this->fakeSliderData($sliderFields);
        return $sliderRepo->create($theme);
    }

    /**
     * Get fake instance of Slider
     *
     * @param array $sliderFields
     * @return Slider
     */
    public function fakeSlider($sliderFields = [])
    {
        return new Slider($this->fakeSliderData($sliderFields));
    }

    /**
     * Get fake data of Slider
     *
     * @param array $postFields
     * @return array
     */
    public function fakeSliderData($sliderFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'for_module' => $fake->word,
            'for_id_module' => $fake->randomDigitNotNull,
            'name_galley' => $fake->word,
            'code' => $fake->word,
            'id_user' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $sliderFields);
    }
}
