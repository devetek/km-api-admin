<?php

use Faker\Factory as Faker;
use App\Models\Purchase_packet;
use App\Repositories\Member\admin\Purchase_packetRepository;

trait MakePurchase_packetTrait
{
    /**
     * Create fake instance of Purchase_packet and save it in database
     *
     * @param array $purchasePacketFields
     * @return Purchase_packet
     */
    public function makePurchase_packet($purchasePacketFields = [])
    {
        /** @var Purchase_packetRepository $purchasePacketRepo */
        $purchasePacketRepo = App::make(Purchase_packetRepository::class);
        $theme = $this->fakePurchase_packetData($purchasePacketFields);
        return $purchasePacketRepo->create($theme);
    }

    /**
     * Get fake instance of Purchase_packet
     *
     * @param array $purchasePacketFields
     * @return Purchase_packet
     */
    public function fakePurchase_packet($purchasePacketFields = [])
    {
        return new Purchase_packet($this->fakePurchase_packetData($purchasePacketFields));
    }

    /**
     * Get fake data of Purchase_packet
     *
     * @param array $postFields
     * @return array
     */
    public function fakePurchase_packetData($purchasePacketFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'for_module' => $fake->word,
            'unique_code' => $fake->randomDigitNotNull,
            'module_id' => $fake->randomDigitNotNull,
            'price_purchase' => $fake->randomDigitNotNull,
            'date_purchase' => $fake->date('Y-m-d H:i:s'),
            'id_member' => $fake->randomDigitNotNull,
            'id_user' => $fake->randomDigitNotNull,
            'qty' => $fake->randomDigitNotNull,
            'id_user_accept' => $fake->randomDigitNotNull,
            'date_accept' => $fake->date('Y-m-d H:i:s'),
            'amount_paid' => $fake->randomDigitNotNull,
            'validasi' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $purchasePacketFields);
    }
}
