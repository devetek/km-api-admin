<?php

use Faker\Factory as Faker;
use App\Models\BookCover;
use App\Repositories\Book\admin\BookCoverRepository;

trait MakeBookCoverTrait
{
    /**
     * Create fake instance of BookCover and save it in database
     *
     * @param array $bookCoverFields
     * @return BookCover
     */
    public function makeBookCover($bookCoverFields = [])
    {
        /** @var BookCoverRepository $bookCoverRepo */
        $bookCoverRepo = App::make(BookCoverRepository::class);
        $theme = $this->fakeBookCoverData($bookCoverFields);
        return $bookCoverRepo->create($theme);
    }

    /**
     * Get fake instance of BookCover
     *
     * @param array $bookCoverFields
     * @return BookCover
     */
    public function fakeBookCover($bookCoverFields = [])
    {
        return new BookCover($this->fakeBookCoverData($bookCoverFields));
    }

    /**
     * Get fake data of BookCover
     *
     * @param array $postFields
     * @return array
     */
    public function fakeBookCoverData($bookCoverFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'title_book' => $fake->word,
            'org_price' => $fake->randomDigitNotNull,
            'actual_price' => $fake->randomDigitNotNull,
            'description' => $fake->text,
            'promotional_text' => $fake->text,
            'id_publisher' => $fake->randomDigitNotNull,
            'id_user' => $fake->randomDigitNotNull,
            'cover_url' => $fake->word,
            'thum_cover_url' => $fake->word,
            'date_publish' => $fake->word,
            'status' => $fake->word,
            'validasi' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $bookCoverFields);
    }
}
