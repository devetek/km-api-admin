<?php

use App\Models\Packet_sale;
use App\Repositories\Packet\admin\Packet_saleRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Packet_saleRepositoryTest extends TestCase
{
    use MakePacket_saleTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Packet_saleRepository
     */
    protected $packetSaleRepo;

    public function setUp()
    {
        parent::setUp();
        $this->packetSaleRepo = App::make(Packet_saleRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePacket_sale()
    {
        $packetSale = $this->fakePacket_saleData();
        $createdPacket_sale = $this->packetSaleRepo->create($packetSale);
        $createdPacket_sale = $createdPacket_sale->toArray();
        $this->assertArrayHasKey('id', $createdPacket_sale);
        $this->assertNotNull($createdPacket_sale['id'], 'Created Packet_sale must have id specified');
        $this->assertNotNull(Packet_sale::find($createdPacket_sale['id']), 'Packet_sale with given id must be in DB');
        $this->assertModelData($packetSale, $createdPacket_sale);
    }

    /**
     * @test read
     */
    public function testReadPacket_sale()
    {
        $packetSale = $this->makePacket_sale();
        $dbPacket_sale = $this->packetSaleRepo->find($packetSale->id_packet_sale);
        $dbPacket_sale = $dbPacket_sale->toArray();
        $this->assertModelData($packetSale->toArray(), $dbPacket_sale);
    }

    /**
     * @test update
     */
    public function testUpdatePacket_sale()
    {
        $packetSale = $this->makePacket_sale();
        $fakePacket_sale = $this->fakePacket_saleData();
        $updatedPacket_sale = $this->packetSaleRepo->update($fakePacket_sale, $packetSale->id_packet_sale);
        $this->assertModelData($fakePacket_sale, $updatedPacket_sale->toArray());
        $dbPacket_sale = $this->packetSaleRepo->find($packetSale->id_packet_sale);
        $this->assertModelData($fakePacket_sale, $dbPacket_sale->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePacket_sale()
    {
        $packetSale = $this->makePacket_sale();
        $resp = $this->packetSaleRepo->delete($packetSale->id_packet_sale);
        $this->assertTrue($resp);
        $this->assertNull(Packet_sale::find($packetSale->id_packet_sale), 'Packet_sale should not exist in DB');
    }
}
