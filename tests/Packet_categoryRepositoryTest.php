<?php

use App\Models\Packet_category;
use App\Repositories\Packet\client\Packet_categoryRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Packet_categoryRepositoryTest extends TestCase
{
    use MakePacket_categoryTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Packet_categoryRepository
     */
    protected $packetCategoryRepo;

    public function setUp()
    {
        parent::setUp();
        $this->packetCategoryRepo = App::make(Packet_categoryRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePacket_category()
    {
        $packetCategory = $this->fakePacket_categoryData();
        $createdPacket_category = $this->packetCategoryRepo->create($packetCategory);
        $createdPacket_category = $createdPacket_category->toArray();
        $this->assertArrayHasKey('id', $createdPacket_category);
        $this->assertNotNull($createdPacket_category['id'], 'Created Packet_category must have id specified');
        $this->assertNotNull(Packet_category::find($createdPacket_category['id']), 'Packet_category with given id must be in DB');
        $this->assertModelData($packetCategory, $createdPacket_category);
    }

    /**
     * @test read
     */
    public function testReadPacket_category()
    {
        $packetCategory = $this->makePacket_category();
        $dbPacket_category = $this->packetCategoryRepo->find($packetCategory->id_packet_category);
        $dbPacket_category = $dbPacket_category->toArray();
        $this->assertModelData($packetCategory->toArray(), $dbPacket_category);
    }

    /**
     * @test update
     */
    public function testUpdatePacket_category()
    {
        $packetCategory = $this->makePacket_category();
        $fakePacket_category = $this->fakePacket_categoryData();
        $updatedPacket_category = $this->packetCategoryRepo->update($fakePacket_category, $packetCategory->id_packet_category);
        $this->assertModelData($fakePacket_category, $updatedPacket_category->toArray());
        $dbPacket_category = $this->packetCategoryRepo->find($packetCategory->id_packet_category);
        $this->assertModelData($fakePacket_category, $dbPacket_category->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePacket_category()
    {
        $packetCategory = $this->makePacket_category();
        $resp = $this->packetCategoryRepo->delete($packetCategory->id_packet_category);
        $this->assertTrue($resp);
        $this->assertNull(Packet_category::find($packetCategory->id_packet_category), 'Packet_category should not exist in DB');
    }
}
