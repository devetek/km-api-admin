<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Purchase_confirmApiTest extends TestCase
{
    use MakePurchase_confirmTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePurchase_confirm()
    {
        $purchaseConfirm = $this->fakePurchase_confirmData();
        $this->json('POST', '/api/v1/purchaseConfirms', $purchaseConfirm);

        $this->assertApiResponse($purchaseConfirm);
    }

    /**
     * @test
     */
    public function testReadPurchase_confirm()
    {
        $purchaseConfirm = $this->makePurchase_confirm();
        $this->json('GET', '/api/v1/purchaseConfirms/'.$purchaseConfirm->id_purchase_confirm);

        $this->assertApiResponse($purchaseConfirm->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePurchase_confirm()
    {
        $purchaseConfirm = $this->makePurchase_confirm();
        $editedPurchase_confirm = $this->fakePurchase_confirmData();

        $this->json('PUT', '/api/v1/purchaseConfirms/'.$purchaseConfirm->id_purchase_confirm, $editedPurchase_confirm);

        $this->assertApiResponse($editedPurchase_confirm);
    }

    /**
     * @test
     */
    public function testDeletePurchase_confirm()
    {
        $purchaseConfirm = $this->makePurchase_confirm();
        $this->json('DELETE', '/api/v1/purchaseConfirms/'.$purchaseConfirm->id_purchase_confirm);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/purchaseConfirms/'.$purchaseConfirm->id_purchase_confirm);

        $this->assertResponseStatus(404);
    }
}
