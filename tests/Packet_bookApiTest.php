<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Packet_bookApiTest extends TestCase
{
    use MakePacket_bookTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePacket_book()
    {
        $packetBook = $this->fakePacket_bookData();
        $this->json('POST', '/api/v1/packetBooks', $packetBook);

        $this->assertApiResponse($packetBook);
    }

    /**
     * @test
     */
    public function testReadPacket_book()
    {
        $packetBook = $this->makePacket_book();
        $this->json('GET', '/api/v1/packetBooks/'.$packetBook->id_packet_book);

        $this->assertApiResponse($packetBook->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePacket_book()
    {
        $packetBook = $this->makePacket_book();
        $editedPacket_book = $this->fakePacket_bookData();

        $this->json('PUT', '/api/v1/packetBooks/'.$packetBook->id_packet_book, $editedPacket_book);

        $this->assertApiResponse($editedPacket_book);
    }

    /**
     * @test
     */
    public function testDeletePacket_book()
    {
        $packetBook = $this->makePacket_book();
        $this->json('DELETE', '/api/v1/packetBooks/'.$packetBook->id_packet_book);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/packetBooks/'.$packetBook->id_packet_book);

        $this->assertResponseStatus(404);
    }
}
