<?php

use App\Models\BookAdmin;
use App\Repositories\Book\BookAdminRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BookAdminRepositoryTest extends TestCase
{
    use MakeBookAdminTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var BookAdminRepository
     */
    protected $bookAdminRepo;

    public function setUp()
    {
        parent::setUp();
        $this->bookAdminRepo = App::make(BookAdminRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateBookAdmin()
    {
        $bookAdmin = $this->fakeBookAdminData();
        $createdBookAdmin = $this->bookAdminRepo->create($bookAdmin);
        $createdBookAdmin = $createdBookAdmin->toArray();
        $this->assertArrayHasKey('id', $createdBookAdmin);
        $this->assertNotNull($createdBookAdmin['id'], 'Created BookAdmin must have id specified');
        $this->assertNotNull(BookAdmin::find($createdBookAdmin['id']), 'BookAdmin with given id must be in DB');
        $this->assertModelData($bookAdmin, $createdBookAdmin);
    }

    /**
     * @test read
     */
    public function testReadBookAdmin()
    {
        $bookAdmin = $this->makeBookAdmin();
        $dbBookAdmin = $this->bookAdminRepo->find($bookAdmin->id_book);
        $dbBookAdmin = $dbBookAdmin->toArray();
        $this->assertModelData($bookAdmin->toArray(), $dbBookAdmin);
    }

    /**
     * @test update
     */
    public function testUpdateBookAdmin()
    {
        $bookAdmin = $this->makeBookAdmin();
        $fakeBookAdmin = $this->fakeBookAdminData();
        $updatedBookAdmin = $this->bookAdminRepo->update($fakeBookAdmin, $bookAdmin->id_book);
        $this->assertModelData($fakeBookAdmin, $updatedBookAdmin->toArray());
        $dbBookAdmin = $this->bookAdminRepo->find($bookAdmin->id_book);
        $this->assertModelData($fakeBookAdmin, $dbBookAdmin->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteBookAdmin()
    {
        $bookAdmin = $this->makeBookAdmin();
        $resp = $this->bookAdminRepo->delete($bookAdmin->id_book);
        $this->assertTrue($resp);
        $this->assertNull(BookAdmin::find($bookAdmin->id_book), 'BookAdmin should not exist in DB');
    }
}
