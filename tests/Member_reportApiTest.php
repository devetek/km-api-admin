<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Member_reportApiTest extends TestCase
{
    use MakeMember_reportTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateMember_report()
    {
        $memberReport = $this->fakeMember_reportData();
        $this->json('POST', '/api/v1/memberReports', $memberReport);

        $this->assertApiResponse($memberReport);
    }

    /**
     * @test
     */
    public function testReadMember_report()
    {
        $memberReport = $this->makeMember_report();
        $this->json('GET', '/api/v1/memberReports/'.$memberReport->id_company);

        $this->assertApiResponse($memberReport->toArray());
    }

    /**
     * @test
     */
    public function testUpdateMember_report()
    {
        $memberReport = $this->makeMember_report();
        $editedMember_report = $this->fakeMember_reportData();

        $this->json('PUT', '/api/v1/memberReports/'.$memberReport->id_company, $editedMember_report);

        $this->assertApiResponse($editedMember_report);
    }

    /**
     * @test
     */
    public function testDeleteMember_report()
    {
        $memberReport = $this->makeMember_report();
        $this->json('DELETE', '/api/v1/memberReports/'.$memberReport->id_company);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/memberReports/'.$memberReport->id_company);

        $this->assertResponseStatus(404);
    }
}
