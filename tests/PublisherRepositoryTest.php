<?php

use App\Models\Publisher;
use App\Repositories\Publisher\admin\PublisherRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PublisherRepositoryTest extends TestCase
{
    use MakePublisherTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PublisherRepository
     */
    protected $publisherRepo;

    public function setUp()
    {
        parent::setUp();
        $this->publisherRepo = App::make(PublisherRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePublisher()
    {
        $publisher = $this->fakePublisherData();
        $createdPublisher = $this->publisherRepo->create($publisher);
        $createdPublisher = $createdPublisher->toArray();
        $this->assertArrayHasKey('id', $createdPublisher);
        $this->assertNotNull($createdPublisher['id'], 'Created Publisher must have id specified');
        $this->assertNotNull(Publisher::find($createdPublisher['id']), 'Publisher with given id must be in DB');
        $this->assertModelData($publisher, $createdPublisher);
    }

    /**
     * @test read
     */
    public function testReadPublisher()
    {
        $publisher = $this->makePublisher();
        $dbPublisher = $this->publisherRepo->find($publisher->id_publisher);
        $dbPublisher = $dbPublisher->toArray();
        $this->assertModelData($publisher->toArray(), $dbPublisher);
    }

    /**
     * @test update
     */
    public function testUpdatePublisher()
    {
        $publisher = $this->makePublisher();
        $fakePublisher = $this->fakePublisherData();
        $updatedPublisher = $this->publisherRepo->update($fakePublisher, $publisher->id_publisher);
        $this->assertModelData($fakePublisher, $updatedPublisher->toArray());
        $dbPublisher = $this->publisherRepo->find($publisher->id_publisher);
        $this->assertModelData($fakePublisher, $dbPublisher->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePublisher()
    {
        $publisher = $this->makePublisher();
        $resp = $this->publisherRepo->delete($publisher->id_publisher);
        $this->assertTrue($resp);
        $this->assertNull(Publisher::find($publisher->id_publisher), 'Publisher should not exist in DB');
    }
}
