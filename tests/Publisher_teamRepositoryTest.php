<?php

use App\Models\Publisher_team;
use App\Repositories\Publisher\admin\Publisher_teamRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Publisher_teamRepositoryTest extends TestCase
{
    use MakePublisher_teamTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Publisher_teamRepository
     */
    protected $publisherTeamRepo;

    public function setUp()
    {
        parent::setUp();
        $this->publisherTeamRepo = App::make(Publisher_teamRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePublisher_team()
    {
        $publisherTeam = $this->fakePublisher_teamData();
        $createdPublisher_team = $this->publisherTeamRepo->create($publisherTeam);
        $createdPublisher_team = $createdPublisher_team->toArray();
        $this->assertArrayHasKey('id', $createdPublisher_team);
        $this->assertNotNull($createdPublisher_team['id'], 'Created Publisher_team must have id specified');
        $this->assertNotNull(Publisher_team::find($createdPublisher_team['id']), 'Publisher_team with given id must be in DB');
        $this->assertModelData($publisherTeam, $createdPublisher_team);
    }

    /**
     * @test read
     */
    public function testReadPublisher_team()
    {
        $publisherTeam = $this->makePublisher_team();
        $dbPublisher_team = $this->publisherTeamRepo->find($publisherTeam->id_publisher_team);
        $dbPublisher_team = $dbPublisher_team->toArray();
        $this->assertModelData($publisherTeam->toArray(), $dbPublisher_team);
    }

    /**
     * @test update
     */
    public function testUpdatePublisher_team()
    {
        $publisherTeam = $this->makePublisher_team();
        $fakePublisher_team = $this->fakePublisher_teamData();
        $updatedPublisher_team = $this->publisherTeamRepo->update($fakePublisher_team, $publisherTeam->id_publisher_team);
        $this->assertModelData($fakePublisher_team, $updatedPublisher_team->toArray());
        $dbPublisher_team = $this->publisherTeamRepo->find($publisherTeam->id_publisher_team);
        $this->assertModelData($fakePublisher_team, $dbPublisher_team->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePublisher_team()
    {
        $publisherTeam = $this->makePublisher_team();
        $resp = $this->publisherTeamRepo->delete($publisherTeam->id_publisher_team);
        $this->assertTrue($resp);
        $this->assertNull(Publisher_team::find($publisherTeam->id_publisher_team), 'Publisher_team should not exist in DB');
    }
}
