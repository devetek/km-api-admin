<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PublisherApiTest extends TestCase
{
    use MakePublisherTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePublisher()
    {
        $publisher = $this->fakePublisherData();
        $this->json('POST', '/api/v1/publishers', $publisher);

        $this->assertApiResponse($publisher);
    }

    /**
     * @test
     */
    public function testReadPublisher()
    {
        $publisher = $this->makePublisher();
        $this->json('GET', '/api/v1/publishers/'.$publisher->id_publisher);

        $this->assertApiResponse($publisher->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePublisher()
    {
        $publisher = $this->makePublisher();
        $editedPublisher = $this->fakePublisherData();

        $this->json('PUT', '/api/v1/publishers/'.$publisher->id_publisher, $editedPublisher);

        $this->assertApiResponse($editedPublisher);
    }

    /**
     * @test
     */
    public function testDeletePublisher()
    {
        $publisher = $this->makePublisher();
        $this->json('DELETE', '/api/v1/publishers/'.$publisher->id_publisher);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/publishers/'.$publisher->id_publisher);

        $this->assertResponseStatus(404);
    }
}
