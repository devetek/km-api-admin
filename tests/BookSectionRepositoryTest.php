<?php

use App\Models\BookSection;
use App\Repositories\Book\member\BookSectionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BookSectionRepositoryTest extends TestCase
{
    use MakeBookSectionTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var BookSectionRepository
     */
    protected $bookSectionRepo;

    public function setUp()
    {
        parent::setUp();
        $this->bookSectionRepo = App::make(BookSectionRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateBookSection()
    {
        $bookSection = $this->fakeBookSectionData();
        $createdBookSection = $this->bookSectionRepo->create($bookSection);
        $createdBookSection = $createdBookSection->toArray();
        $this->assertArrayHasKey('id', $createdBookSection);
        $this->assertNotNull($createdBookSection['id'], 'Created BookSection must have id specified');
        $this->assertNotNull(BookSection::find($createdBookSection['id']), 'BookSection with given id must be in DB');
        $this->assertModelData($bookSection, $createdBookSection);
    }

    /**
     * @test read
     */
    public function testReadBookSection()
    {
        $bookSection = $this->makeBookSection();
        $dbBookSection = $this->bookSectionRepo->find($bookSection->id_book_section);
        $dbBookSection = $dbBookSection->toArray();
        $this->assertModelData($bookSection->toArray(), $dbBookSection);
    }

    /**
     * @test update
     */
    public function testUpdateBookSection()
    {
        $bookSection = $this->makeBookSection();
        $fakeBookSection = $this->fakeBookSectionData();
        $updatedBookSection = $this->bookSectionRepo->update($fakeBookSection, $bookSection->id_book_section);
        $this->assertModelData($fakeBookSection, $updatedBookSection->toArray());
        $dbBookSection = $this->bookSectionRepo->find($bookSection->id_book_section);
        $this->assertModelData($fakeBookSection, $dbBookSection->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteBookSection()
    {
        $bookSection = $this->makeBookSection();
        $resp = $this->bookSectionRepo->delete($bookSection->id_book_section);
        $this->assertTrue($resp);
        $this->assertNull(BookSection::find($bookSection->id_book_section), 'BookSection should not exist in DB');
    }
}
