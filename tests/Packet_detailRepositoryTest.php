<?php

use App\Models\Packet_detail;
use App\Repositories\Packet\client\Packet_detailRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Packet_detailRepositoryTest extends TestCase
{
    use MakePacket_detailTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Packet_detailRepository
     */
    protected $packetDetailRepo;

    public function setUp()
    {
        parent::setUp();
        $this->packetDetailRepo = App::make(Packet_detailRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePacket_detail()
    {
        $packetDetail = $this->fakePacket_detailData();
        $createdPacket_detail = $this->packetDetailRepo->create($packetDetail);
        $createdPacket_detail = $createdPacket_detail->toArray();
        $this->assertArrayHasKey('id', $createdPacket_detail);
        $this->assertNotNull($createdPacket_detail['id'], 'Created Packet_detail must have id specified');
        $this->assertNotNull(Packet_detail::find($createdPacket_detail['id']), 'Packet_detail with given id must be in DB');
        $this->assertModelData($packetDetail, $createdPacket_detail);
    }

    /**
     * @test read
     */
    public function testReadPacket_detail()
    {
        $packetDetail = $this->makePacket_detail();
        $dbPacket_detail = $this->packetDetailRepo->find($packetDetail->id_packet_detail);
        $dbPacket_detail = $dbPacket_detail->toArray();
        $this->assertModelData($packetDetail->toArray(), $dbPacket_detail);
    }

    /**
     * @test update
     */
    public function testUpdatePacket_detail()
    {
        $packetDetail = $this->makePacket_detail();
        $fakePacket_detail = $this->fakePacket_detailData();
        $updatedPacket_detail = $this->packetDetailRepo->update($fakePacket_detail, $packetDetail->id_packet_detail);
        $this->assertModelData($fakePacket_detail, $updatedPacket_detail->toArray());
        $dbPacket_detail = $this->packetDetailRepo->find($packetDetail->id_packet_detail);
        $this->assertModelData($fakePacket_detail, $dbPacket_detail->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePacket_detail()
    {
        $packetDetail = $this->makePacket_detail();
        $resp = $this->packetDetailRepo->delete($packetDetail->id_packet_detail);
        $this->assertTrue($resp);
        $this->assertNull(Packet_detail::find($packetDetail->id_packet_detail), 'Packet_detail should not exist in DB');
    }
}
