<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Packet_requirementApiTest extends TestCase
{
    use MakePacket_requirementTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePacket_requirement()
    {
        $packetRequirement = $this->fakePacket_requirementData();
        $this->json('POST', '/api/v1/packetRequirements', $packetRequirement);

        $this->assertApiResponse($packetRequirement);
    }

    /**
     * @test
     */
    public function testReadPacket_requirement()
    {
        $packetRequirement = $this->makePacket_requirement();
        $this->json('GET', '/api/v1/packetRequirements/'.$packetRequirement->id_packet_requirements);

        $this->assertApiResponse($packetRequirement->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePacket_requirement()
    {
        $packetRequirement = $this->makePacket_requirement();
        $editedPacket_requirement = $this->fakePacket_requirementData();

        $this->json('PUT', '/api/v1/packetRequirements/'.$packetRequirement->id_packet_requirements, $editedPacket_requirement);

        $this->assertApiResponse($editedPacket_requirement);
    }

    /**
     * @test
     */
    public function testDeletePacket_requirement()
    {
        $packetRequirement = $this->makePacket_requirement();
        $this->json('DELETE', '/api/v1/packetRequirements/'.$packetRequirement->id_packet_requirements);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/packetRequirements/'.$packetRequirement->id_packet_requirements);

        $this->assertResponseStatus(404);
    }
}
