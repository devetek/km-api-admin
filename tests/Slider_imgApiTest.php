<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Slider_imgApiTest extends TestCase
{
    use MakeSlider_imgTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateSlider_img()
    {
        $sliderImg = $this->fakeSlider_imgData();
        $this->json('POST', '/api/v1/sliderImgs', $sliderImg);

        $this->assertApiResponse($sliderImg);
    }

    /**
     * @test
     */
    public function testReadSlider_img()
    {
        $sliderImg = $this->makeSlider_img();
        $this->json('GET', '/api/v1/sliderImgs/'.$sliderImg->id_gallery_img);

        $this->assertApiResponse($sliderImg->toArray());
    }

    /**
     * @test
     */
    public function testUpdateSlider_img()
    {
        $sliderImg = $this->makeSlider_img();
        $editedSlider_img = $this->fakeSlider_imgData();

        $this->json('PUT', '/api/v1/sliderImgs/'.$sliderImg->id_gallery_img, $editedSlider_img);

        $this->assertApiResponse($editedSlider_img);
    }

    /**
     * @test
     */
    public function testDeleteSlider_img()
    {
        $sliderImg = $this->makeSlider_img();
        $this->json('DELETE', '/api/v1/sliderImgs/'.$sliderImg->id_gallery_img);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/sliderImgs/'.$sliderImg->id_gallery_img);

        $this->assertResponseStatus(404);
    }
}
