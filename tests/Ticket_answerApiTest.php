<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Ticket_answerApiTest extends TestCase
{
    use MakeTicket_answerTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateTicket_answer()
    {
        $ticketAnswer = $this->fakeTicket_answerData();
        $this->json('POST', '/api/v1/ticketAnswers', $ticketAnswer);

        $this->assertApiResponse($ticketAnswer);
    }

    /**
     * @test
     */
    public function testReadTicket_answer()
    {
        $ticketAnswer = $this->makeTicket_answer();
        $this->json('GET', '/api/v1/ticketAnswers/'.$ticketAnswer->id_ticket_answer);

        $this->assertApiResponse($ticketAnswer->toArray());
    }

    /**
     * @test
     */
    public function testUpdateTicket_answer()
    {
        $ticketAnswer = $this->makeTicket_answer();
        $editedTicket_answer = $this->fakeTicket_answerData();

        $this->json('PUT', '/api/v1/ticketAnswers/'.$ticketAnswer->id_ticket_answer, $editedTicket_answer);

        $this->assertApiResponse($editedTicket_answer);
    }

    /**
     * @test
     */
    public function testDeleteTicket_answer()
    {
        $ticketAnswer = $this->makeTicket_answer();
        $this->json('DELETE', '/api/v1/ticketAnswers/'.$ticketAnswer->id_ticket_answer);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ticketAnswers/'.$ticketAnswer->id_ticket_answer);

        $this->assertResponseStatus(404);
    }
}
