<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ChangeCoverApiTest extends TestCase
{
    use MakeChangeCoverTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateChangeCover()
    {
        $changeCover = $this->fakeChangeCoverData();
        $this->json('POST', '/api/v1/changeCovers', $changeCover);

        $this->assertApiResponse($changeCover);
    }

    /**
     * @test
     */
    public function testReadChangeCover()
    {
        $changeCover = $this->makeChangeCover();
        $this->json('GET', '/api/v1/changeCovers/'.$changeCover->id);

        $this->assertApiResponse($changeCover->toArray());
    }

    /**
     * @test
     */
    public function testUpdateChangeCover()
    {
        $changeCover = $this->makeChangeCover();
        $editedChangeCover = $this->fakeChangeCoverData();

        $this->json('PUT', '/api/v1/changeCovers/'.$changeCover->id, $editedChangeCover);

        $this->assertApiResponse($editedChangeCover);
    }

    /**
     * @test
     */
    public function testDeleteChangeCover()
    {
        $changeCover = $this->makeChangeCover();
        $this->json('DELETE', '/api/v1/changeCovers/'.$changeCover->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/changeCovers/'.$changeCover->id);

        $this->assertResponseStatus(404);
    }
}
