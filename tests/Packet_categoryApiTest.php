<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Packet_categoryApiTest extends TestCase
{
    use MakePacket_categoryTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePacket_category()
    {
        $packetCategory = $this->fakePacket_categoryData();
        $this->json('POST', '/api/v1/packetCategories', $packetCategory);

        $this->assertApiResponse($packetCategory);
    }

    /**
     * @test
     */
    public function testReadPacket_category()
    {
        $packetCategory = $this->makePacket_category();
        $this->json('GET', '/api/v1/packetCategories/'.$packetCategory->id_packet_category);

        $this->assertApiResponse($packetCategory->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePacket_category()
    {
        $packetCategory = $this->makePacket_category();
        $editedPacket_category = $this->fakePacket_categoryData();

        $this->json('PUT', '/api/v1/packetCategories/'.$packetCategory->id_packet_category, $editedPacket_category);

        $this->assertApiResponse($editedPacket_category);
    }

    /**
     * @test
     */
    public function testDeletePacket_category()
    {
        $packetCategory = $this->makePacket_category();
        $this->json('DELETE', '/api/v1/packetCategories/'.$packetCategory->id_packet_category);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/packetCategories/'.$packetCategory->id_packet_category);

        $this->assertResponseStatus(404);
    }
}
