<?php

use App\Models\Publisher_team_member;
use App\Repositories\Publisher\admin\Publisher_team_memberRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Publisher_team_memberRepositoryTest extends TestCase
{
    use MakePublisher_team_memberTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Publisher_team_memberRepository
     */
    protected $publisherTeamMemberRepo;

    public function setUp()
    {
        parent::setUp();
        $this->publisherTeamMemberRepo = App::make(Publisher_team_memberRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePublisher_team_member()
    {
        $publisherTeamMember = $this->fakePublisher_team_memberData();
        $createdPublisher_team_member = $this->publisherTeamMemberRepo->create($publisherTeamMember);
        $createdPublisher_team_member = $createdPublisher_team_member->toArray();
        $this->assertArrayHasKey('id', $createdPublisher_team_member);
        $this->assertNotNull($createdPublisher_team_member['id'], 'Created Publisher_team_member must have id specified');
        $this->assertNotNull(Publisher_team_member::find($createdPublisher_team_member['id']), 'Publisher_team_member with given id must be in DB');
        $this->assertModelData($publisherTeamMember, $createdPublisher_team_member);
    }

    /**
     * @test read
     */
    public function testReadPublisher_team_member()
    {
        $publisherTeamMember = $this->makePublisher_team_member();
        $dbPublisher_team_member = $this->publisherTeamMemberRepo->find($publisherTeamMember->id_team_member);
        $dbPublisher_team_member = $dbPublisher_team_member->toArray();
        $this->assertModelData($publisherTeamMember->toArray(), $dbPublisher_team_member);
    }

    /**
     * @test update
     */
    public function testUpdatePublisher_team_member()
    {
        $publisherTeamMember = $this->makePublisher_team_member();
        $fakePublisher_team_member = $this->fakePublisher_team_memberData();
        $updatedPublisher_team_member = $this->publisherTeamMemberRepo->update($fakePublisher_team_member, $publisherTeamMember->id_team_member);
        $this->assertModelData($fakePublisher_team_member, $updatedPublisher_team_member->toArray());
        $dbPublisher_team_member = $this->publisherTeamMemberRepo->find($publisherTeamMember->id_team_member);
        $this->assertModelData($fakePublisher_team_member, $dbPublisher_team_member->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePublisher_team_member()
    {
        $publisherTeamMember = $this->makePublisher_team_member();
        $resp = $this->publisherTeamMemberRepo->delete($publisherTeamMember->id_team_member);
        $this->assertTrue($resp);
        $this->assertNull(Publisher_team_member::find($publisherTeamMember->id_team_member), 'Publisher_team_member should not exist in DB');
    }
}
