<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Member_coverApiTest extends TestCase
{
    use MakeMember_coverTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateMember_cover()
    {
        $memberCover = $this->fakeMember_coverData();
        $this->json('POST', '/api/v1/memberCovers', $memberCover);

        $this->assertApiResponse($memberCover);
    }

    /**
     * @test
     */
    public function testReadMember_cover()
    {
        $memberCover = $this->makeMember_cover();
        $this->json('GET', '/api/v1/memberCovers/'.$memberCover->id_member);

        $this->assertApiResponse($memberCover->toArray());
    }

    /**
     * @test
     */
    public function testUpdateMember_cover()
    {
        $memberCover = $this->makeMember_cover();
        $editedMember_cover = $this->fakeMember_coverData();

        $this->json('PUT', '/api/v1/memberCovers/'.$memberCover->id_member, $editedMember_cover);

        $this->assertApiResponse($editedMember_cover);
    }

    /**
     * @test
     */
    public function testDeleteMember_cover()
    {
        $memberCover = $this->makeMember_cover();
        $this->json('DELETE', '/api/v1/memberCovers/'.$memberCover->id_member);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/memberCovers/'.$memberCover->id_member);

        $this->assertResponseStatus(404);
    }
}
