<?php

use App\Models\Purchase_confirm;
use App\Repositories\Client_Page\web\Purchase_confirmRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Purchase_confirmRepositoryTest extends TestCase
{
    use MakePurchase_confirmTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Purchase_confirmRepository
     */
    protected $purchaseConfirmRepo;

    public function setUp()
    {
        parent::setUp();
        $this->purchaseConfirmRepo = App::make(Purchase_confirmRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePurchase_confirm()
    {
        $purchaseConfirm = $this->fakePurchase_confirmData();
        $createdPurchase_confirm = $this->purchaseConfirmRepo->create($purchaseConfirm);
        $createdPurchase_confirm = $createdPurchase_confirm->toArray();
        $this->assertArrayHasKey('id', $createdPurchase_confirm);
        $this->assertNotNull($createdPurchase_confirm['id'], 'Created Purchase_confirm must have id specified');
        $this->assertNotNull(Purchase_confirm::find($createdPurchase_confirm['id']), 'Purchase_confirm with given id must be in DB');
        $this->assertModelData($purchaseConfirm, $createdPurchase_confirm);
    }

    /**
     * @test read
     */
    public function testReadPurchase_confirm()
    {
        $purchaseConfirm = $this->makePurchase_confirm();
        $dbPurchase_confirm = $this->purchaseConfirmRepo->find($purchaseConfirm->id_purchase_confirm);
        $dbPurchase_confirm = $dbPurchase_confirm->toArray();
        $this->assertModelData($purchaseConfirm->toArray(), $dbPurchase_confirm);
    }

    /**
     * @test update
     */
    public function testUpdatePurchase_confirm()
    {
        $purchaseConfirm = $this->makePurchase_confirm();
        $fakePurchase_confirm = $this->fakePurchase_confirmData();
        $updatedPurchase_confirm = $this->purchaseConfirmRepo->update($fakePurchase_confirm, $purchaseConfirm->id_purchase_confirm);
        $this->assertModelData($fakePurchase_confirm, $updatedPurchase_confirm->toArray());
        $dbPurchase_confirm = $this->purchaseConfirmRepo->find($purchaseConfirm->id_purchase_confirm);
        $this->assertModelData($fakePurchase_confirm, $dbPurchase_confirm->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePurchase_confirm()
    {
        $purchaseConfirm = $this->makePurchase_confirm();
        $resp = $this->purchaseConfirmRepo->delete($purchaseConfirm->id_purchase_confirm);
        $this->assertTrue($resp);
        $this->assertNull(Purchase_confirm::find($purchaseConfirm->id_purchase_confirm), 'Purchase_confirm should not exist in DB');
    }
}
