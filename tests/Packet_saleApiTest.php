<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Packet_saleApiTest extends TestCase
{
    use MakePacket_saleTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePacket_sale()
    {
        $packetSale = $this->fakePacket_saleData();
        $this->json('POST', '/api/v1/packetSales', $packetSale);

        $this->assertApiResponse($packetSale);
    }

    /**
     * @test
     */
    public function testReadPacket_sale()
    {
        $packetSale = $this->makePacket_sale();
        $this->json('GET', '/api/v1/packetSales/'.$packetSale->id_packet_sale);

        $this->assertApiResponse($packetSale->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePacket_sale()
    {
        $packetSale = $this->makePacket_sale();
        $editedPacket_sale = $this->fakePacket_saleData();

        $this->json('PUT', '/api/v1/packetSales/'.$packetSale->id_packet_sale, $editedPacket_sale);

        $this->assertApiResponse($editedPacket_sale);
    }

    /**
     * @test
     */
    public function testDeletePacket_sale()
    {
        $packetSale = $this->makePacket_sale();
        $this->json('DELETE', '/api/v1/packetSales/'.$packetSale->id_packet_sale);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/packetSales/'.$packetSale->id_packet_sale);

        $this->assertResponseStatus(404);
    }
}
