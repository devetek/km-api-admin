<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Publisher_team_memberApiTest extends TestCase
{
    use MakePublisher_team_memberTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePublisher_team_member()
    {
        $publisherTeamMember = $this->fakePublisher_team_memberData();
        $this->json('POST', '/api/v1/publisherTeamMembers', $publisherTeamMember);

        $this->assertApiResponse($publisherTeamMember);
    }

    /**
     * @test
     */
    public function testReadPublisher_team_member()
    {
        $publisherTeamMember = $this->makePublisher_team_member();
        $this->json('GET', '/api/v1/publisherTeamMembers/'.$publisherTeamMember->id_team_member);

        $this->assertApiResponse($publisherTeamMember->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePublisher_team_member()
    {
        $publisherTeamMember = $this->makePublisher_team_member();
        $editedPublisher_team_member = $this->fakePublisher_team_memberData();

        $this->json('PUT', '/api/v1/publisherTeamMembers/'.$publisherTeamMember->id_team_member, $editedPublisher_team_member);

        $this->assertApiResponse($editedPublisher_team_member);
    }

    /**
     * @test
     */
    public function testDeletePublisher_team_member()
    {
        $publisherTeamMember = $this->makePublisher_team_member();
        $this->json('DELETE', '/api/v1/publisherTeamMembers/'.$publisherTeamMember->id_team_member);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/publisherTeamMembers/'.$publisherTeamMember->id_team_member);

        $this->assertResponseStatus(404);
    }
}
