<?php

use App\Models\Purchase_packet;
use App\Repositories\Member\admin\Purchase_packetRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Purchase_packetRepositoryTest extends TestCase
{
    use MakePurchase_packetTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Purchase_packetRepository
     */
    protected $purchasePacketRepo;

    public function setUp()
    {
        parent::setUp();
        $this->purchasePacketRepo = App::make(Purchase_packetRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePurchase_packet()
    {
        $purchasePacket = $this->fakePurchase_packetData();
        $createdPurchase_packet = $this->purchasePacketRepo->create($purchasePacket);
        $createdPurchase_packet = $createdPurchase_packet->toArray();
        $this->assertArrayHasKey('id', $createdPurchase_packet);
        $this->assertNotNull($createdPurchase_packet['id'], 'Created Purchase_packet must have id specified');
        $this->assertNotNull(Purchase_packet::find($createdPurchase_packet['id']), 'Purchase_packet with given id must be in DB');
        $this->assertModelData($purchasePacket, $createdPurchase_packet);
    }

    /**
     * @test read
     */
    public function testReadPurchase_packet()
    {
        $purchasePacket = $this->makePurchase_packet();
        $dbPurchase_packet = $this->purchasePacketRepo->find($purchasePacket->id_purchase);
        $dbPurchase_packet = $dbPurchase_packet->toArray();
        $this->assertModelData($purchasePacket->toArray(), $dbPurchase_packet);
    }

    /**
     * @test update
     */
    public function testUpdatePurchase_packet()
    {
        $purchasePacket = $this->makePurchase_packet();
        $fakePurchase_packet = $this->fakePurchase_packetData();
        $updatedPurchase_packet = $this->purchasePacketRepo->update($fakePurchase_packet, $purchasePacket->id_purchase);
        $this->assertModelData($fakePurchase_packet, $updatedPurchase_packet->toArray());
        $dbPurchase_packet = $this->purchasePacketRepo->find($purchasePacket->id_purchase);
        $this->assertModelData($fakePurchase_packet, $dbPurchase_packet->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePurchase_packet()
    {
        $purchasePacket = $this->makePurchase_packet();
        $resp = $this->purchasePacketRepo->delete($purchasePacket->id_purchase);
        $this->assertTrue($resp);
        $this->assertNull(Purchase_packet::find($purchasePacket->id_purchase), 'Purchase_packet should not exist in DB');
    }
}
