<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PublisherCoverApiTest extends TestCase
{
    use MakePublisherCoverTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePublisherCover()
    {
        $publisherCover = $this->fakePublisherCoverData();
        $this->json('POST', '/api/v1/publisherCovers', $publisherCover);

        $this->assertApiResponse($publisherCover);
    }

    /**
     * @test
     */
    public function testReadPublisherCover()
    {
        $publisherCover = $this->makePublisherCover();
        $this->json('GET', '/api/v1/publisherCovers/'.$publisherCover->id_publisher);

        $this->assertApiResponse($publisherCover->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePublisherCover()
    {
        $publisherCover = $this->makePublisherCover();
        $editedPublisherCover = $this->fakePublisherCoverData();

        $this->json('PUT', '/api/v1/publisherCovers/'.$publisherCover->id_publisher, $editedPublisherCover);

        $this->assertApiResponse($editedPublisherCover);
    }

    /**
     * @test
     */
    public function testDeletePublisherCover()
    {
        $publisherCover = $this->makePublisherCover();
        $this->json('DELETE', '/api/v1/publisherCovers/'.$publisherCover->id_publisher);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/publisherCovers/'.$publisherCover->id_publisher);

        $this->assertResponseStatus(404);
    }
}
