<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PacketIncomeApiTest extends TestCase
{
    use MakePacketIncomeTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePacketIncome()
    {
        $packetIncome = $this->fakePacketIncomeData();
        $this->json('POST', '/api/v1/packetIncomes', $packetIncome);

        $this->assertApiResponse($packetIncome);
    }

    /**
     * @test
     */
    public function testReadPacketIncome()
    {
        $packetIncome = $this->makePacketIncome();
        $this->json('GET', '/api/v1/packetIncomes/'.$packetIncome->id_packet);

        $this->assertApiResponse($packetIncome->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePacketIncome()
    {
        $packetIncome = $this->makePacketIncome();
        $editedPacketIncome = $this->fakePacketIncomeData();

        $this->json('PUT', '/api/v1/packetIncomes/'.$packetIncome->id_packet, $editedPacketIncome);

        $this->assertApiResponse($editedPacketIncome);
    }

    /**
     * @test
     */
    public function testDeletePacketIncome()
    {
        $packetIncome = $this->makePacketIncome();
        $this->json('DELETE', '/api/v1/packetIncomes/'.$packetIncome->id_packet);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/packetIncomes/'.$packetIncome->id_packet);

        $this->assertResponseStatus(404);
    }
}
