<?php

use App\Models\PublisherCover;
use App\Repositories\Publisher\admin\PublisherCoverRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PublisherCoverRepositoryTest extends TestCase
{
    use MakePublisherCoverTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PublisherCoverRepository
     */
    protected $publisherCoverRepo;

    public function setUp()
    {
        parent::setUp();
        $this->publisherCoverRepo = App::make(PublisherCoverRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePublisherCover()
    {
        $publisherCover = $this->fakePublisherCoverData();
        $createdPublisherCover = $this->publisherCoverRepo->create($publisherCover);
        $createdPublisherCover = $createdPublisherCover->toArray();
        $this->assertArrayHasKey('id', $createdPublisherCover);
        $this->assertNotNull($createdPublisherCover['id'], 'Created PublisherCover must have id specified');
        $this->assertNotNull(PublisherCover::find($createdPublisherCover['id']), 'PublisherCover with given id must be in DB');
        $this->assertModelData($publisherCover, $createdPublisherCover);
    }

    /**
     * @test read
     */
    public function testReadPublisherCover()
    {
        $publisherCover = $this->makePublisherCover();
        $dbPublisherCover = $this->publisherCoverRepo->find($publisherCover->id_publisher);
        $dbPublisherCover = $dbPublisherCover->toArray();
        $this->assertModelData($publisherCover->toArray(), $dbPublisherCover);
    }

    /**
     * @test update
     */
    public function testUpdatePublisherCover()
    {
        $publisherCover = $this->makePublisherCover();
        $fakePublisherCover = $this->fakePublisherCoverData();
        $updatedPublisherCover = $this->publisherCoverRepo->update($fakePublisherCover, $publisherCover->id_publisher);
        $this->assertModelData($fakePublisherCover, $updatedPublisherCover->toArray());
        $dbPublisherCover = $this->publisherCoverRepo->find($publisherCover->id_publisher);
        $this->assertModelData($fakePublisherCover, $dbPublisherCover->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePublisherCover()
    {
        $publisherCover = $this->makePublisherCover();
        $resp = $this->publisherCoverRepo->delete($publisherCover->id_publisher);
        $this->assertTrue($resp);
        $this->assertNull(PublisherCover::find($publisherCover->id_publisher), 'PublisherCover should not exist in DB');
    }
}
