<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Packet_coverApiTest extends TestCase
{
    use MakePacket_coverTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePacket_cover()
    {
        $packetCover = $this->fakePacket_coverData();
        $this->json('POST', '/api/v1/packetCovers', $packetCover);

        $this->assertApiResponse($packetCover);
    }

    /**
     * @test
     */
    public function testReadPacket_cover()
    {
        $packetCover = $this->makePacket_cover();
        $this->json('GET', '/api/v1/packetCovers/'.$packetCover->id_packet);

        $this->assertApiResponse($packetCover->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePacket_cover()
    {
        $packetCover = $this->makePacket_cover();
        $editedPacket_cover = $this->fakePacket_coverData();

        $this->json('PUT', '/api/v1/packetCovers/'.$packetCover->id_packet, $editedPacket_cover);

        $this->assertApiResponse($editedPacket_cover);
    }

    /**
     * @test
     */
    public function testDeletePacket_cover()
    {
        $packetCover = $this->makePacket_cover();
        $this->json('DELETE', '/api/v1/packetCovers/'.$packetCover->id_packet);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/packetCovers/'.$packetCover->id_packet);

        $this->assertResponseStatus(404);
    }
}
