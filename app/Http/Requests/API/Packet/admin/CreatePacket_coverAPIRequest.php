<?php

namespace App\Http\Requests\API\Packet\admin;

use App\Models\Packet_cover;
use InfyOm\Generator\Request\APIRequest;

class CreatePacket_coverAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Packet_cover::$rules;
    }
}
