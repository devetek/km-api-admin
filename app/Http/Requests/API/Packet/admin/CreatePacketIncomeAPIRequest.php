<?php

namespace App\Http\Requests\API\Packet\admin;

use App\Models\PacketIncome;
use InfyOm\Generator\Request\APIRequest;

class CreatePacketIncomeAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return PacketIncome::$rules;
    }
}
