<?php

namespace App\Http\Requests\API\Packet\admin;

use App\Models\Packet_book;
use InfyOm\Generator\Request\APIRequest;

class UpdatePacket_bookAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Packet_book::$rules;
    }
}
