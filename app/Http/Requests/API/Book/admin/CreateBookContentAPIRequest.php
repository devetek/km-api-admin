<?php

namespace App\Http\Requests\API\Book\admin;

use App\Models\BookContent;
use InfyOm\Generator\Request\APIRequest;

class CreateBookContentAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return BookContent::$rules;
    }
}
