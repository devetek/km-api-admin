<?php

namespace App\Http\Requests\API\Book\member;

use App\Models\BookSection;
use InfyOm\Generator\Request\APIRequest;

class UpdateBookSectionAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return BookSection::$rules;
    }
}
