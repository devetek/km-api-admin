<?php

namespace App\Http\Requests\API\Member\admin;

use App\Models\Member_cover;
use InfyOm\Generator\Request\APIRequest;

class UpdateMember_coverAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Member_cover::$rules;
    }
}
