<?php

namespace App\Http\Requests\API\Slider\member;

use App\Models\Slider_img;
use InfyOm\Generator\Request\APIRequest;

class UpdateSlider_imgAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Slider_img::$rules;
    }
}
