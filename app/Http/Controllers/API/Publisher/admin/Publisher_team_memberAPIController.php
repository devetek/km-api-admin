<?php

namespace App\Http\Controllers\API\Publisher\admin;

use App\Models\Publisher_team_member;
use App\Repositories\Publisher\admin\Publisher_team_memberRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class Publisher_team_memberController
 * @package publisher/admin/ App\Http\Controllers\API\Publisher\admin
 */

class Publisher_team_memberAPIController extends AppBaseController
{
    /** @var  Publisher_team_memberRepository */
    private $publisherTeamMemberRepository;

    public function __construct(Publisher_team_memberRepository $publisherTeamMemberRepo)
    {
        $this->publisherTeamMemberRepository = $publisherTeamMemberRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/publisher/admin/publisherTeamMembers",
     *      summary="Get a listing of the Publisher_team_members.",
     *      tags={"Publisher/admin/Publisher_team_member"},
     *      description="Get all Publisher_team_members",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Publisher_team_member")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $publisherTeamMembers = $this->publisherTeamMemberRepository->all($request);
        return $this->sendResponse($publisherTeamMembers, 'Publisher Team Members retrieved successfully');
    }

    public function create(Request $reques)
    {

    }

    /**
     * @param CreatePublisher_team_memberAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/publisher/admin/publisherTeamMembers",
     *      summary="Store a newly created Publisher_team_member in storage",
     *      tags={"Publisher/admin/Publisher_team_member"},
     *      description="Store Publisher_team_member",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Publisher_team_member that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Publisher_team_member")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Publisher_team_member"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(Request $request)
    {
            $publisherTeamMembers = $this->publisherTeamMemberRepository->create($request);

            return $this->sendResponse($publisherTeamMembers, 'Publisher Team Member saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/publisher/admin/publisherTeamMembers/{id}",
     *      summary="Display the specified Publisher_team_member",
     *      tags={"Publisher/admin/Publisher_team_member"},
     *      description="Get Publisher_team_member",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Publisher_team_member",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Publisher_team_member"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Publisher_team_member $publisherTeamMember */
        $publisherTeamMember = $this->publisherTeamMemberRepository->findWithoutFail($id);

        if (empty($publisherTeamMember)) {
            return $this->sendError('Publisher Team Member not found');
        }

        return $this->sendResponse($publisherTeamMember->toArray(), 'Publisher Team Member retrieved successfully');
    }

    public function edit($id)
    {
        /** @var Publisher_team_member $publisherTeamMember */
        $publisherTeamMember = $this->publisherTeamMemberRepository->edit($id);

        if (empty($publisherTeamMember)) {
            return $this->sendError('Publisher Team Member not found');
        }

        return $this->sendResponse($publisherTeamMember->toArray(), 'Publisher Team Member retrieved successfully');
    }


    /**
     * @param int $id
     * @param UpdatePublisher_team_memberAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/publisher/admin/publisherTeamMembers/{id}",
     *      summary="Update the specified Publisher_team_member in storage",
     *      tags={"Publisher/admin/Publisher_team_member"},
     *      description="Update Publisher_team_member",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Publisher_team_member",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Publisher_team_member that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Publisher_team_member")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Publisher_team_member"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        /** @var Publisher_team_member $publisherTeamMember */
        $publisherTeamMember = $this->publisherTeamMemberRepository->findWithoutFail($id);

        if (empty($publisherTeamMember)) {
            return $this->sendError('Publisher Team Member not found');
        }

        $publisherTeamMember = $this->publisherTeamMemberRepository->update($input, $id);

        return $this->sendResponse($publisherTeamMember->toArray(), 'Publisher_team_member updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/publisher/admin/publisherTeamMembers/{id}",
     *      summary="Remove the specified Publisher_team_member from storage",
     *      tags={"Publisher/admin/Publisher_team_member"},
     *      description="Delete Publisher_team_member",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Publisher_team_member",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Publisher_team_member $publisherTeamMember */
        $publisherTeamMember = $this->publisherTeamMemberRepository->findWithoutFail($id);

        if (empty($publisherTeamMember)) {
            return $this->sendError('Publisher Team Member not found');
        }

        $publisherTeamMember->delete();

        return $this->sendResponse($id, 'Publisher Team Member deleted successfully');
    }
}
