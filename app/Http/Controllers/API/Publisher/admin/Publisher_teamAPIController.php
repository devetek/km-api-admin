<?php

namespace App\Http\Controllers\API\Publisher\admin;

use App\Models\Publisher_team;
use App\Repositories\Publisher\admin\Publisher_teamRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class Publisher_teamController
 * @package publisher/admin/ App\Http\Controllers\API\Publisher\admin
 */

class Publisher_teamAPIController extends AppBaseController
{
    /** @var  Publisher_teamRepository */
    private $publisherTeamRepository;

    public function __construct(Publisher_teamRepository $publisherTeamRepo)
    {
        $this->publisherTeamRepository = $publisherTeamRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/publisher/admin/publisherTeams",
     *      summary="Get a listing of the Publisher_teams.",
     *      tags={"Publisher/admin/Publisher_team"},
     *      description="Get all Publisher_teams",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Publisher_team")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $publisherTeams = $this->publisherTeamRepository->all();
        return $this->sendResponse($publisherTeams, 'Publisher Teams retrieved successfully');
    }

    public function create(Request $reques)
    {

    }

    /**
     * @param CreatePublisher_teamAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/publisher/admin/publisherTeams",
     *      summary="Store a newly created Publisher_team in storage",
     *      tags={"Publisher/admin/Publisher_team"},
     *      description="Store Publisher_team",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Publisher_team that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Publisher_team")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Publisher_team"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(Request $request)
    {
            $input = $request->all();

            $publisherTeams = $this->publisherTeamRepository->create($input);

            return $this->sendResponse($publisherTeams, 'Publisher Team saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/publisher/admin/publisherTeams/{id}",
     *      summary="Display the specified Publisher_team",
     *      tags={"Publisher/admin/Publisher_team"},
     *      description="Get Publisher_team",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Publisher_team",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Publisher_team"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Publisher_team $publisherTeam */
        $publisherTeam = $this->publisherTeamRepository->findWithoutFail($id);

        if (empty($publisherTeam)) {
            return $this->sendError('Publisher Team not found');
        }

        return $this->sendResponse($publisherTeam->toArray(), 'Publisher Team retrieved successfully');
    }

    public function edit($id)
    {
        /** @var Publisher_team $publisherTeam */
        $publisherTeam = $this->publisherTeamRepository->edit($id);

        if (empty($publisherTeam)) {
            return $this->sendError('Publisher Team not found');
        }

        return $this->sendResponse($publisherTeam->toArray(), 'Publisher Team retrieved successfully');
    }


    /**
     * @param int $id
     * @param UpdatePublisher_teamAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/publisher/admin/publisherTeams/{id}",
     *      summary="Update the specified Publisher_team in storage",
     *      tags={"Publisher/admin/Publisher_team"},
     *      description="Update Publisher_team",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Publisher_team",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Publisher_team that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Publisher_team")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Publisher_team"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        /** @var Publisher_team $publisherTeam */
        $publisherTeam = $this->publisherTeamRepository->findWithoutFail($id);

        if (empty($publisherTeam)) {
            return $this->sendError('Publisher Team not found');
        }

        $publisherTeam = $this->publisherTeamRepository->update($input, $id);

        return $this->sendResponse($publisherTeam->toArray(), 'Publisher_team updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/publisher/admin/publisherTeams/{id}",
     *      summary="Remove the specified Publisher_team from storage",
     *      tags={"Publisher/admin/Publisher_team"},
     *      description="Delete Publisher_team",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Publisher_team",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Publisher_team $publisherTeam */
        $publisherTeam = $this->publisherTeamRepository->findWithoutFail($id);

        if (empty($publisherTeam)) {
            return $this->sendError('Publisher Team not found');
        }

        $publisherTeam->delete();

        return $this->sendResponse($id, 'Publisher Team deleted successfully');
    }
}
