<?php

namespace App\Http\Controllers\Api\User;

use App\Models\TPemohon;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

//use Tymon\JWTAuth\Exceptions\JWTException;
//use JWTAuth;
//use Auth;
use DB;

class AuthCont extends Controller
{
    public function damUser(Request $request)
    {
        DB::select("select * from M_PART");

        $user['name'] = "guest";
        $user['id_izin_posisi'] = 0;
        $data['user'] = $user;
        return $data;
    }

    public function cek(Request $request)
    {
        return response()->json($request->user());
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    public function authenticate(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);
        $credentials = request(['email', 'password']);
        if (!Auth::attempt($credentials))
            return response()->json([
                'message' => 'Unauthorized2 '
            ], 401);
        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();
        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
    }


    public function refresh(Request $request)
    {
        $token = JWTAuth::getToken();
        $newToken = JWTAuth::refresh($token);

        // all good so return the token
        return $newToken;
    }

    public function getAuthenticatedUser(Request $request)
    {
        return response()->json($request->user());
    }

}
