<?php

namespace App\Http\Controllers\API\Packet\client;

use App\Models\Packet_requirement;
use App\Repositories\Packet\client\Packet_requirementRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class Packet_requirementController
 * @package packet/client/ App\Http\Controllers\API\Packet\client
 */

class Packet_requirementAPIController extends AppBaseController
{
    /** @var  Packet_requirementRepository */
    private $packetRequirementRepository;

    public function __construct(Packet_requirementRepository $packetRequirementRepo)
    {
        $this->packetRequirementRepository = $packetRequirementRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/packet/client/packetRequirements",
     *      summary="Get a listing of the Packet_requirements.",
     *      tags={"Packet/client/Packet_requirement"},
     *      description="Get all Packet_requirements",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Packet_requirement")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $packetRequirements = $this->packetRequirementRepository->all($request);
        return $this->sendResponse($packetRequirements, 'Packet Requirements retrieved successfully');
    }

}
