<?php

namespace App\Http\Controllers\API\Packet\client;

use App\Models\Packet;
use App\Repositories\Packet\client\PacketRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class PacketController
 * @package packet/client/ App\Http\Controllers\API\Packet\client
 */

class PacketAPIController extends AppBaseController
{
    /** @var  PacketRepository */
    private $packetRepository;

    public function __construct(PacketRepository $packetRepo)
    {
        $this->packetRepository = $packetRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/packet/client/packets",
     *      summary="Get a listing of the Packets.",
     *      tags={"Packet/client/Packet"},
     *      description="Get all Packets",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Packet")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $packets = $this->packetRepository->all($request);
        return $this->sendResponse($packets, 'Packets retrieved successfully');
    }


    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/packet/client/packets/{id}",
     *      summary="Display the specified Packet",
     *      tags={"Packet/client/Packet"},
     *      description="Get Packet",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Packet",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Packet"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Packet $packet */
        $packet = $this->packetRepository->detail($id);

        if (empty($packet)) {
            return $this->sendError('Packet not found');
        }

        return $this->sendResponse($packet, 'Packet retrieved successfully');
    }


}
