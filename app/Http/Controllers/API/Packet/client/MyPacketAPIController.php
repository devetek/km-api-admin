<?php

namespace App\Http\Controllers\API\Packet\client;

use App\Models\MyPacket;
use App\Repositories\Packet\client\MyPacketRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class MyPacketController
 * @package packet/client/ App\Http\Controllers\API\Packet\client
 */

class MyPacketAPIController extends AppBaseController
{
    /** @var  MyPacketRepository */
    private $myPacketRepository;

    public function __construct(MyPacketRepository $myPacketRepo)
    {
        $this->myPacketRepository = $myPacketRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/packet/client/myPackets",
     *      summary="Get a listing of the MyPackets.",
     *      tags={"Packet/client/MyPacket"},
     *      description="Get all MyPackets",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/MyPacket")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $myPackets = $this->myPacketRepository->all($request);
        return $this->sendResponse($myPackets, 'My Packets retrieved successfully');
    }


    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/packet/client/myPackets/{id}",
     *      summary="Display the specified MyPacket",
     *      tags={"Packet/client/MyPacket"},
     *      description="Get MyPacket",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MyPacket",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MyPacket"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var MyPacket $myPacket */
        $myPacket = $this->myPacketRepository->detail($id);

        if (empty($myPacket)) {
            return $this->sendError('My Packet not found');
        }

        return $this->sendResponse($myPacket, 'My Packet retrieved successfully');
    }
}
