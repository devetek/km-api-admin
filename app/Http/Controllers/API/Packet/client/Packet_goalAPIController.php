<?php

namespace App\Http\Controllers\API\Packet\client;

use App\Models\Packet_goal;
use App\Repositories\Packet\client\Packet_goalRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class Packet_goalController
 * @package packet/client/ App\Http\Controllers\API\Packet\client
 */

class Packet_goalAPIController extends AppBaseController
{
    /** @var  Packet_goalRepository */
    private $packetGoalRepository;

    public function __construct(Packet_goalRepository $packetGoalRepo)
    {
        $this->packetGoalRepository = $packetGoalRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/packet/client/packetGoals",
     *      summary="Get a listing of the Packet_goals.",
     *      tags={"Packet/client/Packet_goal"},
     *      description="Get all Packet_goals",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Packet_goal")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $packetGoals = $this->packetGoalRepository->all($request);
        return $this->sendResponse($packetGoals, 'Packet Goals retrieved successfully');
    }


}
