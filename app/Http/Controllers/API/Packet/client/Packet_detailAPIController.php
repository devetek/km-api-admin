<?php

namespace App\Http\Controllers\API\Packet\client;

use App\Models\Packet_detail;
use App\Repositories\Packet\client\Packet_detailRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class Packet_detailController
 * @package packet/client/ App\Http\Controllers\API\Packet\client
 */

class Packet_detailAPIController extends AppBaseController
{
    /** @var  Packet_detailRepository */
    private $packetDetailRepository;

    public function __construct(Packet_detailRepository $packetDetailRepo)
    {
        $this->packetDetailRepository = $packetDetailRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/packet/client/packetDetails",
     *      summary="Get a listing of the Packet_details.",
     *      tags={"Packet/client/Packet_detail"},
     *      description="Get all Packet_details",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Packet_detail")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $packetDetails = $this->packetDetailRepository->all($request);
        return $this->sendResponse($packetDetails, 'Packet Details retrieved successfully');
    }

}
