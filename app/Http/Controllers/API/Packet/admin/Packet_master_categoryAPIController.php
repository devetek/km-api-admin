<?php

namespace App\Http\Controllers\API\Packet\admin;

use App\Models\Packet_master_category;
use App\Repositories\Packet\admin\Packet_master_categoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class Packet_master_categoryController
 * @package packet/admin/ App\Http\Controllers\API\Packet\admin
 */

class Packet_master_categoryAPIController extends AppBaseController
{
    /** @var  Packet_master_categoryRepository */
    private $packetMasterCategoryRepository;

    public function __construct(Packet_master_categoryRepository $packetMasterCategoryRepo)
    {
        $this->packetMasterCategoryRepository = $packetMasterCategoryRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/packet/admin/packetMasterCategories",
     *      summary="Get a listing of the Packet_master_categories.",
     *      tags={"Packet/admin/Packet_master_category"},
     *      description="Get all Packet_master_categories",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Packet_master_category")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $packetMasterCategories = $this->packetMasterCategoryRepository->all();
        return $this->sendResponse($packetMasterCategories, 'Packet Master Categories retrieved successfully');
    }

    public function create(Request $reques)
    {

    }

    /**
     * @param CreatePacket_master_categoryAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/packet/admin/packetMasterCategories",
     *      summary="Store a newly created Packet_master_category in storage",
     *      tags={"Packet/admin/Packet_master_category"},
     *      description="Store Packet_master_category",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Packet_master_category that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Packet_master_category")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Packet_master_category"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(Request $request)
    {
            $input = $request->all();

            $packetMasterCategories = $this->packetMasterCategoryRepository->create($input);

            return $this->sendResponse($packetMasterCategories, 'Packet Master Category saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/packet/admin/packetMasterCategories/{id}",
     *      summary="Display the specified Packet_master_category",
     *      tags={"Packet/admin/Packet_master_category"},
     *      description="Get Packet_master_category",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Packet_master_category",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Packet_master_category"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Packet_master_category $packetMasterCategory */
        $packetMasterCategory = $this->packetMasterCategoryRepository->findWithoutFail($id);

        if (empty($packetMasterCategory)) {
            return $this->sendError('Packet Master Category not found');
        }

        return $this->sendResponse($packetMasterCategory->toArray(), 'Packet Master Category retrieved successfully');
    }


    /**
     * @param int $id
     * @param UpdatePacket_master_categoryAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/packet/admin/packetMasterCategories/{id}",
     *      summary="Update the specified Packet_master_category in storage",
     *      tags={"Packet/admin/Packet_master_category"},
     *      description="Update Packet_master_category",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Packet_master_category",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Packet_master_category that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Packet_master_category")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Packet_master_category"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        /** @var Packet_master_category $packetMasterCategory */
        $packetMasterCategory = $this->packetMasterCategoryRepository->findWithoutFail($id);

        if (empty($packetMasterCategory)) {
            return $this->sendError('Packet Master Category not found');
        }

        $packetMasterCategory = $this->packetMasterCategoryRepository->update($input, $id);

        return $this->sendResponse($packetMasterCategory->toArray(), 'Packet_master_category updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/packet/admin/packetMasterCategories/{id}",
     *      summary="Remove the specified Packet_master_category from storage",
     *      tags={"Packet/admin/Packet_master_category"},
     *      description="Delete Packet_master_category",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Packet_master_category",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Packet_master_category $packetMasterCategory */
        $packetMasterCategory = $this->packetMasterCategoryRepository->findWithoutFail($id);

        if (empty($packetMasterCategory)) {
            return $this->sendError('Packet Master Category not found');
        }

        $packetMasterCategory->delete();

        return $this->sendResponse($id, 'Packet Master Category deleted successfully');
    }
}
