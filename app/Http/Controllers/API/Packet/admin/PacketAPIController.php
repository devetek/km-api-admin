<?php

namespace App\Http\Controllers\API\Packet\admin;

use App\Models\Packet;
use App\Repositories\Packet\admin\PacketRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class PacketController
 * @package packet/admin/ App\Http\Controllers\API\Packet\admin
 */

class PacketAPIController extends AppBaseController
{
    /** @var  PacketRepository */
    private $packetRepository;

    public function __construct(PacketRepository $packetRepo)
    {
        $this->packetRepository = $packetRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/packet/admin/packets",
     *      summary="Get a listing of the Packets.",
     *      tags={"Packet/admin/Packet"},
     *      description="Get all Packets",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Packet")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $packets = $this->packetRepository->all($request);
        return $this->sendResponse($packets, 'Packets retrieved successfully');
    }

    public function create(Request $reques)
    {

    }

    /**
     * @param CreatePacketAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/packet/admin/packets",
     *      summary="Store a newly created Packet in storage",
     *      tags={"Packet/admin/Packet"},
     *      description="Store Packet",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Packet that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Packet")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Packet"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(Request $request)
    {
            $input = $request->all();

            $packets = $this->packetRepository->create($input);

            return $this->sendResponse($packets, 'Packet saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/packet/admin/packets/{id}",
     *      summary="Display the specified Packet",
     *      tags={"Packet/admin/Packet"},
     *      description="Get Packet",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Packet",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Packet"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Packet $packet */
        $packet = $this->packetRepository->detail($id);

        if (empty($packet)) {
            return $this->sendError('Packet not found');
        }

        return $this->sendResponse($packet, 'Packet retrieved successfully');
    }

    public function edit($id)
    {
        /** @var Packet $packet */
        $packet = $this->packetRepository->edit($id);

        if (empty($packet)) {
            return $this->sendError('Packet not found');
        }

        return $this->sendResponse($packet->toArray(), 'Packet retrieved successfully');
    }


    /**
     * @param int $id
     * @param UpdatePacketAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/packet/admin/packets/{id}",
     *      summary="Update the specified Packet in storage",
     *      tags={"Packet/admin/Packet"},
     *      description="Update Packet",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Packet",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Packet that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Packet")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Packet"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        /** @var Packet $packet */
        $packet = $this->packetRepository->findWithoutFail($id);

        if (empty($packet)) {
            return $this->sendError('Packet not found');
        }

        $packet = $this->packetRepository->update($input, $id);

        return $this->sendResponse($packet->toArray(), 'Packet updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/packet/admin/packets/{id}",
     *      summary="Remove the specified Packet from storage",
     *      tags={"Packet/admin/Packet"},
     *      description="Delete Packet",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Packet",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Packet $packet */
        $packet = $this->packetRepository->findWithoutFail($id);

        if (empty($packet)) {
            return $this->sendError('Packet not found');
        }

        $packet->delete();

        return $this->sendResponse($id, 'Packet deleted successfully');
    }
}
