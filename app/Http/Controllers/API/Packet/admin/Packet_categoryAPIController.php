<?php

namespace App\Http\Controllers\API\Packet\admin;

use App\Models\Packet_category;
use App\Repositories\Packet\admin\Packet_categoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class Packet_categoryController
 * @package packet/admin/ App\Http\Controllers\API\Packet\admin
 */

class Packet_categoryAPIController extends AppBaseController
{
    /** @var  Packet_categoryRepository */
    private $packetCategoryRepository;

    public function __construct(Packet_categoryRepository $packetCategoryRepo)
    {
        $this->packetCategoryRepository = $packetCategoryRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/packet/admin/packetCategories",
     *      summary="Get a listing of the Packet_categories.",
     *      tags={"Packet/admin/Packet_category"},
     *      description="Get all Packet_categories",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Packet_category")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $packetCategories = $this->packetCategoryRepository->all($request);
        return $this->sendResponse($packetCategories, 'Packet Categories retrieved successfully');
    }

    public function create(Request $reques)
    {

    }

    /**
     * @param CreatePacket_categoryAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/packet/admin/packetCategories",
     *      summary="Store a newly created Packet_category in storage",
     *      tags={"Packet/admin/Packet_category"},
     *      description="Store Packet_category",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Packet_category that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Packet_category")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Packet_category"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(Request $request)
    {
            $input = $request->all();

            $packetCategories = $this->packetCategoryRepository->create($request);

            return $this->sendResponse($packetCategories, 'Packet Category saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/packet/admin/packetCategories/{id}",
     *      summary="Display the specified Packet_category",
     *      tags={"Packet/admin/Packet_category"},
     *      description="Get Packet_category",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Packet_category",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Packet_category"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Packet_category $packetCategory */
        $packetCategory = $this->packetCategoryRepository->findWithoutFail($id);
        $out = [];
        foreach ($packetCategory as $x)
        {
            $out[] = $x->id_packet_master_category;
        }

        return $this->sendResponse($out, 'Packet Category retrieved successfully');
    }

    public function edit($id)
    {
        /** @var Packet_category $packetCategory */
        $packetCategory = $this->packetCategoryRepository->edit($id);

        if (empty($packetCategory)) {
            return $this->sendError('Packet Category not found');
        }

        return $this->sendResponse($packetCategory->toArray(), 'Packet Category retrieved successfully');
    }


    /**
     * @param int $id
     * @param UpdatePacket_categoryAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/packet/admin/packetCategories/{id}",
     *      summary="Update the specified Packet_category in storage",
     *      tags={"Packet/admin/Packet_category"},
     *      description="Update Packet_category",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Packet_category",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Packet_category that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Packet_category")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Packet_category"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        /** @var Packet_category $packetCategory */
        $packetCategory = $this->packetCategoryRepository->findWithoutFail($id);

        if (empty($packetCategory)) {
            return $this->sendError('Packet Category not found');
        }

        $packetCategory = $this->packetCategoryRepository->update($input, $id);

        return $this->sendResponse($packetCategory->toArray(), 'Packet_category updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/packet/admin/packetCategories/{id}",
     *      summary="Remove the specified Packet_category from storage",
     *      tags={"Packet/admin/Packet_category"},
     *      description="Delete Packet_category",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Packet_category",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Packet_category $packetCategory */
        $packetCategory = $this->packetCategoryRepository->findWithoutFail($id);

        if (empty($packetCategory)) {
            return $this->sendError('Packet Category not found');
        }

        $packetCategory->delete();

        return $this->sendResponse($id, 'Packet Category deleted successfully');
    }
}
