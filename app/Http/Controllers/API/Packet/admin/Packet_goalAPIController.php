<?php

namespace App\Http\Controllers\API\Packet\admin;

use App\Models\Packet_goal;
use App\Repositories\Packet\admin\Packet_goalRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class Packet_goalController
 * @package packet/admin/ App\Http\Controllers\API\Packet\admin
 */

class Packet_goalAPIController extends AppBaseController
{
    /** @var  Packet_goalRepository */
    private $packetGoalRepository;

    public function __construct(Packet_goalRepository $packetGoalRepo)
    {
        $this->packetGoalRepository = $packetGoalRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/packet/admin/packetGoals",
     *      summary="Get a listing of the Packet_goals.",
     *      tags={"Packet/admin/Packet_goal"},
     *      description="Get all Packet_goals",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Packet_goal")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $packetGoals = $this->packetGoalRepository->all($request);
        return $this->sendResponse($packetGoals, 'Packet Goals retrieved successfully');
    }

    public function create(Request $reques)
    {

    }

    /**
     * @param CreatePacket_goalAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/packet/admin/packetGoals",
     *      summary="Store a newly created Packet_goal in storage",
     *      tags={"Packet/admin/Packet_goal"},
     *      description="Store Packet_goal",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Packet_goal that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Packet_goal")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Packet_goal"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(Request $request)
    {
            $input = $request->all();

            $packetGoals = $this->packetGoalRepository->create($input);

            return $this->sendResponse($packetGoals, 'Packet Goal saved successfully');
    }
    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/packet/admin/packetGoals/{id}",
     *      summary="Remove the specified Packet_goal from storage",
     *      tags={"Packet/admin/Packet_goal"},
     *      description="Delete Packet_goal",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Packet_goal",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Packet_goal $packetGoal */
        $packetGoal = $this->packetGoalRepository->findWithoutFail($id);

        if (empty($packetGoal)) {
            return $this->sendError('Packet Goal not found');
        }

        $packetGoal->delete();

        return $this->sendResponse($id, 'Packet Goal deleted successfully');
    }
}
