<?php

namespace App\Http\Controllers\API\Packet\admin;

use App\Models\PacketIncome;
use App\Repositories\Packet\admin\PacketIncomeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class PacketIncomeController
 * @package packet/admin/ App\Http\Controllers\API\Packet\admin
 */

class PacketIncomeAPIController extends AppBaseController
{
    /** @var  PacketIncomeRepository */
    private $packetIncomeRepository;

    public function __construct(PacketIncomeRepository $packetIncomeRepo)
    {
        $this->packetIncomeRepository = $packetIncomeRepo;
    }


    public function index(Request $request)
    {
        $packetIncomes = $this->packetIncomeRepository->all($request);
        return $this->sendResponse($packetIncomes, 'Packet Incomes retrieved successfully');
    }


    public function show($id)
    {
        /** @var PacketIncome $packetIncome */
        $packetIncome = $this->packetIncomeRepository->findWithoutFail($id);

        if (empty($packetIncome)) {
            return $this->sendError('Packet Income not found');
        }

        return $this->sendResponse($packetIncome->toArray(), 'Packet Income retrieved successfully');
    }


}
