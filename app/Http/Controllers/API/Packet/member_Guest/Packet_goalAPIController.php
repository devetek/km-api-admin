<?php

namespace App\Http\Controllers\API\Packet\member_Guest;

use App\Models\Packet_goal;
use App\Repositories\Packet\member_Guest\Packet_goalRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class Packet_goalController
 * @package packet/memberGuest/ App\Http\Controllers\API\Packet\member_Guest
 */

class Packet_goalAPIController extends AppBaseController
{
    /** @var  Packet_goalRepository */
    private $packetGoalRepository;

    public function __construct(Packet_goalRepository $packetGoalRepo)
    {
        $this->packetGoalRepository = $packetGoalRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/packet/memberGuest/packetGoals",
     *      summary="Get a listing of the Packet_goals.",
     *      tags={"Packet/member_Guest/Packet_goal"},
     *      description="Get all Packet_goals",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Packet_goal")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $packetGoals = $this->packetGoalRepository->all($request);
        return $this->sendResponse($packetGoals, 'Packet Goals retrieved successfully');
    }

}
