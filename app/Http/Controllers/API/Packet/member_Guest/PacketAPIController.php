<?php

namespace App\Http\Controllers\API\Packet\member_Guest;

use App\Models\Packet;
use App\Repositories\Packet\member_Guest\PacketRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class PacketController
 * @package packet/memberGuest/ App\Http\Controllers\API\Packet\member_Guest
 */

class PacketAPIController extends AppBaseController
{
    /** @var  PacketRepository */
    private $packetRepository;

    public function __construct(PacketRepository $packetRepo)
    {
        $this->packetRepository = $packetRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/packet/memberGuest/packets",
     *      summary="Get a listing of the Packets.",
     *      tags={"Packet/member_Guest/Packet"},
     *      description="Get all Packets",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/V_packet_sale")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $packets = $this->packetRepository->all();
        return $this->sendResponse($packets, 'Packets retrieved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/packet/memberGuest/packets/{id}",
     *      summary="Display the specified Packet",
     *      tags={"Packet/member_Guest/Packet"},
     *      description="Get Packet",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Packet",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Packet"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Packet $packet */
        $packet = $this->packetRepository->findById($id);


        if (empty($packet)) {
            return $this->sendError('Packet not found');
        }

        return $this->sendResponse($packet->toArray(), 'Packet retrieved successfully');
    }

}
