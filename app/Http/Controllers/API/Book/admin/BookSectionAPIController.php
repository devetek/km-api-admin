<?php

namespace App\Http\Controllers\API\Book\admin;

use App\Models\BookSection;
use App\Repositories\Book\admin\BookSectionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class BookSectionController
 * @package book/admin/ App\Http\Controllers\API\Book\admin
 */

class BookSectionAPIController extends AppBaseController
{
    /** @var  BookSectionRepository */
    private $bookSectionRepository;

    public function __construct(BookSectionRepository $bookSectionRepo)
    {
        $this->bookSectionRepository = $bookSectionRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/book/admin/bookSections",
     *      summary="Get a listing of the BookSections.",
     *      tags={"Book/admin/BookSection"},
     *      description="Get all BookSections",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/BookSection")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $bookSections = $this->bookSectionRepository->all($request);
        return $this->sendResponse($bookSections, 'Book Sections retrieved successfully');
    }

    public function create(Request $reques)
    {

    }

    /**
     * @param CreateBookSectionAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/book/admin/bookSections",
     *      summary="Store a newly created BookSection in storage",
     *      tags={"Book/admin/BookSection"},
     *      description="Store BookSection",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="BookSection that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/BookSection")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/BookSection"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(Request $request)
    {
            $input = $request->all();

            $bookSections = $this->bookSectionRepository->create($input,$request);

            return $this->sendResponse($bookSections, 'Book Section saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/book/admin/bookSections/{id}",
     *      summary="Display the specified BookSection",
     *      tags={"Book/admin/BookSection"},
     *      description="Get BookSection",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of BookSection",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/BookSection"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var BookSection $bookSection */
        $bookSection = $this->bookSectionRepository->findWithoutFail($id);

        if (empty($bookSection)) {
            return $this->sendError('Book Section not found');
        }

        return $this->sendResponse($bookSection->toArray(), 'Book Section retrieved successfully');
    }


    /**
     * @param int $id
     * @param UpdateBookSectionAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/book/admin/bookSections/{id}",
     *      summary="Update the specified BookSection in storage",
     *      tags={"Book/admin/BookSection"},
     *      description="Update BookSection",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of BookSection",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="BookSection that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/BookSection")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/BookSection"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        /** @var BookSection $bookSection */
        $bookSection = $this->bookSectionRepository->findWithoutFail($id);

        if (empty($bookSection)) {
            return $this->sendError('Book Section not found');
        }

        $bookSection = $this->bookSectionRepository->update($input, $id);

        return $this->sendResponse($bookSection->toArray(), 'BookSection updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/book/admin/bookSections/{id}",
     *      summary="Remove the specified BookSection from storage",
     *      tags={"Book/admin/BookSection"},
     *      description="Delete BookSection",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of BookSection",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var BookSection $bookSection */
        $bookSection = $this->bookSectionRepository->findWithoutFail($id);

        if (empty($bookSection)) {
            return $this->sendError('Book Section not found');
        }

        $bookSection->delete();

        return $this->sendResponse($id, 'Book Section deleted successfully');
    }
}
