<?php

namespace App\Http\Controllers\API\Member\admin;

use App\Models\Purchase_packet;
use App\Repositories\Member\admin\Purchase_packetRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class Purchase_packetController
 * @package member/admin/ App\Http\Controllers\API\Member\admin
 */

class Purchase_packetAPIController extends AppBaseController
{
    /** @var  Purchase_packetRepository */
    private $purchasePacketRepository;

    public function __construct(Purchase_packetRepository $purchasePacketRepo)
    {
        $this->purchasePacketRepository = $purchasePacketRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/member/admin/purchasePackets",
     *      summary="Get a listing of the Purchase_packets.",
     *      tags={"Member/admin/Purchase_packet"},
     *      description="Get all Purchase_packets",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Purchase_packet")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $purchasePackets = $this->purchasePacketRepository->all($request);
        return $this->sendResponse($purchasePackets, 'Purchase Packets retrieved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/member/admin/purchasePackets/{id}",
     *      summary="Display the specified Purchase_packet",
     *      tags={"Member/admin/Purchase_packet"},
     *      description="Get Purchase_packet",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Purchase_packet",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Purchase_packet"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Purchase_packet $purchasePacket */
        $purchasePacket = $this->purchasePacketRepository->findWithoutFail($id);

        if (empty($purchasePacket)) {
            return $this->sendError('Purchase Packet not found');
        }

        return $this->sendResponse($purchasePacket->toArray(), 'Purchase Packet retrieved successfully');
    }

}
