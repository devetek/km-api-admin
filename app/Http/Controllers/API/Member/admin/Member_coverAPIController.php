<?php

namespace App\Http\Controllers\API\Member\admin;

use App\Models\Member_cover;
use App\Repositories\Member\admin\Member_coverRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class Member_coverController
 * @package member/admin/ App\Http\Controllers\API\Member\admin
 */

class Member_coverAPIController extends AppBaseController
{
    /** @var  Member_coverRepository */
    private $memberCoverRepository;

    public function __construct(Member_coverRepository $memberCoverRepo)
    {
        $this->memberCoverRepository = $memberCoverRepo;
    }


    public function store(Request $request)
    {

            $memberCovers = $this->memberCoverRepository->create($request);

            return $this->sendResponse($memberCovers, 'Member Cover saved successfully');
    }

}
