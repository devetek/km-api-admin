<?php

namespace App\Http\Controllers\API\Member\admin;

use App\Models\Member_report;
use App\Repositories\Member\admin\Member_reportRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class Member_reportController
 * @package member/admin/ App\Http\Controllers\API\Member\admin
 */

class Member_reportAPIController extends AppBaseController
{
    /** @var  Member_reportRepository */
    private $memberReportRepository;

    public function __construct(Member_reportRepository $memberReportRepo)
    {
        $this->memberReportRepository = $memberReportRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/member/admin/memberReports",
     *      summary="Get a listing of the Member_reports.",
     *      tags={"Member/admin/Member_report"},
     *      description="Get all Member_reports",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Member_report")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $memberReports = $this->memberReportRepository->all($request);
        return $this->sendResponse($memberReports, 'Member Reports retrieved successfully');
    }

}
