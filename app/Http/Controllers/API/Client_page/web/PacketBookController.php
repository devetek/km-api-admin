<?php

namespace App\Http\Controllers\API\Client_page\web;

use App\Repositories\Client_page\web\PacketBookRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;



class PacketBookController extends AppBaseController
{
    /** @var  BookRepository */
    private $packetBookRepository;

    public function __construct(PacketBookRepository $packetBookRepo)
    {
        $this->packetBookRepository = $packetBookRepo;
    }

    public function index(Request $request)
    {
        $books = $this->packetBookRepository->all($request);
        return $this->sendResponse($books, 'Home retrieved successfully');
    }


    public function show(Request $request,$id_book)
    {
        $books = $this->packetBookRepository->detail($id_book);
        return $this->sendResponse($books, 'Packet retrieved successfully');
    }

}
