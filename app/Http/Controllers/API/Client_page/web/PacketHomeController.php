<?php

namespace App\Http\Controllers\API\Client_page\web;

use App\Repositories\Client_page\web\PacketRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;



class PacketHomeController extends AppBaseController
{
    /** @var  BookRepository */
    private $packetRepository;

    public function __construct(PacketRepository $packetRepo)
    {
        $this->packetRepository = $packetRepo;
    }

    public function index(Request $request)
    {
        $books = $this->packetRepository->allForHome($request);
        return $this->sendResponse($books, 'Home retrieved successfully');
    }
     


}
