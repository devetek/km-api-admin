<?php

namespace App\Http\Controllers\API\Client_page\mobile_guest;

use App\Repositories\Client_page\mobile_guest\PacketRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;



class PacketController extends AppBaseController
{
    /** @var  BookRepository */
    private $packetRepository;

    public function __construct(PacketRepository $packetRepo)
    {
        $this->packetRepository = $packetRepo;
    }

    public function show(Request $request,$id_packet)
    {
        $books = $this->packetRepository->detail($id_packet);
        return $this->sendResponse($books, 'Packet retrieved successfully');
    }

}
