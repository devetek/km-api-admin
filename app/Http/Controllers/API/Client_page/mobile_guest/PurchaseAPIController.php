<?php

namespace App\Http\Controllers\API\Client_Page\mobile_guest;

use App\Models\Purchase;
use App\Repositories\Client_Page\mobile_guest\PurchaseRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class PurchaseController
 * @package clientPage/mobileQuest/ App\Http\Controllers\API\Client_Page\mobile_Quest
 */

class PurchaseAPIController extends AppBaseController
{
    /** @var  PurchaseRepository */
    private $purchaseRepository;

    public function __construct(PurchaseRepository $purchaseRepo)
    {
        $this->purchaseRepository = $purchaseRepo;
    }

    /**
     * @param CreatePurchaseAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/clientPage/mobileQuest/purchases",
     *      summary="Store a newly created Purchase in storage",
     *      tags={"Client_Page/mobile_Quest/Purchase"},
     *      description="Store Purchase",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Purchase that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Purchase")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Purchase"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(Request $request)
    {
            $input = $request->all();

            $purchases = $this->purchaseRepository->create($input);

            return $this->sendResponse($purchases, 'Purchase saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/clientPage/mobileQuest/purchases/{id}",
     *      summary="Display the specified Purchase",
     *      tags={"Client_Page/mobile_Quest/Purchase"},
     *      description="Get Purchase",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Purchase",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Purchase"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Purchase $purchase */
        $purchase = $this->purchaseRepository->findWithoutFail($id);

        if (empty($purchase)) {
            return $this->sendError('Purchase not found');
        }

        return $this->sendResponse($purchase->toArray(), 'Purchase retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdatePurchaseAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/clientPage/mobileQuest/purchases/{id}",
     *      summary="Update the specified Purchase in storage",
     *      tags={"Client_Page/mobile_Quest/Purchase"},
     *      description="Update Purchase",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Purchase",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Purchase that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Purchase")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Purchase"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        /** @var Purchase $purchase */
        $purchase = $this->purchaseRepository->findWithoutFail($id);

        if (empty($purchase)) {
            return $this->sendError('Purchase not found');
        }

        $purchase = $this->purchaseRepository->update($request, $id);

        return $this->sendResponse($purchase->toArray(), 'Purchase updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/clientPage/mobileQuest/purchases/{id}",
     *      summary="Remove the specified Purchase from storage",
     *      tags={"Client_Page/mobile_Quest/Purchase"},
     *      description="Delete Purchase",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Purchase",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Purchase $purchase */
        $purchase = $this->purchaseRepository->findWithoutFail($id);

        if (empty($purchase)) {
            return $this->sendError('Purchase not found');
        }

        $purchase->delete();

        return $this->sendResponse($id, 'Purchase deleted successfully');
    }
}
