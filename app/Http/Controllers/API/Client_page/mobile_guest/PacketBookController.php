<?php

namespace App\Http\Controllers\API\Client_page\mobile_guest;

use App\Repositories\Client_page\mobile_guest\PacketBookRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;



class PacketBookController extends AppBaseController
{
    /** @var  BookRepository */
    private $packetBookRepository;

    public function __construct(PacketBookRepository $packetBookRepo)
    {
        $this->packetBookRepository = $packetBookRepo;
    }

    public function show(Request $request,$id_packet)
    {
        $books = $this->packetBookRepository->detail($id_packet);
        return $this->sendResponse($books, 'Packet retrieved successfully');
    }

}
