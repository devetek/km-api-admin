<?php

namespace App\Http\Controllers\API\Client_page\mobile;

use App\Repositories\Client_page\mobile\HomeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;



class HomeController extends AppBaseController
{
    /** @var  BookRepository */
    private $homeRepository;

    public function __construct(HomeRepository $homeRepo)
    {
        $this->homeRepository = $homeRepo;
    }

    public function index(Request $request)
    {
        $books = $this->homeRepository->all($request);
        return $this->sendResponse($books, 'Home retrieved successfully');
    }

}
