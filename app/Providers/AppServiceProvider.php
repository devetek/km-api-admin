<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Libraries\CustomUrlGenerator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        /**
         * Register custom url generator, make flexible base url base on APP_URL environment variable.
         * Author: Prakasa <prakasa@devetek.com>
         * 
         * @return void
         */
        $routes = $this->app['router']->getRoutes();
        $customUrlGenerator = new CustomUrlGenerator($routes, $this->app->make('request'));
        $this->app->instance('url', $customUrlGenerator);
    }
}
