<?php

namespace App\Repositories\Packet\client;

use App\Helper\Query;
use App\Models\Packet_requirement;
use Illuminate\Support\Facades\DB;

/**
 * Class Packet_requirementRepository
 * @package App\Repositories\Packet\client
 * @version April 7, 2019, 3:45 pm UTC
 *
 * @method Packet_requirement find($id, $columns = ['*'])
 * @method Packet_requirement first($columns = ['*'])
*/
class Packet_requirementRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_packet',
        'order_requirement',
        'icon',
        'validasi',
        'requirement'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Packet_requirement::class;
    }

    public function all($request)
    {
        $query = Query::filterRequest($this->fieldSearchable,$request);
        $sql = "Select * from packet_requirements

                where validasi > 0 ".$query."
                order by created_at DESC
                ";
        $data = DB::select($sql);
        return $data;
    }

}
