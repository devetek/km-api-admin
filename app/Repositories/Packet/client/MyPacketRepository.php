<?php

namespace App\Repositories\Packet\client;

use App\Helper\Query;
use App\Models\MyPacket;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

/**
 * Class MyPacketRepository
 * @package App\Repositories\Packet\client
 * @version April 7, 2019, 1:35 pm UTC
 *
 * @method MyPacket find($id, $columns = ['*'])
 * @method MyPacket first($columns = ['*'])
*/
class MyPacketRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_packet',
        'name_packet',
        'excerpt_packet',
        'description_packet',
        'id_publisher_team',
        'cover_packet',
        'thum_packet',
        'date_publish',
        'id_company',
        'status_packet',
        'is_public',
        'price',
        'validasi',
        'id_packet_sale',
        'promo_text',
        'price_packet_sale',
        'time_support',
        'phone_company',
        'name_company',
        'about_company',
        'id_member_subscribe',
        'amount_paid',
        'date_active'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MyPacket::class;
    }

    public function all($request)
    {
        $query = Query::filterRequest($this->fieldSearchable,$request);
        $sql = "Select * from v_my_packet

                where validasi > 0 ".$query."
                and id_member=".Query::getUser()->id_from_division."
                order by id_member_subscribe DESC
                ";

        if($request->input('limit'))
        {
            $sql="select a.* from (".$sql.") a limit 0,".$request->input('limit');
        }
        $data = DB::select($sql);

        $out = [];

        foreach ($data as $x) {

            $sql=" select * from v_my_packet_support where module_id=".$x->id_packet_sale." ";
            $support = DB::select($sql);
            $x->support = $support;

            if ($x->cover_packet == null) {
                $x->cover_packet = URL::to('/') . "/app/packet/default.png";
            } else {
                $x->cover_packet = URL::to('/') . "/app/packet/" . $x->id_packet . "/" . $x->cover_packet;

            }
            if ($x->thum_packet == null) {
                $x->thum_packet = URL::to('/') . "/app/packet/thum_default.png";
            } else {
                $x->thum_packet = URL::to('/') . "/app/packet/" . $x->id_packet . "/" . $x->thum_packet;

            }
            $out[] = $x;
        }

        return $out;
    }

    public function detail($id, $columns = ['*'])
    {
        $sql = "Select * from v_my_packet 
                where validasi > 0  
                and id_packet=".$id." 
                ";
        $data = DB::select($sql)[0];
        if ($data->cover_packet == null) {
            $data->cover_packet = URL::to('/') . "/app/packet/default.png";
        } else {
            $data->cover_packet = URL::to('/') . "/app/packet/" . $data->id_packet . "/" . $data->cover_packet;

        }
        if ($data->thum_packet == null) {
            $data->thum_packet = URL::to('/') . "/app/packet/thum_default.png";
        } else {
            $data->thum_packet = URL::to('/') . "/app/packet/" . $data->id_packet . "/" . $data->thum_packet;

        }

        $sql=" select * from v_my_packet_support where module_id=".$data->id_packet_sale." ";
        $support = DB::select($sql);
        $data->support = $support;

        return $data;
    }



}
