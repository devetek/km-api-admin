<?php

namespace App\Repositories\Packet\client;

use App\Helper\Query;
use App\Models\Packet;
use App\Models\V_packet_sale;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

/**
 * Class PacketRepository
 * @package App\Repositories\Packet\client
 * @version April 17, 2019, 11:37 am UTC
 *
 * @method Packet find($id, $columns = ['*'])
 * @method Packet first($columns = ['*'])
*/
class PacketRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name_packet',
        'excerpt_packet',
        'description_packet',
        'id_publisher_team',
        'cover_packet',
        'thum_packet',
        'date_publish',
        'id_company',
        'status_packet',
        'is_public',
        'price',
        'id_user',
        'validasi'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Packet::class;
    }

    public function all($request)
    {
        $sql = "Select * from v_packet_sale 
                where id_packet > 0
                ";

        if($request->input('category'))
        {
            if($request->input('category')!='')
            {
                $sql.=" and id_packet (
                    select 
                        id_packet 
                     from packet_category 
                     where id_packet_master_category in (".implode(',',$request->input('category')).")
                 )";

            }
        }

        if($request->input('price')) {
            if ($request->input('price') == 'free') {
                $sql.=" and price_packet_sale = 0 ";
            }
            if ($request->input('price') == 'premium') {
                $sql.=" and price_packet_sale > 0 ";
            }
        }

        $sql.=" order by created_at DESC";


        $data = DB::select($sql);
        $out = [];
        foreach ($data as $x) {
            if ($x->cover_packet == null) {
                $x->cover_packet = URL::to('/') . "/app/packet/default.png";
            } else {
                $x->cover_packet = URL::to('/') . "/app/packet/" . $x->id_packet . "/" . $x->cover_packet;

            }
            if ($x->thum_packet == null) {
                $x->thum_packet = URL::to('/') . "/app/packet/thum_default.png";
            } else {
                $x->thum_packet = URL::to('/') . "/app/packet/" . $x->id_packet . "/" . $x->thum_packet;

            }
            $out[] = $x;
        }
        return $out;
    }

    public function detail($id_packet)
    {
        $data['packet'] = $this->getPacket($id_packet);
        $data['packet_detail'] = $this->getPacketDetail($id_packet);
        $data['packet_goal'] = $this->getPacketGoal($id_packet);
        $data['packet_requirement'] = $this->getPacketRequirement($id_packet);
        return $data;
    }

    public function getPacket($id_packet)
    {
        $data = V_packet_sale::where('id_packet',$id_packet)->get();
        $data = $data[0];
        if ($data->cover_packet == null) {
            $data->cover_packet = URL::to('/') . "/app/packet/default.png";
        } else {
            $data->cover_packet = URL::to('/') . "/app/packet/" . $data->id_packet . "/" . $data->cover_packet;

        }

        if ($data->thum_packet == null) {
            $data->thum_packet = URL::to('/') . "/app/packet/thum_default.png";
        } else {
            $data->thum_packet = URL::to('/') . "/app/packet/" . $data->id_packet . "/" . $data->thum_packet;
        }

        return $data;
    }

    public function getPacketDetail($id_packet)
    {
        $sql = "Select * from packet_detail

                where id_packet = " . $id_packet . "
                order by order_detail ASC
                ";
        $data = DB::select($sql);
        return $data;
    }

    public function getPacketGoal($id_packet)
    {
        $sql = "Select * from packet_goal

                where id_packet = " . $id_packet . "
                order by order_goal ASC
                ";
        $data = DB::select($sql);
        return $data;
    }

    public function getPacketRequirement($id_packet)
    {
        $sql = "Select * from packet_requirements

                where id_packet = " . $id_packet . "
                order by order_requirement ASC
                ";
        $data = DB::select($sql);
        return $data;
    }

}
