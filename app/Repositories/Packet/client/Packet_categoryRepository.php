<?php

namespace App\Repositories\Packet\client;

use App\Helper\Query;
use App\Models\Packet_category;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

/**
 * Class Packet_categoryRepository
 * @package App\Repositories\Packet\client
 * @version April 16, 2019, 3:04 pm UTC
 *
 * @method Packet_category find($id, $columns = ['*'])
 * @method Packet_category first($columns = ['*'])
*/
class Packet_categoryRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_packet_master_category',
        'id_packet'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Packet_category::class;
    }

    public function all($request)
    {
        $sql="SELECT
                    *
                FROM packet_master_category
                WHERE id_company=".Query::getCompany()->id_company;
        $data = DB::select($sql);
        $out = [];
        foreach ($data as $x) {
            if ($x->cover_category == null) {
                $x->cover_category = URL::to('/') . "/app/category/default.png";
            } else {
                $x->cover_category = URL::to('/') . "/app/category/" . $x->id_packet_master_category . "/" . $x->cover_url;

            }
            $out[] = $x;
        }

        return $out;
    }

    public function findWithoutFail($id, $columns = ['*'])
    {
            try {
                return $this->model()::find($id);
            } catch (Exception $e) {
                return;
            }
    }

    public function edit($id)
    {
            try {
                return $this->model()::find($id);
            } catch (Exception $e) {
                return;
            }
    }

    public function create(array $attributes)
        {
            $model = new Packet_category($attributes);
            $cek = Query::beforeInsert($model);
            return $cek;
        }

        public function update(array $attributes, $id)
        {
            $data = Packet_category::find($id);
            $data->fill($attributes);
            if ($data->validasi == 0) {
                $data->validasi = 1;
            }
            $data->save();
            return $data;
        }

}
