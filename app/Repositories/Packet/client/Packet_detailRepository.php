<?php

namespace App\Repositories\Packet\client;

use App\Helper\Query;
use App\Models\Packet_detail;
use Illuminate\Support\Facades\DB;

/**
 * Class Packet_detailRepository
 * @package App\Repositories\Packet\client
 * @version April 7, 2019, 3:46 pm UTC
 *
 * @method Packet_detail find($id, $columns = ['*'])
 * @method Packet_detail first($columns = ['*'])
*/
class Packet_detailRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_packet',
        'detail_packet',
        'icon',
        'order_detail'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Packet_detail::class;
    }

    public function all($request)
    {
        $query = Query::filterRequest($this->fieldSearchable,$request);
        $sql = "Select * from packet_detail

                where id_packet > 0 ".$query."
                order by order_detail ASC
                ";
        $data = DB::select($sql);
        return $data;
    }


}
