<?php

namespace App\Repositories\Packet\client;

use App\Helper\Query;
use App\Models\Packet_book;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

/**
 * Class Packet_bookRepository
 * @package App\Repositories\Packet\client
 * @version April 7, 2019, 3:55 pm UTC
 *
 * @method Packet_book find($id, $columns = ['*'])
 * @method Packet_book first($columns = ['*'])
 */
class Packet_bookRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_book',
        'id_packet',
        'validasi',
        'id_user'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Packet_book::class;
    }

    public function all($request)
    {
        $query = Query::filterRequest($this->fieldSearchable, $request);
        $sql = "Select 
                    b.*,
                    a.id_packet_book
                from packet_book a,
                v_book b 
                where a.id_book=b.id_book 
                and a.validasi > 0 " . $query . "
                order by created_at DESC
                ";
        $data = DB::select($sql);

        $out = [];
        foreach ($data as $x) {
            if ($x->cover_url == null) {
                $x->cover_url = URL::to('/') . "/app/book/default.png";
            } else {
                $x->cover_url = URL::to('/') . "/app/book/" . $x->id_book . "/" . $x->cover_url;

            }

            if ($x->thum_cover_url == null) {
                $x->thum_cover_url = URL::to('/') . "/app/book/thum_default.png";
            } else {
                $x->thum_cover_url = URL::to('/') . "/app/book/" . $x->id_book . "/" . $x->thum_cover_url;

            }
            $out[] = $x;
        }

        return $out;

    }

    public function findWithoutFail($id, $columns = ['*'])
    {
        try {
            return $this->model()::find($id);
        } catch (Exception $e) {
            return;
        }
    }

    public function edit($id)
    {
        try {
            return $this->model()::find($id);
        } catch (Exception $e) {
            return;
        }
    }

    public function create(array $attributes)
    {
        $model = new Packet_book($attributes);
        $cek = Query::beforeInsert($model);
        return $cek;
    }

    public function update(array $attributes, $id)
    {
        $data = Packet_book::find($id);
        $data->fill($attributes);
        if ($data->validasi == 0) {
            $data->validasi = 1;
        }
        $data->save();
        return $data;
    }

}
