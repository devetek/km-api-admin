<?php

namespace App\Repositories\Packet\admin;

use App\Helper\Query;
use App\Models\MyPacket;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

/**
 * Class MyPacketRepository
 * @package App\Repositories\Packet\admin
 * @version April 7, 2019, 10:04 am UTC
 *
 * @method MyPacket find($id, $columns = ['*'])
 * @method MyPacket first($columns = ['*'])
*/
class MyPacketRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_packet',
        'name_packet',
        'excerpt_packet',
        'description_packet',
        'id_publisher_team',
        'cover_packet',
        'thum_packet',
        'date_publish',
        'id_company',
        'status_packet',
        'is_public',
        'price',
        'validasi',
        'id_packet_sale',
        'promo_text',
        'price_packet_sale',
        'time_support',
        'phone_company',
        'name_company',
        'about_company',
        'id_member_subscribe',
        'amount_paid',
        'date_active'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MyPacket::class;
    }

    public function all($request)
    {
        $query = Query::filterRequest($this->fieldSearchable,$request);
        $sql = "Select * from v_my_packet

                where validasi > 0 ".$query."
                order by created_at DESC
                ";
        $data = DB::select($sql);

        $out = [];

        foreach ($data as $x) {

            $sql=" select * from v_my_packet_support where module_id=".$x->id_packet_sale." ";
            $support = DB::select($sql);
            $x->support = $support;

            if ($x->cover_packet == null) {
                $x->cover_packet = URL::to('/') . "/app/packet/default.png";
            } else {
                $x->cover_packet = URL::to('/') . "/app/packet/" . $x->id_packet . "/" . $x->cover_packet;

            }
            if ($x->thum_packet == null) {
                $x->thum_packet = URL::to('/') . "/app/packet/thum_default.png";
            } else {
                $x->thum_packet = URL::to('/') . "/app/packet/" . $x->id_packet . "/" . $x->thum_packet;

            }
            $out[] = $x;
        }

        return $out;
    }

    public function findWithoutFail($id, $columns = ['*'])
    {
            try {
                return $this->model()::find($id);
            } catch (Exception $e) {
                return;
            }
    }

    public function edit($id)
    {
            try {
                return $this->model()::find($id);
            } catch (Exception $e) {
                return;
            }
    }

    public function create(array $attributes)
        {
            $model = new MyPacket($attributes);
            $cek = Query::beforeInsert($model);
            return $cek;
        }

        public function update(array $attributes, $id)
        {
            $data = MyPacket::find($id);
            $data->fill($attributes);
            if ($data->validasi == 0) {
                $data->validasi = 1;
            }
            $data->save();
            return $data;
        }

}
