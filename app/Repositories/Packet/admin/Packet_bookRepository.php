<?php

namespace App\Repositories\Packet\admin;

use App\Helper\Query;
use App\Models\Packet_book;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

/**
 * Class Packet_bookRepository
 * @package App\Repositories\Packet\admin
 * @version March 4, 2019, 12:00 am UTC
 *
 * @method Packet_book find($id, $columns = ['*'])
 * @method Packet_book first($columns = ['*'])
*/
class Packet_bookRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_book',
        'id_packet'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Packet_book::class;
    }

    public function all($request)
    {
        $query = Query::filterRequest($this->fieldSearchable,$request);
        $sql = "Select 
                    b.*,
                    a.id_packet_book
                from packet_book a,
                v_book b 
                where a.id_book=b.id_book 
                and a.validasi > 0 ".$query."
                order by created_at DESC
                ";
        $data = DB::select($sql);

        $out = [];
        foreach ($data as $x)
        {
            if($x->cover_url == null)
            {
                $x->cover_url = URL::to('/')."/app/book/default.png";
            }
            else
            {
                $x->cover_url = URL::to('/')."/app/book/".$x->id_book."/".$x->cover_url;

            }

            if($x->thum_cover_url == null)
            {
                $x->thum_cover_url = URL::to('/')."/app/book/thum_default.png";
            }
            else
            {
                $x->thum_cover_url = URL::to('/')."/app/book/".$x->id_book."/".$x->thum_cover_url;

            }
            $out[] = $x;
        }

        return $out;
    }

    public function pree($request)
    {
        $query = Query::filterRequest($this->fieldSearchable,$request);
        $sql = "SELECT
                    max(a.id_book)          id_book,
                    max(b.id_publisher)     id_publisher,
                    max(b.name_company)     name_company,
                    max(b.public_name)      public_name,
                    max(b.first_name)       first_name,
                    max(b.last_name)        last_name,
                    max(b.title_book)       title_book,
                    max(b.description)      description,
                    max(b.promotional_text) promotional_text,
                    max(b.cover_url)        cover_url,
                    max(b.thum_cover_url)   thum_cover_url,
                    max(b.date_publish)     date_publish,
                    max(b.actual_price)     actual_price
                FROM
                    book_shared a,
                    v_book b
                WHERE a.id_book = b.id_book
                and b.validasi > 0 ".$query."
                and a.id_company='".$request->input('id_company')."'
                group by a.id_book 
                ";
        $data = DB::select($sql);


        $out = [];
        foreach ($data as $x)
        {
            if($x->cover_url == null)
            {
                $x->cover_url = URL::to('/')."/app/book/default.png";
            }
            else
            {
                $x->cover_url = URL::to('/')."/app/book/".$x->id_book."/".$x->cover_url;

            }

            if($x->thum_cover_url == null)
            {
                $x->thum_cover_url = URL::to('/')."/app/book/thum_default.png";
            }
            else
            {
                $x->thum_cover_url = URL::to('/')."/app/book/".$x->id_book."/".$x->thum_cover_url;

            }
            $out[] = $x;
        }

        return $out;
    }

    public function findWithoutFail($id, $columns = ['*'])
    {
            try {
                return $this->model()::find($id);
            } catch (Exception $e) {
                return;
            }
    }

    public function edit($id)
    {
            try {
                return $this->model()::find($id);
            } catch (Exception $e) {
                return;
            }
    }

    public function create($request,array $attributes)
    {

        $sql=" Delete  from packet_book where id_packet='".$request->input('id_packet')."'";
        DB::delete($sql);

        for ($a=0;$a < count($request->input('id_book')); $a++)
        {
            $book = new Packet_book();
            $book->id_packet = $request->input('id_packet');
            $book->id_book = $request->input('id_book')[$a];
            $book->validasi = 1;
            $book->save();
        }

        return "berhasil";

    }



}
