<?php

namespace App\Repositories\Packet\admin;

use App\Helper\Query;
use App\Models\Packet_category;
use Illuminate\Support\Facades\DB;

/**
 * Class Packet_categoryRepository
 * @package App\Repositories\Packet\admin
 * @version March 16, 2019, 2:30 am UTC
 *
 * @method Packet_category find($id, $columns = ['*'])
 * @method Packet_category first($columns = ['*'])
*/
class Packet_categoryRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_packet_master_category',
        'id_packet'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Packet_category::class;
    }

    public function all($request)
    { 
        $sql = "Select 
                a.description_category,
                a.name_category,
                a.id_packet_master_category,
                b.id_packet_category
                from packet_master_category a 
                left join packet_category b on a.id_packet_master_category=b.id_packet_master_category 
                ";
        $data = DB::select($sql);
        return $data;
    }

    public function findWithoutFail($id, $columns = ['*'])
    {
        $sql="select id_packet_master_category  from packet_category where id_packet='".$id."'";
        $data =  DB::select($sql);
        return $data;
    }

    public function edit($id)
    {
            try {
                return $this->model()::find($id);
            } catch (Exception $e) {
                return;
            }
    }

    public function create($request)
        {
            $sql="delete from packet_category where id_packet='".$request->input('id_packet')."'";
            DB::delete($sql);
            $id_packet_master_category = $request->input('param');
            for($a=0; $a < count($id_packet_master_category); $a++)
            {
                $data = new Packet_category();
                $data->id_packet_master_category = $id_packet_master_category[$a];
                $data->id_packet = $request->input('id_packet');
                $data->save();
            }
            return $id_packet_master_category;
        }

        public function update(array $attributes, $id)
        {
            $data = Packet_category::find($id);
            $data->fill($attributes);
            if ($data->validasi == 0) {
                $data->validasi = 1;
            }
            $data->save();
            return $data;
        }

}
