<?php

namespace App\Repositories\Packet\admin;

use App\Helper\Query;
use App\Models\Packet_requirement;
use Illuminate\Support\Facades\DB;

/**
 * Class Packet_requirementRepository
 * @package App\Repositories\Packet\admin
 * @version April 1, 2019, 1:33 am UTC
 *
 * @method Packet_requirement find($id, $columns = ['*'])
 * @method Packet_requirement first($columns = ['*'])
*/
class Packet_requirementRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_packet',
        'order_requirement',
        'icon',
        'validasi',
        'requirement'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Packet_requirement::class;
    }

    public function all($request)
    {
        $query = Query::filterRequest($this->fieldSearchable,$request);
        $sql = "Select * from packet_requirements

                where validasi > 0 ".$query."
                order by created_at DESC
                ";
        $data = DB::select($sql);
        return $data;
    }

    public function findWithoutFail($id, $columns = ['*'])
    {
            try {
                return $this->model()::find($id);
            } catch (Exception $e) {
                return;
            }
    }

    public function edit($id)
    {
            try {
                return $this->model()::find($id);
            } catch (Exception $e) {
                return;
            }
    }

    public function create(array $attributes)
        {
            $model = new Packet_requirement($attributes);
            $model->save();
            return $model;
        }

        public function update(array $attributes, $id)
        {
            $data = Packet_requirement::find($id);
            $data->fill($attributes);
            if ($data->validasi == 0) {
                $data->validasi = 1;
            }
            $data->save();
            return $data;
        }

}
