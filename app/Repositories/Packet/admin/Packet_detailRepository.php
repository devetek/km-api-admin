<?php

namespace App\Repositories\Packet\admin;

use App\Helper\Query;
use App\Models\Packet_detail;
use Illuminate\Support\Facades\DB;

/**
 * Class Packet_detailRepository
 * @package App\Repositories\Packet\admin
 * @version March 3, 2019, 12:24 pm UTC
 *
 * @method Packet_detail find($id, $columns = ['*'])
 * @method Packet_detail first($columns = ['*'])
*/
class Packet_detailRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_packet',
        'detail_packet',
        'icon',
        'order_detail'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Packet_detail::class;
    }

    public function all($request)
    {
        $query = Query::filterRequest($this->fieldSearchable,$request);
        $sql = "Select * from packet_detail

                where id_packet > 0 ".$query."
                order by order_detail ASC
                ";
        $data = DB::select($sql);
        return $data;
    }

    public function findWithoutFail($id, $columns = ['*'])
    {
            try {
                return $this->model()::find($id);
            } catch (Exception $e) {
                return;
            }
    }


    public function create(array $attributes)
        {
            $model = new Packet_detail($attributes);
            $model->save();
            return $model;
        }

        public function update(array $attributes, $id)
        {
            $data = Packet_detail::find($id);
            $data->fill($attributes);
            if ($data->validasi == 0) {
                $data->validasi = 1;
            }
            $data->save();
            return $data;
        }

}
