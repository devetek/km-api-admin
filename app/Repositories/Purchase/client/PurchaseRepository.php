<?php

namespace App\Repositories\Purchase\client;

use App\Helper\Query;
use App\Models\Purchase;
use Illuminate\Support\Facades\DB;

/**
 * Class PurchaseRepository
 * @package App\Repositories\Purchase\client
 * @version May 15, 2019, 12:46 pm UTC
 *
 * @method Purchase find($id, $columns = ['*'])
 * @method Purchase first($columns = ['*'])
*/
class PurchaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'for_module',
        'unique_code',
        'module_id',
        'price_purchase',
        'date_purchase',
        'id_member',
        'id_user',
        'qty',
        'id_user_accept',
        'date_accept',
        'amount_paid',
        'validasi'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Purchase::class;
    }

    public function all($request)
    {
        $query = Query::filterRequest($this->fieldSearchable,$request);
        $sql = "Select * from purchase

                where validasi > 0 ".$query."
                order by created_at DESC
                ";
        $data = DB::select($sql);
        return $data;
    }

    public function findWithoutFail($id, $columns = ['*'])
    {
            try {
                return $this->model()::find($id);
            } catch (Exception $e) {
                return;
            }
    }

    public function edit($id)
    {
            try {
                return $this->model()::find($id);
            } catch (Exception $e) {
                return;
            }
    }

    public function create(array $attributes)
        {
            $model = new Purchase($attributes);
            $cek = Query::beforeInsert($model);
            return $cek;
        }

        public function update(array $attributes, $id)
        {
            $data = Purchase::find($id);
            $data->fill($attributes);
            if ($data->validasi == 0) {
                $data->validasi = 1;
            }
            $data->save();
            return $data;
        }

}
