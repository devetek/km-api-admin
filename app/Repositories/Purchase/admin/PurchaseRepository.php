<?php

namespace App\Repositories\Purchase\admin;

use App\Helper\Query;
use App\Models\Purchase;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

/**
 * Class PurchaseRepository
 * @package App\Repositories\Purchase\admin
 * @version April 2, 2019, 6:09 am UTC
 *
 * @method Purchase find($id, $columns = ['*'])
 * @method Purchase first($columns = ['*'])
*/
class PurchaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'for_module',
        'unique_code',
        'module_id',
        'price_purchase',
        'date_purchase',
        'id_member',
        'id_user',
        'qty',
        'id_user_accept',
        'date_accept',
        'amount_paid',
        'validasi'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Purchase::class;
    }

    public function all($request)
    {
        $query = Query::filterRequest($this->fieldSearchable,$request);
        $sql = "SELECT
                    a.*,
                    b.name_packet,
                    x.id_purchase_confirm,
                    x.date_confirm,
                    c.first_name,
                    c.last_name,
                    d.email,
                    d.name,
                    c.phone_member
                FROM purchase a
                    LEFT JOIN purchase_confirm x on a.id_purchase=x.id_purchase,
                    v_packet_sale b,
                    member c,
                    users d
                WHERE a.module_id = b.id_packet_sale
                      and a.id_member=c.id_member
                    and a.id_user=d.id
                      and a.validasi > 0
                ";

        if($request->input('status'))
        {
            if($request->input('status') =='pending')
            {
                $sql.=" 
                      AND a.amount_paid IS NULL ";
            }
            if($request->input('status') =='active')
            {
                $sql.=" 
                      AND a.amount_paid is not null ";
            }
        }

        $sql.=" Order by a.id_purchase DESC";

        $data = DB::select($sql);
        return $data;
    }

    public function detail( $id)
    {
        $sql = "SELECT
                    a.*,
                    b.name_packet,
                    x.id_purchase_confirm,
                    x.date_confirm,
                    x.transfer_form_chanel,
                    x.transfer_form_name,
                    x.value_paid,
                    x.foto_confirm,
                    c.first_name,
                    c.last_name,
                    d.email,
                    d.name,
                    c.phone_member
                FROM purchase a
                    LEFT JOIN purchase_confirm x on a.id_purchase=x.id_purchase,
                    v_packet_sale b,
                    member c,
                    users d
                WHERE a.module_id = b.id_packet_sale
                      and a.id_member=c.id_member
                    and a.id_user=d.id
                      and a.validasi > 0
                      and a.id_purchase=".$id."
                ";

        $data = DB::select($sql)[0];
        $data->foto_confirm = URL::to('/').'/confirm/'.$data->id_purchase_confirm.'/'.$data->foto_confirm;
        return $data;
    }

    public function findWithoutFail($id, $columns = ['*'])
    {
            try {
                return $this->model()::find($id);
            } catch (Exception $e) {
                return;
            }
    }

    public function edit($id)
    {
            try {
                return $this->model()::find($id);
            } catch (Exception $e) {
                return;
            }
    }


        public function update($request, $id)
        {
            $data = Purchase::find($id);
            if($request->input('amount_paid') > 0)
            {
                $data->amount_paid = $request->input('amount_paid');
                $data->id_user_accept = Query::getUser()->id;
                $data->date_accept = date('Y-m-d h:i');
            }
            $data->save();
            return $data;
        }

}
