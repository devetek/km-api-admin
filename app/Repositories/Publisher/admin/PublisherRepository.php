<?php

namespace App\Repositories\Publisher\admin;

use App\Helper\Query;
use App\Models\Publisher;
use App\Models\v_publisher;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

/**
 * Class PublisherRepository
 * @package App\Repositories\Publisher\admin
 * @version February 15, 2019, 4:36 am UTC
 *
 * @method Publisher find($id, $columns = ['*'])
 * @method Publisher first($columns = ['*'])
*/
class PublisherRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'first_name',
        'last_name',
        'public_name',
        'title_publisher',
        'foto_publisher',
        'thum_publisher',
        'phone_number',
        'mail_publisher',
        'id_company',
        'join_date',
        'status_publisher',
        'validasi'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Publisher::class;
    }

    public function all()
    {
        $sql = "Select * from v_publisher
                where validasi > 0
                order by created_at DESC
                ";
        $data = DB::select($sql);

        $out = [];
        foreach ($data as $x)
        {
            if($x->foto_publisher == null)
            {
                $x->foto_publisher = URL::to('/')."/app/publisher/default.png";
            }
            else
            {
                $x->foto_publisher = URL::to('/')."/app/publisher/".$x->id_publisher."/".$x->foto_publisher;

            }

            if($x->thum_publisher == null)
            {
                $x->thum_publisher = URL::to('/')."/app/publisher/thum_default.png";
            }
            else
            {
                $x->thum_publisher = URL::to('/')."/app/publisher/".$x->id_publisher."/".$x->thum_publisher;

            }
            $out[] = $x;
        }

        return $out;

    }

    public function findWithoutFail($id, $columns = ['*'])
    {
        try {
            return $this->model()::find($id);
        } catch (Exception $e) {
            return;
        }

    }

    public function detail($id, $columns = ['*'])
    {
        $publisher = v_publisher::find($id);

        if($publisher->foto_publisher == null)
        {
            $publisher->foto_publisher = URL::to('/')."/app/publisher/default.png";
        }
        else
        {
            $publisher->foto_publisher = URL::to('/')."/app/publisher/".$publisher->id_publisher."/".$publisher->foto_publisher;

        }

        if($publisher->thum_publisher == null)
        {
            $publisher->thum_publisher = URL::to('/')."/app/publisher/thum_default.png";
        }
        else
        {
            $publisher->thum_publisher = URL::to('/')."/app/publisher/".$publisher->id_publisher."/".$publisher->thum_publisher;

        }
        return $publisher;

    }

    public function create(array $attributes)
        {
            $model = new Publisher($attributes);
            $cek = Query::beforeInsert($model);
            return $cek;
        }

        public function update(array $attributes, $id)
        {
            $data = Publisher::find($id);
            $data->fill($attributes);
            if ($data->validasi == 0) {
                $data->validasi = 1;
            }
            $data->save();
            return $data;
        }

}
