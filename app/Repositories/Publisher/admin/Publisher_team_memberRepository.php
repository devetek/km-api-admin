<?php

namespace App\Repositories\Publisher\admin;

use App\Helper\Query;
use App\Models\Publisher_team_member;
use Illuminate\Support\Facades\DB;

/**
 * Class Publisher_team_memberRepository
 * @package App\Repositories\Publisher\admin
 * @version March 3, 2019, 1:07 am UTC
 *
 * @method Publisher_team_member find($id, $columns = ['*'])
 * @method Publisher_team_member first($columns = ['*'])
 */
class Publisher_team_memberRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_publisher_team',
        'id_publisher'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Publisher_team_member::class;
    }

    public function all($request)
    {
        $query = Query::filterRequest($this->fieldSearchable,$request);
        $sql = "Select * from publisher_team_member

                where id_publisher_team > 0 ".$query."
                order by created_at DESC
                ";
        $data = DB::select($sql);
        return $data;
    }

    public function findWithoutFail($id, $columns = ['*'])
    {
        try {
            return $this->model()::find($id);
        } catch (Exception $e) {
            return;
        }
    }

    public function edit($id)
    {
        try {
            return $this->model()::find($id);
        } catch (Exception $e) {
            return;
        }
    }

    public function create($request)
    {
        for ($a=0; $a < count($request->input('id_publisher')); $a++)
        {
            $sql = "select * from publisher_team_member where id_publisher=" . $request->input('id_publisher')[$a] . " and id_publisher_team=" . $request->input('id_publisher_team');
            $cek = DB::select($sql);
            if (count($cek) < 1) {
                $model = new Publisher_team_member();
                $model->id_publisher = $request->input('id_publisher')[$a];
                $model->id_publisher_team = $request->input('id_publisher_team');
                $model->save();
            }
        }

        return $request;
    }


}
