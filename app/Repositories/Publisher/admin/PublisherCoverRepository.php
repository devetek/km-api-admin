<?php

namespace App\Repositories\Publisher\admin;

use App\Helper\Query;
use App\Models\Publisher;
use Illuminate\Support\Facades\DB;

/**
 * Class PublisherCoverRepository
 * @package App\Repositories\Publisher\admin
 * @version February 25, 2019, 12:48 am UTC
 *
 * @method Publisher find($id, $columns = ['*'])
 * @method Publisher first($columns = ['*'])
*/
class PublisherCoverRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'first_name',
        'last_name',
        'public_name',
        'title_publisher',
        'foto_publisher',
        'thum_publisher',
        'phone_number',
        'mail_publisher',
        'id_company',
        'join_date',
        'status_publisher',
        'id_user',
        'validasi'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Publisher::class;
    }

    public function create($request, array $attributes)
    {

        $file = $request->file('file');
        if($file != null)
        {
            $request->request->add(['file'=>$file->getClientOriginalName()]);

            $pacth = public_path().'/app/publisher/'.$request->input('id')."/";
            if (!file_exists($pacth)) {
                mkdir($pacth, 0777, true);
            }
            $file->move($pacth,$file->getClientOriginalName());

            $book = Publisher::find($request->input('id'));
            if($request->input('type')=='cover')
            {
                $book->foto_publisher = $request->input('file');

            }
            else
            {
                $book->thum_publisher = $request->input('file');
            }

            $book->save();

            return $book;
        }
        return "";

    }

}
