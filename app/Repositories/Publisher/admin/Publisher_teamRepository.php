<?php

namespace App\Repositories\Publisher\admin;

use App\Helper\Query;
use App\Models\Publisher_team;
use Illuminate\Support\Facades\DB;

/**
 * Class Publisher_teamRepository
 * @package App\Repositories\Publisher\admin
 * @version March 3, 2019, 12:21 am UTC
 *
 * @method Publisher_team find($id, $columns = ['*'])
 * @method Publisher_team first($columns = ['*'])
 */
class Publisher_teamRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_publisher',
        'name_publisher_team',
        'for_company',
        'description_publisher_team',
        'id_publisher_team_leader'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Publisher_team::class;
    }

    public function all()
    {
        $sql = "Select 
                  a.*,
                  b.last_name,
                  b.first_name,
                  b.id_publisher ,
                  b.public_name               
                from publisher_team a, 
                publisher b 

                where a.id_publisher_team_leader=b.id_publisher 
                and a.validasi > 0
                order by a.created_at DESC
                ";
        $data = DB::select($sql);
        return $data;
    }

    public function findWithoutFail($id, $columns = ['*'])
    {
        try {
            return $this->model()::find($id);
        } catch (Exception $e) {
            return;
        }
    }

    public function edit($id)
    {
        try {
            return $this->model()::find($id);
        } catch (Exception $e) {
            return;
        }
    }

    public function create(array $attributes)
    {
        $model = new Publisher_team($attributes);
        $cek = Query::beforeInsert($model);
        return $cek;
    }

    public function update(array $attributes, $id)
    {
        $data = Publisher_team::find($id);
        $data->fill($attributes);
        if ($data->validasi == 0) {
            $data->validasi = 1;
        }
        $data->save();
        return $data;
    }

}
