<?php

namespace App\Repositories\Slider\member;

use App\Helper\Query;
use App\Models\Slider;
use App\Models\Slider_img;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

/**
 * Class SliderRepository
 * @package App\Repositories\Slider\member
 * @version February 19, 2019, 5:46 am UTC
 *
 * @method Slider find($id, $columns = ['*'])
 * @method Slider first($columns = ['*'])
 */
class SliderRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'for_module',
        'for_id_module',
        'name_galley',
        'code',
        'id_user'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Slider::class;
    }

    public function all($request)
    {
        $sql = "Select * from gallery

                where for_module = 'slider'
                and code = '".$request->input('code')."'
                order by created_at DESC
                ";
        $data = DB::select($sql);
        if(count($data) < 1)
        {
            return $data;
        }
        $image = Slider_img::where('id_gallery',$data[0]->id_gallery)->get();

        $out = [];
        foreach ($image as $x)
        {
            $x->img = URL::to('/').'/app/slider/'.$x->id_gallery."/".$x->img;
            $out[] = $x;
        }
        $data[0]->gallery_img = $out;


        return $data;
    }

    public function findWithoutFail($id, $columns = ['*'])
    {
        try {
            return $this->model()::find($id);
        } catch (Exception $e) {
            return;
        }
    }

    public function create(array $attributes)
    {
        $model = new Slider($attributes);
        $cek = Query::beforeInsert($model);
        return $cek;
    }

    public function update(array $attributes, $id)
    {
        $data = Slider::find($id);
        $data->fill($attributes);
        if ($data->validasi == 0) {
            $data->validasi = 1;
        }
        $data->save();
        return $data;
    }

}
