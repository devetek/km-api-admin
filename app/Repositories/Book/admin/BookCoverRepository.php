<?php

namespace App\Repositories\Book\admin;

use App\Helper\Query;
use App\Models\Book;
use Illuminate\Support\Facades\DB;

/**
 * Class BookCoverRepository
 * @package App\Repositories\Book\admin
 * @version February 24, 2019, 11:43 pm UTC
 *
 * @method Book find($id, $columns = ['*'])
 * @method Book first($columns = ['*'])
 */
class BookCoverRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title_book',
        'org_price',
        'actual_price',
        'description',
        'promotional_text',
        'id_publisher',
        'id_user',
        'cover_url',
        'thum_cover_url',
        'date_publish',
        'status',
        'validasi'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Book::class;
    }


    public function create($request, array $attributes)
    {

        $file = $request->file('file');
        if($file != null)
        {
            $request->request->add(['file'=>$file->getClientOriginalName()]);

            $pacth = public_path().'/app/book/'.$request->input('id')."/";
            if (!file_exists($pacth)) {
                mkdir($pacth, 0777, true);
            }
            $file->move($pacth,$file->getClientOriginalName());

            $book = Book::find($request->input('id'));
            if($request->input('type')=='cover')
            {
                $book->cover_url = $request->input('file');

            }
            else
            {
                $book->thum_cover_url = $request->input('file');
            }

            $book->save();

            return $book;
        }
        return "";

    }


}
