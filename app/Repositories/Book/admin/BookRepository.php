<?php

namespace App\Repositories\Book\admin;

use App\Helper\Query;
use App\Models\Book;
use App\Models\V_book;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

/**
 * Class BookRepository
 * @package App\Repositories\Book\admin
 * @version February 9, 2019, 2:30 pm UTC
 *
 * @method Book find($id, $columns = ['*'])
 * @method Book first($columns = ['*'])
 */
class BookRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title_book',
        'org_price',
        'actual_price',
        'description',
        'promotional_text',
        'id_publisher',
        'id_team',
        'id_user',
        'cover_url',
        'thum_cover_url',
        'date_publish',
        'status',
        'validasi'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Book::class;
    }

    public function all($request)
    {
        $query = Query::filterRequest($this->fieldSearchable, $request);

        $sql = "Select * from v_book

                where validasi > 0 
                " . $query . "
                order by created_at DESC
                ";
        $data = DB::select($sql);

        $out = [];
        foreach ($data as $x) {
            if ($x->cover_url == null) {
                $x->cover_url = URL::to('/') . "/app/book/default.png";
            } else {
                $x->cover_url = URL::to('/') . "/app/book/" . $x->id_book . "/" . $x->cover_url;

            }

            if ($x->thum_cover_url == null) {
                $x->thum_cover_url = URL::to('/') . "/app/book/thum_default.png";
            } else {
                $x->thum_cover_url = URL::to('/') . "/app/book/" . $x->id_book . "/" . $x->thum_cover_url;

            }
            $out[] = $x;
        }

        return $out;
    }

    public function detail($id, $columns = ['*'])
    {
        $book = V_book::find($id);
        if ($book->cover_url == null) {
            $book->cover_url = URL::to('/') . "/app/book/default.png";
        } else {
            $book->cover_url = URL::to('/') . "/app/book/" . $book->id_book . "/" . $book->cover_url;
        }

        if ($book->thum_cover_url == null) {
            $book->thum_cover_url = URL::to('/') . "/app/book/thum_default.png";
        } else {
            $book->thum_cover_url = URL::to('/') . "/app/book/" . $book->id_book . "/" . $book->thum_cover_url;

        }

        return $book;
    }

    public function findWithoutFail($id, $columns = ['*'])
    {
        try {
            return $this->model()::find($id);
        } catch (Exception $e) {
            return;
        }
    }

    public function create(array $attributes)
    {
        $model = new Book($attributes);
        $cek = Query::beforeInsert($model);
        return $cek;
    }

    public function update(array $attributes, $id)
    {
        $data = Book::find($id);
        $data->fill($attributes);
        if ($data->validasi == 0) {
            $data->validasi = 1;
        }
        $data->save();
        return $data;
    }

}
