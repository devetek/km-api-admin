<?php

namespace App\Repositories\Book\member;

use App\Helper\Query;
use App\Models\Book;
use Illuminate\Support\Facades\DB;

/**
 * Class BookRepository
 * @package App\Repositories\Book\member
 * @version February 9, 2019, 2:46 pm UTC
 *
 * @method Book find($id, $columns = ['*'])
 * @method Book first($columns = ['*'])
*/
class BookRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title_book',
        'org_price',
        'actual_price',
        'description',
        'promotional_text',
        'id_publisher',
        'id_user',
        'cover_url',
        'thum_cover_url',
        'date_publish',
        'status',
        'validasi'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Book::class;
    }

    public function all()
    {
        $sql = "Select * from book

                where validasi > 0
                order by created_at DESC
                ";
        $data = DB::select($sql);
        return $data;
    }

    public function findWithoutFail($id, $columns = ['*'])
    {
            try {
                return $this->model()::find($id);
            } catch (Exception $e) {
                return;
            }
    }

    public function create(array $attributes)
        {
            $model = new Book($attributes);
            $cek = Query::beforeInsert($model);
            return $cek;
        }

        public function update(array $attributes, $id)
        {
            $data = Book::find($id);
            $data->fill($attributes);
            if ($data->validasi == 0) {
                $data->validasi = 1;
            }
            $data->save();
            return $data;
        }

}
