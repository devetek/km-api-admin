<?php

namespace App\Repositories\Book\member;

use App\Helper\Query;
use App\Models\BookContent;
use Illuminate\Support\Facades\DB;

/**
 * Class BookContentRepository
 * @package App\Repositories\Book\member
 * @version February 9, 2019, 2:44 pm UTC
 *
 * @method BookContent find($id, $columns = ['*'])
 * @method BookContent first($columns = ['*'])
*/
class BookContentRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_book_section',
        'order_book_content',
        'time_book_content',
        'title_book_content',
        'book_content_type',
        'book_content',
        'validasi'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return BookContent::class;
    }

    public function all()
    {
        $sql = "Select * from book_content

                where validasi > 0
                order by created_at DESC
                ";
        $data = DB::select($sql);
        return $data;
    }

    public function findWithoutFail($id, $columns = ['*'])
    {
            try {
                return $this->model()::find($id);
            } catch (Exception $e) {
                return;
            }
    }

    public function create(array $attributes)
        {
            $model = new BookContent($attributes);
            $cek = Query::beforeInsert($model);
            return $cek;
        }

        public function update(array $attributes, $id)
        {
            $data = BookContent::find($id);
            $data->fill($attributes);
            if ($data->validasi == 0) {
                $data->validasi = 1;
            }
            $data->save();
            return $data;
        }

}
