<?php

namespace App\Repositories\Book\client;

use App\Helper\Query;
use App\Models\Content;
use Illuminate\Support\Facades\DB;

/**
 * Class ContentRepository
 * @package App\Repositories\Book\client
 * @version April 8, 2019, 2:58 am UTC
 *
 * @method Content find($id, $columns = ['*'])
 * @method Content first($columns = ['*'])
*/
class ContentRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_book_section',
        'order_book_content',
        'time_book_content',
        'title_book_content',
        'book_content_type',
        'book_content',
        'validasi',
        'id_user'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Content::class;
    }


    public function detail($id)
    {
            try {
                return $this->model()::find($id);
            } catch (Exception $e) {
                return;
            }
    }



}
