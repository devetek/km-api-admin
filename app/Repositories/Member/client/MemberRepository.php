<?php

namespace App\Repositories\Member\client;

use App\Helper\Query;
use App\Models\Member;
use Illuminate\Support\Facades\DB;

/**
 * Class MemberRepository
 * @package App\Repositories\Member\client
 * @version April 22, 2019, 7:28 am UTC
 *
 * @method Member find($id, $columns = ['*'])
 * @method Member first($columns = ['*'])
 */
class MemberRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_company',
        'first_name',
        'last_name',
        'tgl_lahir',
        'last_education',
        'phone_member',
        'poscode_member',
        'email_member',
        'foto_member',
        'thum_member',
        'address_member',
        'status_member',
        'register_form',
        'validasi',
        'id_user',
        'id_user_rgister'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Member::class;
    }

    public function all($request)
    {
        $query = Query::filterRequest($this->fieldSearchable, $request);
        $sql = "Select * from member

                where validasi > 0 " . $query . "
                and id_member='".Query::getUser()->id_from_division."'
                order by created_at DESC
                ";
        $data = DB::select($sql)[0];
        return $data;
    }

    public function findWithoutFail($id, $columns = ['*'])
    {
        try {
            return $this->model()::find($id);
        } catch (Exception $e) {
            return;
        }
    }


    public function update(array $attributes, $id)
    {
        $data = Member::find($id);
        $data->fill($attributes);
        if ($data->validasi == 0) {
            $data->validasi = 1;
        }
        $data->save();
        return $data;
    }

}
