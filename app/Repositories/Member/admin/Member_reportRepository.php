<?php

namespace App\Repositories\Member\admin;

use App\Helper\Query;
use App\Models\Member_report;
use Illuminate\Support\Facades\DB;

/**
 * Class Member_reportRepository
 * @package App\Repositories\Member\admin
 * @version March 10, 2019, 1:41 am UTC
 *
 * @method Member_report find($id, $columns = ['*'])
 * @method Member_report first($columns = ['*'])
*/
class Member_reportRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_company',
        'name_company',
        'total',
        'active',
        'inactive'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Member_report::class;
    }

    public function all($request)
    {
        $query = Query::filterRequest($this->fieldSearchable,$request);
        $sql = "Select * from v_member_by_company

                where id_company > 0 ".$query." 
                ";
        $data = DB::select($sql);
        return $data;
    }



}
