<?php

namespace App\Repositories\Member\admin;

use App\Helper\Query;
use App\Models\Member;
use Illuminate\Support\Facades\DB;

/**
 * Class MemberRepository
 * @package App\Repositories\Member\admin
 * @version March 10, 2019, 2:40 am UTC
 *
 * @method Member find($id, $columns = ['*'])
 * @method Member first($columns = ['*'])
*/
class Member_coverRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_company',
        'first_name',
        'last_name',
        'last_education',
        'phone_member',
        'poscode_member',
        'email_member',
        'foto_member',
        'thum_member',
        'address_member',
        'status_member',
        'register_form',
        'validasi',
        'id_user',
        'id_user_rgister'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Member::class;
    }

    public function create($request)
        {

            $file = $request->file('file');
            if($file != null)
            {
                $request->request->add(['file'=>$file->getClientOriginalName()]);

                $pacth = public_path().'/app/member/'.$request->input('id')."/";
                if (!file_exists($pacth)) {
                    mkdir($pacth, 0777, true);
                }
                $file->move($pacth,$file->getClientOriginalName());

                $member = Member::find($request->input('id'));
                if($request->input('type')=='cover')
                {
                    $member->foto_member = $request->input('file');

                }
                else
                {
                    $member->thum_member = $request->input('file');
                }

                $member->save();

                return $member;
            }
            return "";
        }



}
