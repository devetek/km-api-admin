<?php

namespace App\Repositories\Client_page\mobile;

use App\Helper\Query;
use App\Models\Company;
use App\Models\Slider_img;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

class HomeRepository
{

    public function model()
    {
        return Company::class;
    }

    public function all($request)
    {
        $data['slide'] = $this->getSlide($request);
        $data['category'] = $this->getPacket($request);

        return $data;
    }

    public function getSlide($request)
    {
        $sql = "Select * from gallery

                where for_module = 'slider'
                and code = 'home_slide_1'
                order by created_at DESC
                ";
        $data = DB::select($sql);
        if(count($data) < 1)
        {
            return $data;
        }
        $image = Slider_img::where('id_gallery',$data[0]->id_gallery)->get();

        $out = [];
        foreach ($image as $x)
        {
            $x->img = URL::to('/').'/app/slider/'.$x->id_gallery."/".$x->img;
            $out[] = $x;
        }
        $data[0]->gallery_img = $out;


        return $data;
    }

    public function getPacket()
    {
        $sql="SELECT
                    *
                FROM packet_master_category
                WHERE id_company=".Query::getCompany()->id_company;
        $category = DB::select($sql);
        $cat_out = [];
        foreach ($category as $cat)
        {
            $sql = "Select * from v_packet_sale 
                where id_packet in (select id_packet from packet_category where id_packet_master_category='".$cat->id_packet_master_category."')
                order by created_at DESC
                ";
            $data = DB::select($sql);
            $out = [];
            foreach ($data as $x) {
                if ($x->cover_packet == null) {
                    $x->cover_packet = URL::to('/') . "/app/packet/default.png";
                } else {
                    $x->cover_packet = URL::to('/') . "/app/packet/" . $x->id_packet . "/" . $x->cover_packet;

                }
                if ($x->thum_packet == null) {
                    $x->thum_packet = URL::to('/') . "/app/packet/thum_default.png";
                } else {
                    $x->thum_packet = URL::to('/') . "/app/packet/" . $x->id_packet . "/" . $x->thum_packet;

                }
                $out[] = $x;
            }
            $cat->packet = $out;
            $cat_out[] = $cat;

        }

        return $cat_out;





        return $out;
    }


}
