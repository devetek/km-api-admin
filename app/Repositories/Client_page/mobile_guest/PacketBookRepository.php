<?php

namespace App\Repositories\Client_page\mobile_guest;

use App\Helper\Query;
use App\Models\Packet_detail;
use App\Models\Slider_img;
use App\Models\V_packet_sale;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

class PacketBookRepository
{


    public function detail($id_packet)
    {
        $data['packet'] = $this->getPacket($id_packet);
        $data['packet_book'] = $this->getPacketBook($id_packet);
        return $data;
    }

    public function getPacket($id_packet)
    {
        $data = V_packet_sale::where('id_packet',$id_packet)->get();
        $data = $data[0];
        if ($data->cover_packet == null) {
            $data->cover_packet = URL::to('/') . "/app/packet/default.png";
        } else {
            $data->cover_packet = URL::to('/') . "/app/packet/" . $data->id_packet . "/" . $data->cover_packet;

        }

        if ($data->thum_packet == null) {
            $data->thum_packet = URL::to('/') . "/app/packet/thum_default.png";
        } else {
            $data->thum_packet = URL::to('/') . "/app/packet/" . $data->id_packet . "/" . $data->thum_packet;
        }

        return $data;
    }

    public function getPacketBook($id_packet)
    {
        $sql = "Select 
                    b.*,
                    a.id_packet_book
                from packet_book a,
                v_book b 
                where a.id_book=b.id_book 
                and a.validasi > 0 
                and a.id_packet='".$id_packet."'
                order by created_at DESC
                ";
        $data = DB::select($sql);

        $out = [];
        foreach ($data as $x)
        {
            if($x->cover_url == null)
            {
                $x->cover_url = URL::to('/')."/app/book/default.png";
            }
            else
            {
                $x->cover_url = URL::to('/')."/app/book/".$x->id_book."/".$x->cover_url;

            }

            if($x->thum_cover_url == null)
            {
                $x->thum_cover_url = URL::to('/')."/app/book/thum_default.png";
            }
            else
            {
                $x->thum_cover_url = URL::to('/')."/app/book/".$x->id_book."/".$x->thum_cover_url;

            }
            $out[] = $x;
        }

        return $out;
    }






}
