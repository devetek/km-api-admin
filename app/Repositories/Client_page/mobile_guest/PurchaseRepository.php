<?php

namespace App\Repositories\Client_Page\mobile_guest;

use App\Helper\Query;
use App\Models\Purchase;
use App\Models\V_packet_sale;
use Illuminate\Support\Facades\DB;

/**
 * Class PurchaseRepository
 * @package App\Repositories\Client_Page\mobile_Quest
 * @version March 16, 2019, 4:17 am UTC
 *
 * @method Purchase find($id, $columns = ['*'])
 * @method Purchase first($columns = ['*'])
 */
class PurchaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'for_module',
        'unique_code',
        'module_id',
        'price_purchase',
        'date_purchase',
        'id_member',
        'id_user',
        'qty',
        'id_user_accept',
        'date_accept',
        'amount_paid',
        'validasi'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Purchase::class;
    }

    public function all($request)
    {
        $query = Query::filterRequest($this->fieldSearchable, $request);
        $sql = "Select * from purchase

                where validasi > 0 " . $query . "
                order by created_at DESC
                ";
        $data = DB::select($sql);
        return $data;
    }

    public function findWithoutFail($id, $columns = ['*'])
    {
        try {
            return $this->model()::find($id);
        } catch (Exception $e) {
            return;
        }
    }


    public function create(array $attributes)
    {
        $model = new Purchase($attributes);
        $cek = Query::beforeInsert($model);
        return $cek;
    }

    public function update($request, $id)
    {
        $data = Purchase::find($id);
        $data->id_user = Query::getUser()->id;
        $data->id_member = Query::getUser()->id_from_division;
        $data->price_purchase = V_packet_sale::find($request->input('id_packet_sale'))->price_packet_sale;
        $data->for_module = 'packet_sale';
        $data->module_id = $request->input('id_packet_sale');
        $data->qty = 1;
        $data->validasi = 1;
        $data->save();
        return $data;
    }

}
