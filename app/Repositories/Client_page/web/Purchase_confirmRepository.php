<?php

namespace App\Repositories\Client_Page\web;

use App\Helper\Query;
use App\Models\Purchase_confirm;
use Illuminate\Support\Facades\DB;

/**
 * Class Purchase_confirmRepository
 * @package App\Repositories\Client_Page\web
 * @version April 2, 2019, 1:19 am UTC
 *
 * @method Purchase_confirm find($id, $columns = ['*'])
 * @method Purchase_confirm first($columns = ['*'])
*/
class Purchase_confirmRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_purchase',
        'date_confirm',
        'transfer_form_chanel',
        'transfer_form_name',
        'value_paid',
        'foto_confirm',
        'validasi',
        'id_user'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Purchase_confirm::class;
    }

    public function all($request)
    {
        $query = Query::filterRequest($this->fieldSearchable,$request);
        $sql = "Select * from purchase_confirm

                where validasi > 0 ".$query."
                order by created_at DESC
                ";
        $data = DB::select($sql);
        return $data;
    }

    public function findWithoutFail($id, $columns = ['*'])
    {
            try {
                return $this->model()::find($id);
            } catch (Exception $e) {
                return;
            }
    }

    public function edit($id)
    {
            try {
                return $this->model()::find($id);
            } catch (Exception $e) {
                return;
            }
    }

    public function create(array $attributes)
        {
            $model = new Purchase_confirm($attributes);
            $model->id_purchase = $attributes['id'];
            $cek = Query::beforeInsert($model);
            return $cek;
        }

        public function update($request, $id)
        {
            $data = Purchase_confirm::find($id);
            $data->foto_confirm = $request->input('foto_confirm');
            $data->keterangan = $request->input('keterangan');
            $data->transfer_form_chanel = $request->input('transfer_form_chanel');
            $data->transfer_form_name = $request->input('transfer_form_name');
            $data->validasi = 1;
            $data->date_confirm = date('Y-m-d');
            $data->save();
            return $data;
        }

}
