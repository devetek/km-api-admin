<?php

namespace App\Repositories\Ticket\admin;

use App\Helper\Query;
use App\Models\Ticket_answer;
use Illuminate\Support\Facades\DB;

/**
 * Class Ticket_answerRepository
 * @package App\Repositories\Ticket\admin
 * @version April 12, 2019, 2:09 am UTC
 *
 * @method Ticket_answer find($id, $columns = ['*'])
 * @method Ticket_answer first($columns = ['*'])
 */
class Ticket_answerRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_ticket',
        'date_answer',
        'id_user',
        'answer',
        'has_approved',
        'validasi'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Ticket_answer::class;
    }

    public function all($request)
    {
        $query = Query::filterRequest($this->fieldSearchable, $request);
        $sql = "Select a.*, b.name 
                from ticket_answer a, 
                users b 
                where a.id_user =b.id 
                and a.validasi > 0 " . $query . "
                order by a.created_at ASC
                ";
        $data = DB::select($sql);
        return $data;
    }

    public function findWithoutFail($id, $columns = ['*'])
    {
        try {
            return $this->model()::find($id);
        } catch (Exception $e) {
            return;
        }
    }

    public function edit($id)
    {
        try {
            return $this->model()::find($id);
        } catch (Exception $e) {
            return;
        }
    }

    public function create(array $attributes)
    {
        $model = new Ticket_answer($attributes);
        $model->date_answer = date('Y-m-d');
        $model->id_user = Query::getUser()->id;
        $model->save();
    }

    public function update(array $attributes, $id)
    {
        $data = Ticket_answer::find($id);
        $data->fill($attributes);
        if ($data->validasi == 0) {
            $data->validasi = 1;
        }
        $data->save();
        return $data;
    }

}
