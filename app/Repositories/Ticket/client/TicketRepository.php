<?php

namespace App\Repositories\Ticket\client;

use App\Helper\Query;
use App\Models\Ticket;
use http\Env\Request;
use Illuminate\Support\Facades\DB;

/**
 * Class TicketRepository
 * @package App\Repositories\Ticket\client
 * @version April 8, 2019, 11:29 am UTC
 *
 * @method Ticket find($id, $columns = ['*'])
 * @method Ticket first($columns = ['*'])
*/
class TicketRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_purchase',
        'for_module',
        'module_id',
        'sub_module_id',
        'id_user',
        'title_ticket',
        'ticket_content',
        'date_publish',
        'date_close',
        'ticket_status',
        'id_publisher_close',
        'id_user_close',
        'note_for_close',
        'validasi'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Ticket::class;
    }

    public function all($request)
    {
        $query = Query::filterRequest($this->fieldSearchable,$request);
        if($request->input('sub_module_id'))
        {
            $sql="SELECT a.*,
                        b.name
                    FROM ticket a,
                        users b 
                    WHERE a.id_user=b.id
            and a.for_module='".$request->input('for_module')."'
            and a.sub_module_id=".$request->input('sub_module_id')." ";

            $data = DB::select($sql);
            return $data;
        }

    }

    public function detail($request,$id)
    {
            $sql="SELECT a.*,
                        b.name
                    FROM ticket a,
                        users b 
                    WHERE a.id_user=b.id
            and a.id_ticket=".$id." ";

            $data = DB::select($sql)[0];
            return $data;


    }

    public function findWithoutFail($id, $columns = ['*'])
    {
            try {
                return $this->model()::find($id);
            } catch (Exception $e) {
                return;
            }
    }

    public function edit($id)
    {
            try {
                return $this->model()::find($id);
            } catch (Exception $e) {
                return;
            }
    }

    public function create(array $attributes)
        {
            $model = new Ticket($attributes);
            $cek = Query::beforeInsert($model);
            return $cek;
        }

        public function update(array $attributes, $id)
        {
            $data = Ticket::find($id);
            $data->fill($attributes);
            if ($data->validasi == 0) {
                $data->validasi = 1;
            }
            $data->save();
            return $data;
        }

}
