<?php

namespace App\Helper; 

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Query
{

    public static function filterRequest($model,$request)
    {
         
        $sql=" ";
        $req = $request->input();
        foreach($req as $key=>$x)
        {
            if(in_array(strtolower($key), $model) )
            {
                
                $sql.=" and ".self::parameter($key,$x)." ";
            }
            
        }

        return $sql;
 
 
    }

    static function parameter($key,$param)
    {

        if (strpos($param, '%') !== false) {
            return " lower(". $key.") ". " like '". (str_replace('%','',$param)) ."%' ";
        }

        return $key." = '".$param."'";
    }


    public static function getUser()
    {
        return Auth::user();

        $user = (Object)[];
        $user->id = 1;
        $user->usernama = 'arivin29';
        return $user;
    }
    public static function getCompany()
    {
        $company = (Object)[];
        $company->id_company = 1;
        $company->name_company = 'Hompes';
        return $company;
    }
    public static function beforeInsert($model,$request=null)
    {
        $sql = " Select * from ".$model->table." where validasi < 1 and id_user=".self::getUser()->id;

        if($request != null)
        {
            foreach($request->input() as $key=>$x)
            {

                $sql.=" and ".self::parameter($key,$x)." ";

            }
        }


        $cek = DB::select($sql);
        if(count($cek) < 1 )
        {
            $model->id_user = self::getUser()->id;
            $model->validasi = 0;
            $model->save();

            $cek=$cek = DB::select($sql)[0];
        }
        else
        {
            $cek = $cek[0];
        }

        return $cek;

    }

    public static function afterProsess($data)
    {

    }
}

