<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="Packet_sale",
 *      required={""},
 *      @SWG\Property(
 *          property="id_packet_sale",
 *          description="id_packet_sale",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="promo_text",
 *          description="promo_text",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="id_packet",
 *          description="id_packet",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="id_user",
 *          description="id_user",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="is_priority",
 *          description="is_priority",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="time_support",
 *          description="time_support",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="price_packet_sale",
 *          description="price_packet_sale",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="validasi",
 *          description="validasi",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Packet_sale extends Model
{

    public $table = 'packet_sale';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $primaryKey = 'id_packet_sale';

    public $fillable = [
        'promo_text',
        'id_packet',
        'efective_date',
        'id_user',
        'is_priority',
        'time_support',
        'price_packet_sale',
        'validasi'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_packet_sale' => 'integer',
        'promo_text' => 'string',
        'id_packet' => 'integer',
        'id_user' => 'integer',
        'is_priority' => 'integer',
        'time_support' => 'integer',
        'price_packet_sale' => 'integer',
        'validasi' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
