<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="Packet_requirements",
 *      required={""},
 *      @SWG\Property(
 *          property="id_packet_requirements",
 *          description="id_packet_requirements",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="id_paket",
 *          description="id_paket",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="order_requirement",
 *          description="order_requirement",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="requirement",
 *          description="requirement",
 *          type="string"
 *      )
 * )
 */
class Packet_requirements extends Model
{

    public $table = 'packet_requirements';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $primaryKey = 'id_packet_requirements';

    public $fillable = [
        'id_paket',
        'order_requirement',
        'requirement'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_packet_requirements' => 'integer',
        'id_paket' => 'integer',
        'order_requirement' => 'integer',
        'requirement' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
