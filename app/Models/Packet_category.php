<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="Packet_category",
 *      required={""},
 *      @SWG\Property(
 *          property="id_packet_category",
 *          description="id_packet_category",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="id_packet_master_category",
 *          description="id_packet_master_category",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="id_packet",
 *          description="id_packet",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Packet_category extends Model
{

    public $table = 'packet_category';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $primaryKey = 'id_packet_category';

    public $fillable = [
        'id_packet_master_category',
        'id_packet'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_packet_category' => 'integer',
        'id_packet_master_category' => 'integer',
        'id_packet' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
