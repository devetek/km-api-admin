<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="Book_shared",
 *      required={""},
 *      @SWG\Property(
 *          property="id_book_shared",
 *          description="id_book_shared",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="id_book",
 *          description="id_book",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="id_company",
 *          description="id_company",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Book_shared extends Model
{

    public $table = 'book_shared';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $primaryKey = 'id_book_shared';

    public $fillable = [
        'id_book',
        'id_company'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_book_shared' => 'integer',
        'id_book' => 'integer',
        'id_company' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
