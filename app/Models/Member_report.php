<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="Member_report",
 *      required={""},
 *      @SWG\Property(
 *          property="id_company",
 *          description="id_company",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name_company",
 *          description="name_company",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="active",
 *          description="active",
 *          type="number",
 *          format="float"
 *      ),
 *      @SWG\Property(
 *          property="inactive",
 *          description="inactive",
 *          type="number",
 *          format="float"
 *      )
 * )
 */
class Member_report extends Model
{

    public $table = 'v_member_by_company';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $primaryKey = 'id_company';

    public $fillable = [
        'id_company',
        'name_company',
        'total',
        'active',
        'inactive'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_company' => 'integer',
        'name_company' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
