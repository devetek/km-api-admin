<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="Packet_detail",
 *      required={""},
 *      @SWG\Property(
 *          property="id_packet_detail",
 *          description="id_packet_detail",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="id_packet",
 *          description="id_packet",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="detail_packet",
 *          description="detail_packet",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="icon",
 *          description="icon",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="order_detail",
 *          description="order_detail",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Packet_detail extends Model
{

    public $table = 'packet_detail';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $primaryKey = 'id_packet_detail';

    public $fillable = [
        'id_packet',
        'detail_packet',
        'icon',
        'order_detail'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_packet_detail' => 'integer',
        'id_packet' => 'integer',
        'detail_packet' => 'string',
        'icon' => 'string',
        'order_detail' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
