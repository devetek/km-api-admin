<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="Purchase_confirm",
 *      required={""},
 *      @SWG\Property(
 *          property="id_purchase_confirm",
 *          description="id_purchase_confirm",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="id_purchase",
 *          description="id_purchase",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="transfer_form_chanel",
 *          description="transfer_form_chanel",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="transfer_form_name",
 *          description="transfer_form_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="value_paid",
 *          description="value_paid",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="foto_confirm",
 *          description="foto_confirm",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="validasi",
 *          description="validasi",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="id_user",
 *          description="id_user",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Purchase_confirm extends Model
{

    public $table = 'purchase_confirm';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $primaryKey = 'id_purchase_confirm';

    public $fillable = [
        'id_purchase',
        'date_confirm',
        'transfer_form_chanel',
        'transfer_form_name',
        'value_paid',
        'foto_confirm',
        'validasi',
        'id_user'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_purchase_confirm' => 'integer',
        'id_purchase' => 'integer',
        'transfer_form_chanel' => 'string',
        'transfer_form_name' => 'string',
        'value_paid' => 'integer',
        'foto_confirm' => 'string',
        'validasi' => 'integer',
        'id_user' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
