<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="Publisher_team_member",
 *      required={""},
 *      @SWG\Property(
 *          property="id_publisher_team_member",
 *          description="id_publisher_team_member",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="id_publisher_team",
 *          description="id_publisher_team",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="id_publisher",
 *          description="id_publisher",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Publisher_team_member extends Model
{

    public $table = 'publisher_team_member';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $primaryKey = 'id_publisher_team_member';

    public $fillable = [
        'id_publisher_team',
        'id_publisher'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_publisher_team_member' => 'integer',
        'id_publisher_team' => 'integer',
        'id_publisher' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
