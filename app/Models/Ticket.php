<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="Ticket",
 *      required={""},
 *      @SWG\Property(
 *          property="id_ticket",
 *          description="id_ticket",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="id_purchase",
 *          description="id_purchase",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="for_module",
 *          description="for_module",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="module_id",
 *          description="module_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="sub_module_id",
 *          description="sub_module_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="id_user",
 *          description="id_user",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="title_ticket",
 *          description="title_ticket",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="ticket_content",
 *          description="ticket_content",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="ticket_status",
 *          description="ticket_status",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="id_publisher_close",
 *          description="id_publisher_close",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="id_user_close",
 *          description="id_user_close",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="note_for_close",
 *          description="note_for_close",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="validasi",
 *          description="validasi",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Ticket extends Model
{

    public $table = 'ticket';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $primaryKey = 'id_ticket';

    public $fillable = [
        'id_purchase',
        'for_module',
        'module_id',
        'sub_module_id',
        'id_user',
        'title_ticket',
        'ticket_content',
        'date_publish',
        'date_close',
        'ticket_status',
        'id_publisher_close',
        'id_user_close',
        'note_for_close',
        'validasi'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_ticket' => 'integer',
        'id_purchase' => 'integer',
        'for_module' => 'string',
        'module_id' => 'integer',
        'sub_module_id' => 'integer',
        'id_user' => 'integer',
        'title_ticket' => 'string',
        'ticket_content' => 'string',
        'ticket_status' => 'string',
        'id_publisher_close' => 'integer',
        'id_user_close' => 'integer',
        'note_for_close' => 'string',
        'validasi' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
