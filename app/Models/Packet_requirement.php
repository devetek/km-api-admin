<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="Packet_requirement",
 *      required={""},
 *      @SWG\Property(
 *          property="id_packet_requirements",
 *          description="id_packet_requirements",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="id_packet",
 *          description="id_packet",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="order_requirement",
 *          description="order_requirement",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="icon",
 *          description="icon",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="validasi",
 *          description="validasi",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="requirement",
 *          description="requirement",
 *          type="string"
 *      )
 * )
 */
class Packet_requirement extends Model
{

    public $table = 'packet_requirements';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $primaryKey = 'id_packet_requirements';

    public $fillable = [
        'id_packet',
        'order_requirement',
        'icon',
        'validasi',
        'requirement'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_packet_requirements' => 'integer',
        'id_packet' => 'integer',
        'order_requirement' => 'integer',
        'icon' => 'string',
        'validasi' => 'integer',
        'requirement' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
