<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="Member",
 *      required={""},
 *      @SWG\Property(
 *          property="id_member",
 *          description="id_member",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="id_company",
 *          description="id_company",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="first_name",
 *          description="first_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="last_name",
 *          description="last_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="tgl_lahir",
 *          description="tgl_lahir",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="last_education",
 *          description="last_education",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="phone_member",
 *          description="phone_member",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="poscode_member",
 *          description="poscode_member",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="email_member",
 *          description="email_member",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="foto_member",
 *          description="foto_member",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="thum_member",
 *          description="thum_member",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="address_member",
 *          description="address_member",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="status_member",
 *          description="status_member",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="register_form",
 *          description="register_form",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="validasi",
 *          description="validasi",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="id_user",
 *          description="id_user",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="id_user_rgister",
 *          description="id_user_rgister",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Member extends Model
{

    public $table = 'member';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $primaryKey = 'id_member';

    public $fillable = [
        'id_company',
        'first_name',
        'last_name',
        'tgl_lahir',
        'last_education',
        'phone_member',
        'poscode_member',
        'email_member',
        'foto_member',
        'thum_member',
        'address_member',
        'status_member',
        'register_form',
        'validasi',
        'id_user',
        'id_user_rgister'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_member' => 'integer',
        'id_company' => 'integer',
        'first_name' => 'string',
        'last_name' => 'string',
        'tgl_lahir' => 'date',
        'last_education' => 'string',
        'phone_member' => 'string',
        'poscode_member' => 'string',
        'email_member' => 'string',
        'foto_member' => 'string',
        'thum_member' => 'string',
        'address_member' => 'string',
        'status_member' => 'string',
        'register_form' => 'string',
        'validasi' => 'integer',
        'id_user' => 'integer',
        'id_user_rgister' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
