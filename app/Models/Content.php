<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="Content",
 *      required={""},
 *      @SWG\Property(
 *          property="id_book_content",
 *          description="id_book_content",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="id_book_section",
 *          description="id_book_section",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="order_book_content",
 *          description="order_book_content",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="time_book_content",
 *          description="time_book_content",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="title_book_content",
 *          description="title_book_content",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="book_content_type",
 *          description="book_content_type",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="book_content",
 *          description="book_content",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="validasi",
 *          description="validasi",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="id_user",
 *          description="id_user",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Content extends Model
{

    public $table = 'book_content';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $primaryKey = 'id_book_content';

    public $fillable = [
        'id_book_section',
        'order_book_content',
        'time_book_content',
        'title_book_content',
        'book_content_type',
        'book_content',
        'validasi',
        'id_user'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_book_content' => 'integer',
        'id_book_section' => 'integer',
        'order_book_content' => 'integer',
        'time_book_content' => 'integer',
        'title_book_content' => 'string',
        'book_content_type' => 'string',
        'book_content' => 'string',
        'validasi' => 'integer',
        'id_user' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
