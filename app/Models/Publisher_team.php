<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="Publisher_team",
 *      required={""},
 *      @SWG\Property(
 *          property="id_publisher_team",
 *          description="id_publisher_team",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="id_publisher",
 *          description="id_publisher",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name_publisher_team",
 *          description="name_publisher_team",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="description_publisher_team",
 *          description="description_publisher_team",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="id_publisher_team_leader",
 *          description="id_publisher_team_leader",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Publisher_team extends Model
{

    public $table = 'publisher_team';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $primaryKey = 'id_publisher_team';

    public $fillable = [
        'id_publisher',
        'name_publisher_team',
        'for_company',
        'description_publisher_team',
        'id_publisher_team_leader'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_publisher_team' => 'integer',
        'id_publisher' => 'integer',
        'for_company' => 'integer',
        'name_publisher_team' => 'string',
        'description_publisher_team' => 'string',
        'id_publisher_team_leader' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
