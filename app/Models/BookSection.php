<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="BookSection",
 *      required={""},
 *      @SWG\Property(
 *          property="id_book_section",
 *          description="id_book_section",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="id_book",
 *          description="id_book",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="order_book_section",
 *          description="order_book_section",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="book_section",
 *          description="book_section",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="description_book_section",
 *          description="description_book_section",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="id_user",
 *          description="id_user",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="validasi",
 *          description="validasi",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class BookSection extends Model
{

    public $table = 'book_section';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $primaryKey = 'id_book_section';

    public $fillable = [
        'id_book',
        'order_book_section',
        'book_section',
        'description_book_section',
        'id_user',
        'validasi'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_book_section' => 'integer',
        'id_book' => 'integer',
        'order_book_section' => 'integer',
        'book_section' => 'string',
        'description_book_section' => 'string',
        'id_user' => 'integer',
        'validasi' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
