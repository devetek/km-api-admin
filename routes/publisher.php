<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//header('Access-Control-Allow-Origin: *');
//header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
//header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization,X-Auth-Token");
//date_default_timezone_set('Asia/Jakarta');



Route::middleware('api')->group( function () {
    Route::resource('admin/publisher', 'admin\PublisherAPIController');
    Route::resource('admin/publisher_covers', 'admin\PublisherCoverAPIController');
    Route::resource('admin/publisher_teams', 'admin\Publisher_teamAPIController');
    Route::resource('admin/publisher_team_members', 'admin\Publisher_team_memberAPIController');
});

//php artisan infyom:api BookContent --fromTable --tableName=book_content --prefix=book/member --primary=id_book_content
