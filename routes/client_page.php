<?php

use Illuminate\Http\Request;



Route::middleware('api')->group( function () {
    Route::resource('mobile/home', 'mobile\HomeController');
});

Route::middleware('api')->group( function () {
    Route::resource('mobile_guest/packet', 'mobile_guest\PacketController');
    Route::resource('mobile_guest/packet_requirement', 'mobile_guest\PacketRequirementController');
    Route::resource('mobile_guest/packet_book', 'mobile_guest\PacketBookController');
    Route::resource('mobile_guest/purchases', 'mobile_guest\PurchaseAPIController');
});

Route::middleware('api')->group( function () {
    Route::resource('web/packet', 'web\PacketController');
    Route::resource('web/packet_book', 'web\PacketBookController');
    Route::resource('web/packet_home', 'web\PacketHomeController');
    Route::resource('web/purchases', 'web\PurchaseAPIController');
    Route::resource('web/purchase_confirms', 'web\Purchase_confirmAPIController');
});
