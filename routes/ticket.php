<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//header('Access-Control-Allow-Origin: *');
//header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
//header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization,X-Auth-Token");
//date_default_timezone_set('Asia/Jakarta');



Route::middleware('api')->group( function () {
    Route::resource('admin/tickets', 'admin\TicketAPIController');
    Route::resource('admin/ticket_answers', 'admin\Ticket_answerAPIController');
});


Route::middleware('api')->group( function () {
    Route::resource('client/tickets', 'client\TicketAPIController');
    Route::resource('client/ticket_answers', 'client\Ticket_answerAPIController');

});
//php artisan infyom:api Book --fromTable --tableName=book --prefix=book/admin --primary=id_book
