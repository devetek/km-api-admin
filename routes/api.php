<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'Api\User\AuthCont@authenticate');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//php artisan infyom:api Slider --fromTable --tableName=gallery --prefix=Slider/member --primary=id_gallery



Route::delete('packet/admin/packet_sales/{packet_sales}', 'Packet/admin\Packet_saleAPIController@destroy');




Route::get('purchase/client/purchases', 'Purchase/client\PurchaseAPIController@index');
Route::post('purchase/client/purchases', 'Purchase/client\PurchaseAPIController@store');
Route::get('purchase/client/purchases/{purchases}', 'Purchase/client\PurchaseAPIController@show');
Route::put('purchase/client/purchases/{purchases}', 'Purchase/client\PurchaseAPIController@update');
Route::patch('purchase/client/purchases/{purchases}', 'Purchase/client\PurchaseAPIController@update');
Route::delete('purchase/client/purchases/{purchases}', 'Purchase/client\PurchaseAPIController@destroy');
