<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//header('Access-Control-Allow-Origin: *');
//header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
//header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization,X-Auth-Token");
//date_default_timezone_set('Asia/Jakarta');



Route::middleware('api')->group( function () {
    Route::resource('admin/packets', 'admin\PacketAPIController');
    Route::resource('admin/packetMasterCategories', 'admin\Packet_master_categoryAPIController');
    Route::resource('admin/packet_details', 'admin\Packet_detailAPIController');
    Route::resource('admin/packet_goals', 'admin\Packet_goalAPIController');
    Route::resource('admin/packet_requirements', 'admin\Packet_requirementAPIController');
    Route::resource('admin/packet_covers', 'admin\Packet_coverAPIController');
    Route::resource('admin/packet_books', 'admin\Packet_bookAPIController');
    Route::resource('admin/packet_sales', 'admin\Packet_saleAPIController');
    Route::resource('admin/packet_categories', 'admin\Packet_categoryAPIController');
    Route::resource('admin/my_packets', 'admin\MyPacketAPIController');
    Route::resource('admin/packet_incomes', 'admin\PacketIncomeAPIController');
});

Route::middleware('api')->group( function () {
    Route::resource('memberGuest/packets', 'member_Guest\PacketAPIController');
    Route::resource('memberGuest/packet_goals', 'member_Guest\Packet_goalAPIController');
    Route::resource('memberGuest/packet_requirements', 'member_Guest\Packet_requirementsAPIController');
});


Route::middleware('api')->group( function () {
    Route::resource('client/packets', 'client\PacketAPIController');
    Route::resource('client/packet_requirements', 'client\Packet_requirementAPIController');
    Route::resource('client/packet_goals', 'client\Packet_goalAPIController');
    Route::resource('client/packet_details', 'client\Packet_detailAPIController');
    Route::resource('client/my_packets', 'client\MyPacketAPIController');
    Route::resource('client/packet_books', 'client\Packet_bookAPIController');
    Route::resource('client/packet_categories', 'client\Packet_categoryAPIController');
});

//php artisan infyom:api Book --fromTable --tableName=book --prefix=book/admin --primary=id_book
